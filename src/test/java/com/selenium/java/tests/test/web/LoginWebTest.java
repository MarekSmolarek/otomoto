package com.selenium.java.tests.test.web;


import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.MethodHelper;
import com.selenium.java.listeners.TestListener;
import com.selenium.java.pages.web.LoginPage;
import com.selenium.java.pages.web.RegistrationPage;
import io.qameta.allure.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestContext;
import org.testng.annotations.*;

@Listeners({TestListener.class})
@Feature("WEB LOGIN: Login Web Tests")
public class LoginWebTest extends BaseTest {

    @BeforeMethod(description = "Starting appium server")
    @Parameters(value = {"Platform", "Browser"})
    public void setUp(String platform, String browser, ITestContext context) {

        headless = true;
        url = "https://www.otomoto.pl/";

        launchWeb(platform, browser, context);
        System.out.println("Test started");

    }

    @DataProvider
    public Object[][] getLogin(){
        return new Object[][] {
                {"damian.testowy12@vp.pl", "Haslo123", true},
                {"paula_testuje@onet.pl", "Tester2020OSW", true},
                {"paula_testuje1@onet.pl", "Tester2020OSW", false},
        };
    }

    @DataProvider
    public Object[][] getFailEmailLogin(){
        return new Object[][] {
                {"damian@vp.pl", "Haslo123", true},
                {"paly@onet.pl", "Tester2020OSW", true},
                {"estuje1@onet.pl", "Tester2020OSW", false},
        };
    }

    @DataProvider
    public Object[][] getNoEmailLogin(){
        return new Object[][] {
                {"", "Haslo123", true},
                {"", "Tester2020OSW", true},
                {"", "Tester2020OSW", false},
        };
    }


    @DataProvider
    public Object[][] getFailPassLogin(){
        return new Object[][] {
                {"damian.testowy12@vp.pl", "Haslo1213", true},
                {"paula_testuje@onet.pl", "Tester20202OSW", true},
                {"paula_testuje1@onet.pl", "Tester20220OSW", false},
        };
    }

    @DataProvider
    public Object[][] getNoPassLogin(){
        return new Object[][] {
                {"damian.testowy12@vp.pl", "", true},
                {"paula_testuje@onet.pl", "", true},
                {"paula_testuje1@onet.pl", "", false},
        };
    }

    @Test(dataProvider = "getLogin", description = "Checking correct login to app")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - login to application")
    @Story("WEB LOGIN: All inputs correct")
    public void loginTest(String email, String pass, boolean remember) {

        WebDriver driver = getDriver();
        MethodHelper methodHelper = PageFactory.initElements(driver, MethodHelper.class);
        RegistrationPage registrationPage = PageFactory.initElements(driver, RegistrationPage.class);
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);

        registrationPage.regMyAccount();

        loginPage.loginSetEmail(email);

        loginPage.loginSetPassword(pass);
        methodHelper.waitTime(2);
        loginPage.loginRememberMe(remember);
        methodHelper.waitTime(2);
        loginPage.loginSingIn();
        methodHelper.waitTime(3);
        loginPage.loginAssert();
        methodHelper.waitTime(2);
        methodHelper.testScreenshot("Login test");
    }

    @Test(dataProvider = "getFailEmailLogin", description = "Checking login to app with wrong email")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - login to application")
    @Story("WEB LOGIN: Wrong email")
    public void loginFailEmailTest(String email, String pass, boolean remember) {

        WebDriver driver = getDriver();
        MethodHelper methodHelper = PageFactory.initElements(driver, MethodHelper.class);
        RegistrationPage registrationPage = PageFactory.initElements(driver, RegistrationPage.class);
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);

        registrationPage.regMyAccount();

        loginPage.loginSetEmail(email);

        loginPage.loginSetPassword(pass);
        methodHelper.waitTime(2);
        loginPage.loginRememberMe(remember);
        methodHelper.waitTime(2);
        loginPage.loginSingIn();
        methodHelper.waitTime(3);
        loginPage.loginFailAssert();
        methodHelper.waitTime(2);
        methodHelper.testScreenshot("Login test");

    }

    @Test(dataProvider = "getNoEmailLogin", description = "Checking login to app without email")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - login to application")
    @Story("WEB LOGIN: No email")
    public void loginNoEmailTest(String email, String pass, boolean remember) {

        WebDriver driver = getDriver();
        MethodHelper methodHelper = PageFactory.initElements(driver, MethodHelper.class);
        RegistrationPage registrationPage = PageFactory.initElements(driver, RegistrationPage.class);
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);

        registrationPage.regMyAccount();

        loginPage.loginSetEmail(email);

        loginPage.loginSetPassword(pass);
        methodHelper.waitTime(2);
        loginPage.loginRememberMe(remember);
        methodHelper.waitTime(2);
        loginPage.loginSingIn();
        methodHelper.waitTime(3);
        loginPage.loginFailAssert();
        methodHelper.waitTime(2);
        methodHelper.testScreenshot("Login test");
    }

    @Test(dataProvider = "getFailPassLogin", description = "Checking login to app with wrong password")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - login to application")
    @Story("WEB LOGIN: Wrong password")
    public void loginFailPasswordTest(String email, String pass, boolean remember) {

        WebDriver driver = getDriver();
        MethodHelper methodHelper = PageFactory.initElements(driver, MethodHelper.class);
        RegistrationPage registrationPage = PageFactory.initElements(driver, RegistrationPage.class);
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);

        registrationPage.regMyAccount();

        loginPage.loginSetEmail(email);

        loginPage.loginSetPassword(pass);
        methodHelper.waitTime(2);
        loginPage.loginRememberMe(remember);
        methodHelper.waitTime(2);
        loginPage.loginSingIn();
        methodHelper.waitTime(3);
        loginPage.loginFailAssert();
        methodHelper.waitTime(2);
        methodHelper.testScreenshot("Login test");
    }

    @Test(dataProvider = "getNoPassLogin", description = "Checking login to app without password")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - login to application")
    @Story("WEB LOGIN: No password")
    public void loginNoPasswordTest(String email, String pass, boolean remember) {

        WebDriver driver = getDriver();
        MethodHelper methodHelper = PageFactory.initElements(driver, MethodHelper.class);
        RegistrationPage registrationPage = PageFactory.initElements(driver, RegistrationPage.class);
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);

        registrationPage.regMyAccount();

        loginPage.loginSetEmail(email);

        loginPage.loginSetPassword(pass);
        methodHelper.waitTime(2);
        loginPage.loginRememberMe(remember);
        methodHelper.waitTime(2);
        loginPage.loginSingIn();
        methodHelper.waitTime(3);
        loginPage.loginFailAssert();
        methodHelper.waitTime(2);
        methodHelper.testScreenshot("Login test");
    }

    @AfterMethod(description = "Finishing test and shutting down app")
    public void tearDown() {

        WebDriver driver = getDriver();

        System.out.println("Test ended");
        //  driver.close();
    }
    }
