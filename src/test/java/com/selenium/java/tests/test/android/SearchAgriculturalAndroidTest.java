package com.selenium.java.tests.test.android;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.MethodHelper;
import com.selenium.java.pages.android.SearchAgriculturalScreen;
import io.qameta.allure.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestContext;
import org.testng.annotations.*;

import static org.testng.Assert.assertTrue;


@Feature("SEARCHING: Agricultural vehicles and machines search Mobile Tests")
public class SearchAgriculturalAndroidTest extends BaseTest {
    @BeforeClass
    public void startup() {

        startAppiumServer();
    }

    @BeforeMethod(description = "Starting appium server")
    @Parameters(value = {"Platform", "DeviceID", "DeviceName"})
    public void setUp(String platform, String deviceId, String deviceName, ITestContext context) {

        instalApp = false;

        launchAndroid(platform, deviceId, deviceName, context);
    }


    //region DATAPROVIDERS

    //region DATAPROVIDER - search tractors

    @DataProvider
    public Object[][] searchTractors() {
        return new Object[][]{
                {"John Deere", "","50000", "300000", "Wszystkie", "PLN",2,3, "2012", "2020", "Polska", "", "", 4,5,"", "",
                        "Klimatyzacja", false, false, true, true, false, false, false, true, false, true, "Łódzkie"}
        };
    }

    //endregion DATAPROVIDER - search tractors

    //region DATAPROVIDER - search harvesters
    @DataProvider
    public Object[][] searchHarvesters() {
        return new Object[][]{
                {"John Deere","","50000", "3000000", "Wszystkie", "PLN",2,3, "2012", "2020", "Polska",4,5, "", "", "Radio",
                        false, false, true, true, false, false, false, true, false, true, "Opolskie"}
        };
    }
    //endregion DATAPROVIDER - search harvesters

    //region DATAPROVIDER - search aggregates
    @DataProvider
    public Object[][] searchAggregates() {
        return new Object[][]{
                {"John Deere", "","50000", "3000000", "Wszystkie", "PLN", 2,3,"2012", "2020", "Polska",
                        false, false, true, true, false, false, false, true, false, true, "Dolnośląskie"}
        };
    }
    //endregion DATAPROVIDER - search aggregates

    //region DATAPROVIDER - search plows
    @DataProvider
    public Object[][] searchPlows() {
        return new Object[][]{
                {"John Deere", "", "50000", "3000000", "Wszystkie", "PLN",2,3, "2012", "2020", "Polska",
                        false, false, true, true, false, false, false, true, false, true, "Dolnośląskie"}
        };
    }
    //endregion DATAPROVIDER - search plows

    //region DATAPROVIDER - search spreaders
    @DataProvider
    public Object[][] searchSpreaders() {
        return new Object[][]{
                {"John Deere", "", "50000", "3000000", "Wszystkie", "PLN",2,3, "2012", "2020", "Polska",
                        false, false, true, true, false, false, false, true, false, true, "Dolnośląskie"}
        };
    }
    //endregion DATAPROVIDER - search spreaders

    //region DATAPROVIDER - search mowers
    @DataProvider
    public Object[][] searchMowers() {
        return new Object[][]{
                {"John Deere", "", "50000", "3000000", "Wszystkie", "PLN",2,3, "2012", "2020", "Polska",
                        false, false, true, true, false, false, false, true, false, true, "Opolskie"}
        };
    }
    //endregion DATAPROVIDER - search mowers

    //region DATAPROVIDER - search front loaders
    @DataProvider
    public Object[][] searchFrontLoaders() {
        return new Object[][]{
                {"John Deere", "", "50000", "3000000", "Wszystkie", "PLN",2,3, "2012", "2020", "Polska",
                        false, false, true, true, false, false, false, true, false, true, "Dolnośląskie"}
        };
    }
    //endregion DATAPROVIDER - search front loaders

    //region DATAPROVIDER - search presses and wrappers
    @DataProvider
    public Object[][] searchPressesAndWrappers() {
        return new Object[][]{
                {"John Deere", "", "50000", "3000000", "Wszystkie", "PLN",2,3, "2012", "2020", "Polska",
                        false, false, true, true, false, false, false, true, false, true, "Małopolskie"}
        };
    }
    //endregion DATAPROVIDER - search presses and wrappers

    //region DATAPROVIDER - search sprayers
    @DataProvider
    public Object[][] searchSprayers() {
        return new Object[][]{
                {"John Deere", "", "50000", "3000000", "Wszystkie", "PLN",2,3, "2012", "2020", "Polska",
                        false, false, true, true, false, false, false, true, false, true, "Lubuskie"}
        };
    }
    //endregion DATAPROVIDER - search sprayers

    //region DATAPROVIDER - search planters and seeders
    @DataProvider
    public Object[][] searchPlantersAndSeeders() {
        return new Object[][]{
                {"John Deere","", "50000", "3000000", "Wszystkie", "PLN",2,3, "2012", "2020", "Polska",
                        false, false, true, true, false, false, false, true, false, true, "Opolskie"}
        };
    }
    //endregion DATAPROVIDER - search planters and seeders

    //region DATAPROVIDER - search forest machinery
    @DataProvider
    public Object[][] searchForestMachinery() {
        return new Object[][]{
                {"John Deere", "","50000", "3000000", "Wszystkie", "PLN",2,3, "2012", "2020", "Polska",
                        false, false, true, true, false, false, false, true, false, true, "Lubelskie"}
        };
    }
    //endregion DATAPROVIDER - search forest machinery

    //region DATAPROVIDER - search other agricultural machinery
    @DataProvider
    public Object[][] searchOtherAgriculturalMachinery() {
        return new Object[][]{
                {"John Deere", "", "50000", "3000000", "Wszystkie", "PLN",2,3, "2012", "2020", "Polska",
                        false, false, true, true, false, false, false, true, false, true, "Lubuskie"}
        };
    }
    //endregion DATAPROVIDER - search other agricultural machinery

    //region DATAPROVIDER - search animal husbrandry machinery
    @DataProvider
    public Object[][] searchAnimalHusbandryMachinery() {
        return new Object[][]{
                {"John Deere", "","50000", "3000000", "Wszystkie", "PLN",2,3, "2012", "2020", "Polska",
                        false, false, true, true, false, false, false, true, false, true, "Pomorskie"}
        };
    }
    //endregion DATAPROVIDER - search animal husbrandry machinery

    //region DATAPROVIDER - search agricultural trailers
    @DataProvider
    public Object[][] searchAgriculturalTrailers() {
        return new Object[][]{
                {"CMT", "","50000", "3000000", "Wszystkie", "PLN",2,3, "2012", "2020", "Polska",
                        false, false, true, true, false, false, false, true, false, true, "Łódzkie"}
        };
    }
    //endregion DATAPROVIDER - search agricultural trailers

    //region DATAPROVIDER - search agricultural machinery accessories / headers / attachments
    @DataProvider
    public Object[][] searchAgriculturalMachineryAccessories() {
        return new Object[][]{
                {"John Deere", "","50000", "3000000", "Wszystkie", "PLN",2,3, "2012", "2020", "Polska",
                        false, false, true, true, false, false, false, true, false, true, "Wielkopolskie"}
        };
    }
    //endregion DATAPROVIDER - search agricultural machinery accessories / headers / attachments

    //region DATAPROVIDER - search telescopic loaders and miniloaders
    @DataProvider
    public Object[][] searchLoaders() {
        return new Object[][]{
                {"John Deere", "","50000", "3000000", "Wszystkie", "PLN",2,3, "2012", "2020", "Polska",
                        false, false, true, true, false, false, false, true, false, true, "Małopolskie"}
        };
    }
    //endregion DATAPROVIDER - search telescopic loaders and miniloaders

    //region DATAPROVIDER - search manure spreaders
    @DataProvider
    public Object[][] searchManureSpreaders() {
        return new Object[][]{
                {"John Deere", "", "50000", "3000000", "Wszystkie", "PLN", 2,3,"2012", "2020", "Polska",
                        false, false, true, true, false, false, false, true, false, true, "Opolskie"}
        };
    }
    //endregion DATAPROVIDER - search manure spreaders

    //region DATAPROVIDER - search chaff-cutters
    @DataProvider
    public Object[][] searchChaffCutters() {
        return new Object[][]{
                {"John Deere", "","50000", "3000000", "Wszystkie", "PLN",2,3, "2012", "2020", "Polska",
                        false, false, true, true, false, false, false, true, false, true, "Mazowieckie"}
        };
    }
    //endregion DATAPROVIDER - search chaff-cutters

    //region DATAPROVIDER - search all in agricultural
    @DataProvider
    public Object[][] searchAllInAgricultural() {
        return new Object[][]{
                {"John Deere", "","50000", "3000000", "Wszystkie", "PLN", 2,3,"2012", "2020", "Polska",
                        false, false, true, true, false, false, false, true, false, true, "Opolskie"}
        };
    }
    //endregion DATAPROVIDER - search all in agricultural

    //endregion DATAPROVIDERS


    //region TESTS
    MethodHelper helper = new MethodHelper();
    //region TEST - tractors searching
    @Test(dataProvider = "searchTractors", description = "Searching item in Tractors category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search tractors")
    public void testTractorsSearching(String vehicleBrand, String vehicleModel, String priceMin, String priceMax,
                                      String privacy, String currency, int third, int fourth, String yearOfProdFrom, String yearOfProdTo,
                                      String country, String gearbox, String drive, int fifth, int sixth, String powerMin, String powerMax,
                                      String addEquip, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat,
                                      boolean financingOption, boolean leasing, boolean rent, boolean damaged, boolean
                                              registerInPoland, boolean firstOwner, boolean noAccidents, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Rolnicze\"");

        WebDriver driver = getDriver();
        SearchAgriculturalScreen agriculturalScreen = PageFactory.initElements(driver, SearchAgriculturalScreen.class);

        agriculturalScreen.searchSetAgricultural();
        agriculturalScreen.searchApplication("Ciągniki (traktory)");
        agriculturalScreen.searchVehicleBrand(vehicleBrand);
        agriculturalScreen.searchVehicleModel(vehicleModel);
        agriculturalScreen.searchSetPrice(priceMin, priceMax);
        agriculturalScreen.searchSetPrivacy(privacy);
        agriculturalScreen.searchSetCurrency(currency);
        agriculturalScreen.searchMoreParameters();
        agriculturalScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        agriculturalScreen.searchSetCountryOfOrigin(country);
        agriculturalScreen.searchSetGearbox(gearbox);
        agriculturalScreen.searchSetDrive(drive);
        agriculturalScreen.searchSetPower(4, 5, powerMin, powerMax);
        agriculturalScreen.searchSetAdditionalEquipment(addEquip);
        agriculturalScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        agriculturalScreen.searchCarStatus(damaged, registerInPoland, firstOwner, noAccidents);
        agriculturalScreen.searchLocation(voivodeship);
        agriculturalScreen.searchShowResults();

        helper.testScreenShoot("SearchAgriculturalAndroidTestTractors");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion TEST - tractors searching

    //region TEST - harvesters searching
    @Test(dataProvider = "searchHarvesters", description = "Searching item in Harvesters category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search harvesters")
    public void testHarvestersSearching(String vehicleBrand, String vehicleModel,String priceMin, String priceMax,
                                        String privacy, String currency, int third, int fourth, String yearOfProdFrom, String yearOfProdTo,
                                        String country, int fifth, int sixth, String powerMin, String powerMax, String addEquip, boolean authorizedDealer,
                                        boolean vatMargin, boolean invoiceVat, boolean financingOption, boolean leasing, boolean rent,
                                        boolean damaged, boolean registerInPoland, boolean firstOwner, boolean noAccidents, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Rolnicze\"");

        WebDriver driver = getDriver();
        SearchAgriculturalScreen agriculturalScreen = PageFactory.initElements(driver, SearchAgriculturalScreen.class);

        agriculturalScreen.searchSetAgricultural();
        agriculturalScreen.searchApplication("Kombajny");
        agriculturalScreen.searchVehicleBrand(vehicleBrand);
        agriculturalScreen.searchVehicleModel(vehicleModel);
        agriculturalScreen.searchSetPrice( priceMin, priceMax);
        agriculturalScreen.searchSetPrivacy(privacy);
        agriculturalScreen.searchSetCurrency(currency);
        agriculturalScreen.searchMoreParameters();
        agriculturalScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        agriculturalScreen.searchSetCountryOfOrigin(country);
        agriculturalScreen.searchSetPower(4, 5, powerMin, powerMax);
        agriculturalScreen.searchSetAdditionalEquipment(addEquip);
        agriculturalScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        agriculturalScreen.searchCarStatus(damaged, registerInPoland, firstOwner, noAccidents);
        agriculturalScreen.searchLocation(voivodeship);
        agriculturalScreen.searchShowResults();

        helper.testScreenShoot("SearchAgriculturalAndroidTestHarvesters");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion TEST - harvesters searching

    //region TEST - aggregates searching
    @Test(dataProvider = "searchAggregates", description = "Searching item in aggregates category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search aggregates")
    public void testAggregatesSearching(String vehicleBrand, String vehicleModel, String priceMin, String priceMax,
                                        String privacy, String currency, int third, int fourth, String yearOfProdFrom, String yearOfProdTo,
                                        String country, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat, boolean financingOption, boolean leasing, boolean rent,
                                        boolean damaged, boolean registerInPoland, boolean firstOwner, boolean noAccidents, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Rolnicze\"");

        WebDriver driver = getDriver();
        SearchAgriculturalScreen agriculturalScreen = PageFactory.initElements(driver, SearchAgriculturalScreen.class);

        agriculturalScreen.searchSetAgricultural();
        agriculturalScreen.searchApplication("Agregaty");
        agriculturalScreen.searchVehicleBrand(vehicleBrand);
        agriculturalScreen.searchVehicleModel(vehicleModel);
        agriculturalScreen.searchSetPrice( priceMin, priceMax);
        agriculturalScreen.searchSetPrivacy(privacy);
        agriculturalScreen.searchSetCurrency(currency);
        agriculturalScreen.searchMoreParameters();
        agriculturalScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        agriculturalScreen.searchSetCountryOfOrigin(country);
        agriculturalScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        agriculturalScreen.searchCarStatus(damaged, registerInPoland, firstOwner, noAccidents);
        agriculturalScreen.searchLocation(voivodeship);
        agriculturalScreen.searchShowResults();

        helper.testScreenShoot("SearchAgriculturalAndroidTestAggregates");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion TEST - aggregates searching

    //region TEST - plows searching
    @Test(dataProvider = "searchPlows", description = "Searching item in aggregates category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search plows")
    public void testPlowsSearching(String vehicleBrand,  String vehicleModel, String priceMin, String priceMax,
                                   String privacy, String currency, int third, int fourth, String yearOfProdFrom, String yearOfProdTo,
                                   String country, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat, boolean financingOption, boolean leasing, boolean rent,
                                   boolean damaged, boolean registerInPoland, boolean firstOwner, boolean noAccidents, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Rolnicze\"");

        WebDriver driver = getDriver();
        SearchAgriculturalScreen agriculturalScreen = PageFactory.initElements(driver, SearchAgriculturalScreen.class);

        agriculturalScreen.searchSetAgricultural();
        agriculturalScreen.searchApplication("Pługi");
        agriculturalScreen.searchVehicleBrand(vehicleBrand);
        agriculturalScreen.searchVehicleModel(vehicleModel);
        agriculturalScreen.searchSetPrice(priceMin, priceMax);
        agriculturalScreen.searchSetPrivacy(privacy);
        agriculturalScreen.searchSetCurrency(currency);
        agriculturalScreen.searchMoreParameters();
        agriculturalScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        agriculturalScreen.searchSetCountryOfOrigin(country);
        agriculturalScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        agriculturalScreen.searchCarStatus(damaged, registerInPoland, firstOwner, noAccidents);
        agriculturalScreen.searchLocation(voivodeship);
        agriculturalScreen.searchShowResults();

        helper.testScreenShoot("SearchAgriculturalAndroidTestPlows");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion TEST - plows searching

    //region TEST - spreaders searching
    @Test(dataProvider = "searchSpreaders", description = "Searching item in spreaders category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search spreaders")
    public void testSpreadersSearching(String vehicleBrand,String vehicleModel, String priceMin, String priceMax,
                                       String privacy, String currency, int third, int fourth, String yearOfProdFrom, String yearOfProdTo,
                                       String country, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat, boolean financingOption, boolean leasing, boolean rent,
                                       boolean damaged, boolean registerInPoland, boolean firstOwner, boolean noAccidents, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Rolnicze\"");

        WebDriver driver = getDriver();
        SearchAgriculturalScreen agriculturalScreen = PageFactory.initElements(driver, SearchAgriculturalScreen.class);

        agriculturalScreen.searchSetAgricultural();
        agriculturalScreen.searchApplication("Rozsiewacze");
        agriculturalScreen.searchVehicleBrand(vehicleBrand);
        agriculturalScreen.searchVehicleModel(vehicleModel);
        agriculturalScreen.searchSetPrice(priceMin, priceMax);
        agriculturalScreen.searchSetPrivacy(privacy);
        agriculturalScreen.searchSetCurrency(currency);
        agriculturalScreen.searchMoreParameters();
        agriculturalScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        agriculturalScreen.searchSetCountryOfOrigin(country);
        agriculturalScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        agriculturalScreen.searchCarStatus(damaged, registerInPoland, firstOwner, noAccidents);
        agriculturalScreen.searchLocation(voivodeship);
        agriculturalScreen.searchShowResults();

        helper.testScreenShoot("SearchAgriculturalAndroidTestSpreaders");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion TEST - spreaders searching

    //region TEST - mowers searching
    @Test(dataProvider = "searchPlows", description = "Searching item in mowers category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search mowers")
    public void testMowersSearching(String vehicleBrand, String vehicleModel, String priceMin, String priceMax,
                                    String privacy, String currency, int third, int fourth, String yearOfProdFrom, String yearOfProdTo,
                                    String country, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat, boolean financingOption, boolean leasing, boolean rent,
                                    boolean damaged, boolean registerInPoland, boolean firstOwner, boolean noAccidents, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Rolnicze\"");

        WebDriver driver = getDriver();
        SearchAgriculturalScreen agriculturalScreen = PageFactory.initElements(driver, SearchAgriculturalScreen.class);

        agriculturalScreen.searchSetAgricultural();
        agriculturalScreen.searchApplication("Kosiarki");
        agriculturalScreen.searchVehicleBrand(vehicleBrand);
        agriculturalScreen.searchVehicleModel(vehicleModel);
        agriculturalScreen.searchSetPrice( priceMin, priceMax);
        agriculturalScreen.searchSetPrivacy(privacy);
        agriculturalScreen.searchSetCurrency(currency);
        agriculturalScreen.searchMoreParameters();
        agriculturalScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        agriculturalScreen.searchSetCountryOfOrigin(country);
        agriculturalScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        agriculturalScreen.searchCarStatus(damaged, registerInPoland, firstOwner, noAccidents);
        agriculturalScreen.searchLocation(voivodeship);
        agriculturalScreen.searchShowResults();

        helper.testScreenShoot("SearchAgriculturalAndroidTestMowers");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion TEST - mowers searching

    //region TEST - front loaders searching
    @Test(dataProvider = "searchFrontLoaders", description = "Searching item in front loaders category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search front loaders")
    public void testFrontLoadersSearching(String vehicleBrand,  String vehicleModel, String priceMin, String priceMax,
                                          String privacy, String currency, int third, int fourth,String yearOfProdFrom, String yearOfProdTo,
                                          String country, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat, boolean financingOption, boolean leasing, boolean rent,
                                          boolean damaged, boolean registerInPoland, boolean firstOwner, boolean noAccidents, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Rolnicze\"");

        WebDriver driver = getDriver();
        SearchAgriculturalScreen agriculturalScreen = PageFactory.initElements(driver, SearchAgriculturalScreen.class);

        agriculturalScreen.searchSetAgricultural();
        agriculturalScreen.searchApplication("Ładowacze czołowe");
        agriculturalScreen.searchVehicleBrand(vehicleBrand);
        agriculturalScreen.searchVehicleModel(vehicleModel);
        agriculturalScreen.searchSetPrice(priceMin, priceMax);
        agriculturalScreen.searchSetPrivacy(privacy);
        agriculturalScreen.searchSetCurrency(currency);
        agriculturalScreen.searchMoreParameters();
        agriculturalScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        agriculturalScreen.searchSetCountryOfOrigin(country);
        agriculturalScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        agriculturalScreen.searchCarStatus(damaged, registerInPoland, firstOwner, noAccidents);
        agriculturalScreen.searchLocation(voivodeship);
        agriculturalScreen.searchShowResults();

        helper.testScreenShoot("SearchAgriculturalAndroidTestFrontLoaders");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion TEST - front loaders searching

    //region TEST - presses and wrappers searching
    @Test(dataProvider = "searchPressesAndWrappers", description = "Searching item in presses and wrappers category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search presses and wrappers")
    public void testPressesAndWrappersSearching(String vehicleBrand, String vehicleModel,String priceMin, String priceMax,
                                                String privacy, String currency, int third, int fourth, String yearOfProdFrom, String yearOfProdTo,
                                                String country, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat, boolean financingOption, boolean leasing, boolean rent,
                                                boolean damaged, boolean registerInPoland, boolean firstOwner, boolean noAccidents, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Rolnicze\"");

        WebDriver driver = getDriver();
        SearchAgriculturalScreen agriculturalScreen = PageFactory.initElements(driver, SearchAgriculturalScreen.class);

        agriculturalScreen.searchSetAgricultural();
        agriculturalScreen.searchApplication("Prasy i owijarki");
        agriculturalScreen.searchVehicleBrand(vehicleBrand);
        agriculturalScreen.searchVehicleModel(vehicleModel);
        agriculturalScreen.searchSetPrice( priceMin, priceMax);
        agriculturalScreen.searchSetPrivacy(privacy);
        agriculturalScreen.searchSetCurrency(currency);
        agriculturalScreen.searchMoreParameters();
        agriculturalScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        agriculturalScreen.searchSetCountryOfOrigin(country);
        agriculturalScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        agriculturalScreen.searchCarStatus(damaged, registerInPoland, firstOwner, noAccidents);
        agriculturalScreen.searchLocation(voivodeship);
        agriculturalScreen.searchShowResults();

        helper.testScreenShoot("SearchAgriculturalAndroidTestPressesAndWrappers");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion TEST - presses and wrappers searching

    //region TEST - sprayers searching
    @Test(dataProvider = "searchSprayers", description = "Searching item in sprayers category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search sprayers")
    public void testSprayersSearching(String vehicleBrand, String vehicleModel, String priceMin, String priceMax,
                                      String privacy, String currency, int third, int fourth,String yearOfProdFrom, String yearOfProdTo,
                                      String country, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat, boolean financingOption, boolean leasing, boolean rent,
                                      boolean damaged, boolean registerInPoland, boolean firstOwner, boolean noAccidents, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Rolnicze\"");

        WebDriver driver = getDriver();
        SearchAgriculturalScreen agriculturalScreen = PageFactory.initElements(driver, SearchAgriculturalScreen.class);

        agriculturalScreen.searchSetAgricultural();
        agriculturalScreen.searchApplication("Opryskiwacze");
        agriculturalScreen.searchVehicleBrand(vehicleBrand);
        agriculturalScreen.searchVehicleModel( vehicleModel);
        agriculturalScreen.searchSetPrice(priceMin, priceMax);
        agriculturalScreen.searchSetPrivacy(privacy);
        agriculturalScreen.searchSetCurrency(currency);
        agriculturalScreen.searchMoreParameters();
        agriculturalScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        agriculturalScreen.searchSetCountryOfOrigin(country);
        agriculturalScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        agriculturalScreen.searchCarStatus(damaged, registerInPoland, firstOwner, noAccidents);
        agriculturalScreen.searchLocation(voivodeship);
        agriculturalScreen.searchShowResults();

        helper.testScreenShoot("SearchAgriculturalAndroidTestSprayers");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion TEST - sprayers searching

    //region TEST - planters and seeders searching
    @Test(dataProvider = "searchPlantersAndSeeders", description = "Searching item in planters and seeders category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search planters and seeders")
    public void testPlantersAndSeedersSearching(String vehicleBrand, String vehicleModel,  String priceMin, String priceMax,
                                                String privacy, String currency, int third, int fourth, String yearOfProdFrom, String yearOfProdTo,
                                                String country, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat, boolean financingOption, boolean leasing, boolean rent,
                                                boolean damaged, boolean registerInPoland, boolean firstOwner, boolean noAccidents, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Rolnicze\"");

        WebDriver driver = getDriver();
        SearchAgriculturalScreen agriculturalScreen = PageFactory.initElements(driver, SearchAgriculturalScreen.class);

        agriculturalScreen.searchSetAgricultural();
        agriculturalScreen.searchApplication("Sadzarki i siewniki");
        agriculturalScreen.searchVehicleBrand(vehicleBrand);
        agriculturalScreen.searchVehicleModel(vehicleModel);
        agriculturalScreen.searchSetPrice(priceMin, priceMax);
        agriculturalScreen.searchSetPrivacy(privacy);
        agriculturalScreen.searchSetCurrency(currency);
        agriculturalScreen.searchMoreParameters();
        agriculturalScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        agriculturalScreen.searchSetCountryOfOrigin(country);
        agriculturalScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        agriculturalScreen.searchCarStatus(damaged, registerInPoland, firstOwner, noAccidents);
        agriculturalScreen.searchLocation(voivodeship);
        agriculturalScreen.searchShowResults();

        helper.testScreenShoot("SearchAgriculturalAndroidTestPlantersAndSeeders");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion TEST - planters and seeders searching

    //region TEST - forest machinery searching
    @Test(dataProvider = "searchForestMachinery", description = "Searching item in forest machinery category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search forest machinery")
    public void testForestMachinerySearching(String vehicleBrand,  String vehicleModel, String priceMin, String priceMax,
                                             String privacy, String currency, int third, int fourth,String yearOfProdFrom, String yearOfProdTo,
                                             String country, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat, boolean financingOption, boolean leasing, boolean rent,
                                             boolean damaged, boolean registerInPoland, boolean firstOwner, boolean noAccidents, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Rolnicze\"");

        WebDriver driver = getDriver();
        SearchAgriculturalScreen agriculturalScreen = PageFactory.initElements(driver, SearchAgriculturalScreen.class);

        agriculturalScreen.searchSetAgricultural();
        agriculturalScreen.searchApplication("Maszyny leśne");
        agriculturalScreen.searchVehicleBrand(vehicleBrand);
        agriculturalScreen.searchVehicleModel(vehicleModel);
        agriculturalScreen.searchSetPrice( priceMin, priceMax);
        agriculturalScreen.searchSetPrivacy(privacy);
        agriculturalScreen.searchSetCurrency(currency);
        agriculturalScreen.searchMoreParameters();
        agriculturalScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        agriculturalScreen.searchSetCountryOfOrigin(country);
        agriculturalScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        agriculturalScreen.searchCarStatus(damaged, registerInPoland, firstOwner, noAccidents);
        agriculturalScreen.searchLocation(voivodeship);
        agriculturalScreen.searchShowResults();

        helper.testScreenShoot("SearchAgriculturalAndroidTestForestMachinery");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion TEST - forest machinery searching

    //region TEST - other agricultural machinery searching
    @Test(dataProvider = "searchOtherAgriculturalMachinery", description = "Searching item in other agricultural machinery category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search other agricultural machinery")
    public void testOtherAgriculturalMachinerySearching(String vehicleBrand, String vehicleModel, String priceMin, String priceMax,
                                                        String privacy, String currency, int third, int fourth,String yearOfProdFrom, String yearOfProdTo,
                                                        String country, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat, boolean financingOption, boolean leasing, boolean rent,
                                                        boolean damaged, boolean registerInPoland, boolean firstOwner, boolean noAccidents, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Rolnicze\"");

        WebDriver driver = getDriver();
        SearchAgriculturalScreen agriculturalScreen = PageFactory.initElements(driver, SearchAgriculturalScreen.class);

        agriculturalScreen.searchSetAgricultural();
        agriculturalScreen.searchApplication("Inne maszyny rolnicze");
        agriculturalScreen.searchVehicleBrand(vehicleBrand);
        agriculturalScreen.searchVehicleModel(vehicleModel);
        agriculturalScreen.searchSetPrice(priceMin, priceMax);
        agriculturalScreen.searchSetPrivacy(privacy);
        agriculturalScreen.searchSetCurrency(currency);
        agriculturalScreen.searchMoreParameters();
        agriculturalScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        agriculturalScreen.searchSetCountryOfOrigin(country);
        agriculturalScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        agriculturalScreen.searchCarStatus(damaged, registerInPoland, firstOwner, noAccidents);
        agriculturalScreen.searchLocation(voivodeship);
        agriculturalScreen.searchShowResults();

        helper.testScreenShoot("SearchAgriculturalAndroidTestOtherAgriculturalMachinery");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion TEST - other agricultural machinery searching

    //region TEST - animal husbrandry machinery searching
    @Test(dataProvider = "searchAnimalHusbandryMachinery", description = "Searching item in animal husbrandry machinery category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search animal husbrandry machinery")
    public void testAnimalHusbandryMachinerySearching(String vehicleBrand,String vehicleModel,  String priceMin, String priceMax,
                                                      String privacy, String currency, int third, int fourth,String yearOfProdFrom, String yearOfProdTo,
                                                      String country, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat, boolean financingOption, boolean leasing, boolean rent,
                                                      boolean damaged, boolean registerInPoland, boolean firstOwner, boolean noAccidents, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Rolnicze\"");

        WebDriver driver = getDriver();
        SearchAgriculturalScreen agriculturalScreen = PageFactory.initElements(driver, SearchAgriculturalScreen.class);

        agriculturalScreen.searchSetAgricultural();
        agriculturalScreen.searchApplication("Inne maszyny rolnicze");
        agriculturalScreen.searchVehicleBrand(vehicleBrand);
        agriculturalScreen.searchVehicleModel( vehicleModel);
        agriculturalScreen.searchSetPrice( priceMin, priceMax);
        agriculturalScreen.searchSetPrivacy(privacy);
        agriculturalScreen.searchSetCurrency(currency);
        agriculturalScreen.searchMoreParameters();
        agriculturalScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        agriculturalScreen.searchSetCountryOfOrigin(country);
        agriculturalScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        agriculturalScreen.searchCarStatus(damaged, registerInPoland, firstOwner, noAccidents);
        agriculturalScreen.searchLocation(voivodeship);
        agriculturalScreen.searchShowResults();

        helper.testScreenShoot("SearchAgriculturalAndroidTestAnimalHusbandryMachinery");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion TEST - animal husbrandry machinery searching

    //region TEST - agricultural trailers searching
    @Test(dataProvider = "searchAgriculturalTrailers", description = "Searching item in agricultural trailers category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search agricultural trailers")
    public void testAgriculturalTrailersSearching(String vehicleBrand,  String vehicleModel, String priceMin, String priceMax,
                                                  String privacy, String currency, int third, int fourth, String yearOfProdFrom, String yearOfProdTo,
                                                  String country, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat, boolean financingOption, boolean leasing, boolean rent,
                                                  boolean damaged, boolean registerInPoland, boolean firstOwner, boolean noAccidents, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Rolnicze\"");

        WebDriver driver = getDriver();
        SearchAgriculturalScreen agriculturalScreen = PageFactory.initElements(driver, SearchAgriculturalScreen.class);

        agriculturalScreen.searchSetAgricultural();
        agriculturalScreen.searchApplication("Przyczepy rolnicze");
        agriculturalScreen.searchVehicleBrand(vehicleBrand);
        agriculturalScreen.searchVehicleModel(vehicleModel);
        agriculturalScreen.searchSetPrice(priceMin, priceMax);
        agriculturalScreen.searchSetPrivacy(privacy);
        agriculturalScreen.searchSetCurrency(currency);
        agriculturalScreen.searchMoreParameters();
        agriculturalScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        agriculturalScreen.searchSetCountryOfOrigin(country);
        agriculturalScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        agriculturalScreen.searchCarStatus(damaged, registerInPoland, firstOwner, noAccidents);
        agriculturalScreen.searchLocation(voivodeship);
        agriculturalScreen.searchShowResults();

        helper.testScreenShoot("SearchAgriculturalAndroidTestAgriculturalTrailers");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion TEST - agricultural trailers searching

    //region TEST - agricultural machinery accessories / headers / attachments searching
    @Test(dataProvider = "searchAgriculturalMachineryAccessories", description = "Searching item in agricultural machinery accessories / headers / attachments category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search agricultural machinery accessories / headers / attachments")
    public void testAgriculturalMachineryAccessoriesSearching(String vehicleBrand, String vehicleModel,String priceMin, String priceMax,
                                                              String privacy, String currency, int third, int fourth, String yearOfProdFrom, String yearOfProdTo,
                                                              String country, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat, boolean financingOption, boolean leasing, boolean rent,
                                                              boolean damaged, boolean registerInPoland, boolean firstOwner, boolean noAccidents, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Rolnicze\"");

        WebDriver driver = getDriver();
        SearchAgriculturalScreen agriculturalScreen = PageFactory.initElements(driver, SearchAgriculturalScreen.class);

        agriculturalScreen.searchSetAgricultural();
        agriculturalScreen.searchApplication("Akcesoria do maszyn rolniczych/hedery/przystawki");
        agriculturalScreen.searchVehicleBrand(vehicleBrand);
        agriculturalScreen.searchVehicleModel(vehicleModel);
        agriculturalScreen.searchSetPrice( priceMin, priceMax);
        agriculturalScreen.searchSetPrivacy(privacy);
        agriculturalScreen.searchSetCurrency(currency);
        agriculturalScreen.searchMoreParameters();
        agriculturalScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        agriculturalScreen.searchSetCountryOfOrigin(country);
        agriculturalScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        agriculturalScreen.searchCarStatus(damaged, registerInPoland, firstOwner, noAccidents);
        agriculturalScreen.searchLocation(voivodeship);
        agriculturalScreen.searchShowResults();

        helper.testScreenShoot("SearchAgriculturalAndroidTestAgrMachAccessories");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion TEST - agricultural machinery accessories / headers / attachments searching

    //region TEST - telescopic loaders and miniloaders searching
    @Test(dataProvider = "searchLoaders", description = "Searching item in telescopic loaders and miniloaders category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search telescopic loaders and miniloaders")
    public void testLoadersSearching(String vehicleBrand, String vehicleModel, String priceMin, String priceMax,
                                     String privacy, String currency, int third, int fourth,String yearOfProdFrom, String yearOfProdTo,
                                     String country, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat, boolean financingOption, boolean leasing, boolean rent,
                                     boolean damaged, boolean registerInPoland, boolean firstOwner, boolean noAccidents, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Rolnicze\"");

        WebDriver driver = getDriver();
        SearchAgriculturalScreen agriculturalScreen = PageFactory.initElements(driver, SearchAgriculturalScreen.class);

        agriculturalScreen.searchSetAgricultural();
        agriculturalScreen.searchApplication("Ładowarki teleskopowe i miniładowarki");
        agriculturalScreen.searchVehicleBrand(vehicleBrand);
        agriculturalScreen.searchVehicleModel(vehicleModel);
        agriculturalScreen.searchSetPrice( priceMin, priceMax);
        agriculturalScreen.searchSetPrivacy(privacy);
        agriculturalScreen.searchSetCurrency(currency);
        agriculturalScreen.searchMoreParameters();
        agriculturalScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        agriculturalScreen.searchSetCountryOfOrigin(country);
        agriculturalScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        agriculturalScreen.searchCarStatus(damaged, registerInPoland, firstOwner, noAccidents);
        agriculturalScreen.searchLocation(voivodeship);
        agriculturalScreen.searchShowResults();

        helper.testScreenShoot("SearchAgriculturalAndroidTestLoaders");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion TEST - telescopic loaders and miniloaders searching

    //region TEST - manure spreaders searching
    @Test(dataProvider = "searchManureSpreaders", description = "Searching item in manure spreaders category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search manure spreaders")
    public void testManureSpreadersSearching(String vehicleBrand,  String vehicleModel, String priceMin, String priceMax,
                                             String privacy, String currency, int third, int fourth, String yearOfProdFrom, String yearOfProdTo,
                                             String country, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat, boolean financingOption, boolean leasing, boolean rent,
                                             boolean damaged, boolean registerInPoland, boolean firstOwner, boolean noAccidents, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Rolnicze\"");

        WebDriver driver = getDriver();
        SearchAgriculturalScreen agriculturalScreen = PageFactory.initElements(driver, SearchAgriculturalScreen.class);

        agriculturalScreen.searchSetAgricultural();
        agriculturalScreen.searchApplication("Rozrzutniki obornika");
        agriculturalScreen.searchVehicleBrand(vehicleBrand);
        agriculturalScreen.searchVehicleModel(vehicleModel);
        agriculturalScreen.searchSetPrice(priceMin, priceMax);
        agriculturalScreen.searchSetPrivacy(privacy);
        agriculturalScreen.searchSetCurrency(currency);
        agriculturalScreen.searchMoreParameters();
        agriculturalScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        agriculturalScreen.searchSetCountryOfOrigin(country);
        agriculturalScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        agriculturalScreen.searchCarStatus(damaged, registerInPoland, firstOwner, noAccidents);
        agriculturalScreen.searchLocation(voivodeship);
        agriculturalScreen.searchShowResults();

        helper.testScreenShoot("SearchAgriculturalAndroidTestManureSpreaders");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion TEST - manure spreaders searching

    //region TEST - chaff-cutters searching
    @Test(dataProvider = "searchChaffCutters", description = "Searching item in telescopic chaff-cutters category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search chaff-cutters")
    public void testChaffCuttersSearching(String vehicleBrand, String vehicleModel, String priceMin, String priceMax,
                                          String privacy, String currency, int third, int fourth, String yearOfProdFrom, String yearOfProdTo,
                                          String country, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat, boolean financingOption, boolean leasing, boolean rent,
                                          boolean damaged, boolean registerInPoland, boolean firstOwner, boolean noAccidents, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Rolnicze\"");

        WebDriver driver = getDriver();
        SearchAgriculturalScreen agriculturalScreen = PageFactory.initElements(driver, SearchAgriculturalScreen.class);

        agriculturalScreen.searchSetAgricultural();
        agriculturalScreen.searchApplication("Sieczkarnie");
        agriculturalScreen.searchVehicleBrand(vehicleBrand);
        agriculturalScreen.searchVehicleModel(vehicleModel);
        agriculturalScreen.searchSetPrice(priceMin, priceMax);
        agriculturalScreen.searchSetPrivacy(privacy);
        agriculturalScreen.searchSetCurrency(currency);
        agriculturalScreen.searchMoreParameters();
        agriculturalScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        agriculturalScreen.searchSetCountryOfOrigin(country);
        agriculturalScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        agriculturalScreen.searchCarStatus(damaged, registerInPoland, firstOwner, noAccidents);
        agriculturalScreen.searchLocation(voivodeship);
        agriculturalScreen.searchShowResults();

        helper.testScreenShoot("SearchAgriculturalAndroidTestChaffCutters");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }

    //endregion TEST - chaff-cutters searching

    //region TEST - all in agricultural searching
    @Test(dataProvider = "searchAllInAgricultural", description = "Searching item in all in agricultural category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search all in agricultural")
    public void testAllInAgriculturalSearching(String vehicleBrand, String vehicleModel, String priceMin, String priceMax,
                                               String privacy, String currency, int third, int fourth,String yearOfProdFrom, String yearOfProdTo,
                                               String country, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat, boolean financingOption, boolean leasing, boolean rent,
                                               boolean damaged, boolean registerInPoland, boolean firstOwner, boolean noAccidents, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Rolnicze\"");

        WebDriver driver = getDriver();
        SearchAgriculturalScreen agriculturalScreen = PageFactory.initElements(driver, SearchAgriculturalScreen.class);

        agriculturalScreen.searchSetAgricultural();
        agriculturalScreen.searchApplication("Wszystkie w Rolnicze");
        agriculturalScreen.searchVehicleBrand(vehicleBrand);
        agriculturalScreen.searchVehicleModel(vehicleModel);
        agriculturalScreen.searchSetPrice(priceMin, priceMax);
        agriculturalScreen.searchSetPrivacy(privacy);
        agriculturalScreen.searchSetCurrency(currency);
        agriculturalScreen.searchMoreParameters();
        agriculturalScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        agriculturalScreen.searchSetCountryOfOrigin(country);
        agriculturalScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        agriculturalScreen.searchCarStatus(damaged, registerInPoland, firstOwner, noAccidents);
        agriculturalScreen.searchLocation(voivodeship);
        agriculturalScreen.searchShowResults();

        helper.testScreenShoot("SearchAgriculturalAndroidTestAllInAgr");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }

    //endregion TEST - all in agricultural searching


    //endregion TESTS

    @AfterMethod(description = "Finishing test and shutting down app")
    public void tearDown() {
        System.out.println("Koniec testu...");
    }
}
