package com.selenium.java.tests.test.android;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.MethodHelper;
import com.selenium.java.listeners.TestListener;
import com.selenium.java.pages.android.SearchCarsScreen;
import io.qameta.allure.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestContext;
import org.testng.annotations.*;

@Listeners({TestListener.class})
@Feature("SEARCHING: Car search Mobile Tests")

public class SearchCarsAndroidTest extends BaseTest {

    @BeforeClass
    public void startup() {

        startAppiumServer();
    }

    @BeforeMethod(description = "Starting appium server")
    @Parameters(value = {"Platform", "DeviceID", "DeviceName"})
    public void setUp(String platform, String deviceId, String deviceName, ITestContext context) {

        instalApp = false;
        launchAndroid(platform, deviceId, deviceName, context);
    }

    @DataProvider
    public Object[][] getCarsSearch() {
        return new Object[][]{
                {"Kompakt", "Opel", "Astra", "100", "200000", "Benzyna", "1999", "2019", "Prywatne", "PLN", "50000",
                        "480000", "1000", "2000", "70", "180", "Manualna", "na przednie", false, "ABS", "Immobilizer",
                        "ESP", "Czarny", "3", "5", "3", "5", true, false, false, false, false, false, false,
                        false, false, false, false, false, false, false, false, false, false, false, false, false,
                        "Kujawsko-pomorskie", "Bydgoszcz", "100"},
                {"Kompakt", "Audi", "A3", "100", "200000", "Benzyna", "1999", "2019", "Prywatne", "PLN", "50000",
                        "480000", "1000", "2000", "70", "180", "Manualna", "na przednie", false, "ABS", "Immobilizer",
                        "ESP", "Czarny", "3", "5", "3", "5", true, false, true, false, false, true, false,
                        true, false, true, true, false, true, false, false, true, false, true, false, true,
                        "Kujawsko-pomorskie", "Bydgoszcz", "100"}
        };
    }

    @Test(dataProvider = "getCarsSearch", description = "Searching car with all parameters")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching car")
    @Story("SEARCHING: Search car with all parameters")
    public void testNoLoginCarsSearch(String type, String brand, String model, String min_prce, String max_price,
                                      String fuel, String year_from, String year_to, String privacy, String currency,
                                      String milage_min, String milage_max, String displacment_min,
                                      String displacment_max, String power_min, String power_max, String transmission,
                                      String drive, boolean filter, String first, String second, String third,
                                      String colour, String from_seats, String to_seats, String from_doors,
                                      String to_doors, boolean metallic, boolean pearl, boolean mat, boolean acrylic,
                                      boolean english_wheel, boolean dealer, boolean margin, boolean invoice,
                                      boolean funding, boolean leasing, boolean vin, boolean registration,
                                      boolean damaged, boolean reg_in_pl, boolean first_owner, boolean collision_free,
                                      boolean aso, boolean relic, boolean tuning, boolean approval, String voivodeship,
                                      String city, String distance) {

        WebDriver driver = getDriver();
        SearchCarsScreen carsSearchScreen = PageFactory.initElements(driver, SearchCarsScreen.class);
        MethodHelper methodHelper = new MethodHelper();

        System.out.println("Checking Covid banner");

        carsSearchScreen.carsBannerClick();
        System.out.println("Covid banner checked. Now setting type");

        carsSearchScreen.carsSetTypeOfCar(type);
        System.out.println("Type set. Now setting brand");

        carsSearchScreen.carsSetBrandOfCar(brand);
        System.out.println("Brand set. Now setting model");

        carsSearchScreen.carsSetModelOfCar(model);
        System.out.println("Model set. Now setting price");

        carsSearchScreen.carsSetPrice(min_prce, max_price);
        System.out.println("Price set. Now setting year of production");

        methodHelper.waitTime(2);
        methodHelper.scrollText("Rodzaj paliwa", direction.UP, "up", 0.7, 2);
        System.out.println("Swipe to rodzaj paliwa");

        carsSearchScreen.carsSetProductionYear(year_from, year_to);
        System.out.println("Year set. Now setting fuel");

        carsSearchScreen.carsSetTypeOfFuel(fuel);
        System.out.println("Type set. Now setting privacy");

        carsSearchScreen.carsPrivacy(privacy);
        System.out.println("Fuel set. Now setting currency");

        carsSearchScreen.carsCurrency(currency);
        System.out.println("Currency set. Now click on show more");

        carsSearchScreen.carsShowMore();
        System.out.println("Show more");
        methodHelper.waitTime(3);
        methodHelper.scrollText("Kraj pochodzenia", direction.UP, "up", 0.4, 2);
        System.out.println("Scroll to kraj pochodzenia. Now setting mileage");

        carsSearchScreen.carsMileage(milage_min, milage_max);
        System.out.println("Mileage set. Now setting country of origin");

        carsSearchScreen.carsCountryOfOrigin("Niemcy");
        System.out.println("Country of origin set. Now setting displacement");

        methodHelper.waitTime(2);
        methodHelper.scrollText("Pojemność skokowa", direction.UP, "up", 0.4, 2);

        carsSearchScreen.carsDisplacement(displacment_min, displacment_max);
        System.out.println("Displacement set. Now setting engine power");

        carsSearchScreen.carsEnginePower(power_min, power_max);
        methodHelper.waitTime(2);
        System.out.println("Engine power set. Now setting transmission");

        methodHelper.scrollText("Napęd", direction.UP, "up", 0.4, 1);
        carsSearchScreen.carsTransmission(transmission);
        System.out.println("Transmission set. Now setting drive");

        carsSearchScreen.carsDrive(drive);
        System.out.println("Drive set. Now setting filter");

        carsSearchScreen.carsFilter(filter);
        System.out.println("Filter set. Now setting additional equipment");

        methodHelper.waitTime(2);
        methodHelper.scrollText("Kolor", direction.UP, "up", 0.4, 1);
        carsSearchScreen.carsAdditionalEquipment(first, second, third);
        System.out.println("Additional equipment set. Now setting colour");

        methodHelper.waitTime(2);
        methodHelper.scrollText("Liczba drzwi", direction.UP, "up", 0.4, 1);
        carsSearchScreen.carsColour(colour);
        System.out.println("Colour set. Now setting number of seats");

        carsSearchScreen.carsNumberOfSeats(from_seats, to_seats);
        System.out.println("Number of seats set. Now setting number of doors");

        carsSearchScreen.carsNumberOfDoors(from_doors, to_doors);
        System.out.println("Number of doors set. Now setting car body");

        methodHelper.waitTime(2);
        methodHelper.scrollText("Matowy", direction.UP, "up", 0.4, 1);
        carsSearchScreen.carsCarBody(metallic, pearl, mat, acrylic);
        System.out.println("Car body set. Now setting type of wheel");

        methodHelper.scrollText("Kierownica", direction.UP, "up", 0.4, 1);
        carsSearchScreen.carsEnglishWheel(english_wheel);
        System.out.println("Type of wheel set. Now setting financial information");

        methodHelper.waitTime(2);
        methodHelper.scrollText("Możliwość finansowania", direction.UP, "up", 0.4, 1);
        carsSearchScreen.carsFinancialInformation(dealer, margin, invoice, funding, leasing);
        System.out.println("Financial information set. Now setting vehicle condition");

        methodHelper.waitTime(2);
        methodHelper.scrollText("Bezwypadkowy", direction.UP, "up", 0.4, 1);
        carsSearchScreen.carsVehicleCondition(vin, registration, damaged, reg_in_pl, first_owner, collision_free, aso, relic,
                tuning, approval);
        System.out.println("Vehicle condition set. Now setting location");

        carsSearchScreen.carsSearchLocation(voivodeship, city, distance);
        System.out.println("Location set. Now showing results");

        carsSearchScreen.carsSearchShowResults();
        System.out.println("Results shown. Test passed");
   //     carsSearchScreen.carsAssert();
        methodHelper.testScreenshot("testNoLoginCarsSearch");
    }
}
