package com.selenium.java.tests.test.web;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.MethodHelper;
import com.selenium.java.listeners.TestListener;

import com.selenium.java.pages.web.SearchCarPage;
import com.selenium.java.pages.web.SearchMotorcyclePage;
import io.qameta.allure.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestContext;
import org.testng.annotations.*;

@Listeners({TestListener.class})
@Feature("WEB SEARCHING: Motorcycle search Web Tests")
public class SearchMotorcycleWebTest extends BaseTest {

    @BeforeMethod(description = "Starting appium server")
    @Parameters(value = {"Platform", "Browser"})
    public void setUp(String platform, String browser, ITestContext context) {

        headless = false;
        url = "https://www.otomoto.pl/";

        launchWeb(platform, browser, context);
        System.out.println("Test started");

    }

    @DataProvider
    public Object[][] getData() {
        return new Object[][]{
//                {"Honda", "CBR", "1000", "50000", "1999", "2019", "250", "600", "Benzyna"},
//                {"BMW", "R", "2000", "200000", "2003", "2019", "125", "750", "Benzyna"},
//                {"Yamaha", "Virago", "1000", "100000", "2005", "2019", "250", "1000", "Benzyna"},
                {"Suzuki", "DL", "11000", "1000000", "2012", "2019", "250", "1250", "Diesel"},
                {"Kawasaki", "ER", "16000", "100000", "2000", "600", "1500", "500000", "Benzyna"},
        };
    }

    @Test(dataProvider = "getData", description = "Checking correct searching items")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - correct searching items")
    @Story("SEARCHING: All inputs correct")
    public void searchTest(String brand, String model, String price_from, String price_to,
                           String year_from, String year_to, String dis_from, String dis_to, String fuel) {

        WebDriver driver = getDriver();
        MethodHelper methodHelper = PageFactory.initElements(driver, MethodHelper.class);
        SearchMotorcyclePage searchMotorcyclePage = PageFactory.initElements(driver, SearchMotorcyclePage.class);

        searchMotorcyclePage.searchWebMotorcycle();
        methodHelper.waitTime(3);
        System.out.println("Accepting cookies");
        searchMotorcyclePage.searchCookies();
        methodHelper.waitTime(2);
        searchMotorcyclePage.searchWebBrand(brand);
        System.out.println("Brand set. Now setting model");
        methodHelper.waitTime(2);
        searchMotorcyclePage.searchWebModel(model);
        System.out.println("Model set. Now setting price");
        methodHelper.waitTime(2);

        searchMotorcyclePage.searchWebPrice(price_from, price_to);
        System.out.println("Price set. Now setting year of production");

        methodHelper.waitTime(2);
        searchMotorcyclePage.searchWebYear(year_from, year_to);
        System.out.println("Year set. Now setting displacement");
        methodHelper.waitTime(2);
        searchMotorcyclePage.searchWebEngineCapacity(dis_from, dis_to);
        System.out.println("Displacement set. Now setting fuel");
        methodHelper.waitTime(2);
        searchMotorcyclePage.searchWebTypeOfFuel(fuel);
        System.out.println("Fuel set. Now showing results");
        methodHelper.waitTime(2);
        searchMotorcyclePage.searchWebShow();
        methodHelper.waitTime(2);
       searchMotorcyclePage.searchWebAssert();
        methodHelper.testScreenshot("searchTest");

    }

    @AfterMethod(description = "Finishing test and shutting down app")
    public void tearDown() {

        WebDriver driver = getDriver();

        System.out.println("Test ended");
         driver.close();
    }
}
