package com.selenium.java.tests.test.android;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.MethodHelper;
import com.selenium.java.pages.android.SearchMotorScreen;
import io.qameta.allure.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestContext;
import org.testng.annotations.*;

import static org.testng.Assert.assertTrue;


@Feature("SEARCHING: Motor search Mobile Tests")
public class SearchMotorAndroidTest extends BaseTest {
    @BeforeClass
    public void startup() {

        startAppiumServer();
    }

    @BeforeMethod(description = "Starting appium server")
    @Parameters(value = {"Platform", "DeviceID", "DeviceName"})
    public void setUp(String platform, String deviceId, String deviceName, ITestContext context) {

        instalApp = false;

        launchAndroid(platform, deviceId, deviceName, context);
    }

    //region DATAPROVIDERS

    //region DATAPROVIDER - search motors
    @DataProvider
    public Object[][] searchMotors() {
        return new Object[][]{

                {"Harley-Davidson", "Fat Boy","Chopper", "50000", "70000", "1200", "1800", "Prywatne", "Wszystkie", "1995", "2019",
                        "5000", "10000", "Benzyna", "Stany Zjednoczone",  "80", "100", "Pas napędowy", "Manualna", "Immobilizer", "Czterosuwowy",
                        false, true, false, "Srebrny",false, false, false, true, false, false,  false, false, false, false, false, true, false, false, "Wielkopolskie"},
//                {"CF Moto", "Inny","Quad", "20000", "25000", "200", "600","Prywatne", "Wszystkie", "2015", "2019",
//                        "50000", "200000", "Benzyna", "Polska",  "20", "50", "Wał kardana", "Automatyczna", "Kufer", "Czterosuwowy",
//                        true, false, true, "Czerwony",false, false, false, true, false, false,  true, false, false, true, true, true, true, true, "Mazowieckie"},
//                {"Honda", "CRF","Sportowy", "2000", "15000", "232", "232","Prywatne", "Wszystkie", "2015", "2019",
//                        "50000", "200000", "Benzyna", "Polska",  "454", "554", "Pas napędowy", "Automatyczna", "Kufer", "Dwusuwowy",
//                        true, false, true, "Czarny",false, false, false, true, false, false,  true, false, false, true, true, true, true, true, "Dolnośląskie"}
        };
    }

    //endregion DATAPROVIDER - search motors
    //endregion DATAPROVIDERS



    //region TESTS
    MethodHelper helper = new MethodHelper();
    //region TEST - motors searching
    @Test(dataProvider = "searchMotors", description = "Searching item in motors category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search motors")
    public void testMotorsSearching(String vehicleBrand, String vehicleModel,String type,  String priceMin, String priceMax,
                                                   String cubicCapacityMin, String cubicCapacityMax, String privacy,
                                                  String currency,String yearOfProdFrom, String yearOfProdTo,
                                                   String mileageMin, String mileageMax, String fuel, String country,
                                                  String powerMin, String powerMax,String typeOfDrive, String gearbox, String addEquip, String engineType,
                                                  boolean metalic, boolean pearly, boolean mat, String colour, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat,
                                                  boolean financingOption, boolean leasing, boolean rent, boolean registrationNumber, boolean offersWithVin, boolean damaged, boolean registerInPoland,
                                                  boolean noAccidents, boolean servisedASO, boolean tunning, boolean antique, String voivodeship) {

        System.out.println("Starting the search test in the \"Motorcycles\"");
        WebDriver driver = getDriver();
        SearchMotorScreen motorScreen = PageFactory.initElements(driver, SearchMotorScreen.class);



        motorScreen.searchSetMotors();
        motorScreen.searchVehicleBrand(vehicleBrand);
        motorScreen.searchVehicleModel(vehicleModel);
        motorScreen.searchSetType(type);
        motorScreen.searchSetPrice(priceMin, priceMax);
        motorScreen.searchSetCubicCapacity(cubicCapacityMin, cubicCapacityMax);
        motorScreen.searchSetPrivacy(privacy);
        motorScreen.searchSetCurrency(currency);
        motorScreen.searchMoreParameters();
        motorScreen.searchSetYearOfProduction(yearOfProdFrom, yearOfProdTo);
        motorScreen.searchSetMileage(mileageMin, mileageMax);
        motorScreen.searchSetTypeOfFuel(fuel);
        motorScreen.searchSetCountryOfOrigin(country);
     //   motorScreen.searchSetPower(powerMin, powerMax);
        motorScreen.searchSetTypeOfDrive(typeOfDrive);
        motorScreen.searchSetGearbox(gearbox);
        motorScreen.searchSetAdditionalEquipment(addEquip);
        motorScreen.searchSetEngineType(engineType);
        motorScreen.searchSetMetalic(metalic);
        motorScreen.searchSetPearly(pearly);
        motorScreen.searchSetMat(mat);
        motorScreen.searchSetColour(colour);
        motorScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        motorScreen.searchCarStatus(registrationNumber, offersWithVin, damaged, registerInPoland, noAccidents, servisedASO, tunning, antique);
     //   motorScreen.searchLocation(voivodeship);
        motorScreen.searchShowResults();

        helper.testScreenShoot("SearchMotorAndroidTest");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test passed - searched items were found according to specified parameters");
    }

    //region TEST - motors searching

    //endregion TESTS

    @AfterMethod(description = "Finishing test and shutting down app")
    public void tearDown() {
        System.out.println("End of the test...");
    }

}
