package com.selenium.java.tests.test.android;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.MethodHelper;
import com.selenium.java.pages.android.SearchTrailersScreen;
import io.qameta.allure.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestContext;
import org.testng.annotations.*;

import static org.testng.Assert.assertTrue;


@Feature("SEARCHING: Trailers search Mobile Tests")
public class SearchTrailersAndroidTest extends BaseTest {


    @BeforeClass
    public void startup() {

        startAppiumServer();
    }

    @BeforeMethod(description = "Starting appium server")
    @Parameters(value = {"Platform", "DeviceID", "DeviceName"})
    public void setUp(String platform, String deviceId, String deviceName, ITestContext context) {

        instalApp = false;

        launchAndroid(platform, deviceId, deviceName, context);
    }

    //region DATAPROVIDERS
    //region DATAPROVIDER - search other trailers
    @DataProvider
    public Object[][] searchOtherTrailers() {
        return new Object[][]{
                {"MAN", "", "2000", "20000", "Firma", "", 2, 3, "2010", "2017", "Polska", 4, 5, "", "",
                        6, 7, "", "", 8, 9, "", "", true, 10, 11, "1000", "3000", true, false, true, false, false, false, false, true, false, true, "Małopolskie"}
        };
    }


    //endregion DATAPROVIDER - search other trailers

    //region DATAPROVIDER - search caravanas
    @DataProvider
    public Object[][] searchCaravanas() {
        return new Object[][]{
                {"Renault", "", "2000", "20000", "Firma", "", 2, 3, "2010", "2017", "Polska", 4, 5, "", "",
                        8, 9, "", "", true, 10, 11, "1000", "3000", true, false, true, false, false, false, false, true, false, true, "Małopolskie"}
        };
    }


    //endregion DATAPROVIDER - search caravanas

    //region DATAPROVIDER - search other semi-trailers
    @DataProvider
    public Object[][] searchOtherSemiTrailers() {
        return new Object[][]{
                {"Renault", "", "2000", "20000", "Firma", "", 2, 3, "2010", "2017", "Platforma", "Polska", 4, 5, "", "",
                        8, 9, "", "", true, 10, 11, "1000", "3000", true, false, true, false, false, false, false, true, false, true, "Małopolskie"}
        };
    }


    //endregion DATAPROVIDER - search other semi-trailers

    //region DATAPROVIDER - search trailers for passenger cars
    @DataProvider
    public Object[][] searchTrailersForCars() {
        return new Object[][]{
                {"Renault", "", "2000", "20000", "Firma", "", 2, 3, "2010", "2017", "Polska", 4, 5, "", "",
                        8, 9, "", "", true, 10, 11, "1000", "3000", true, false, true, false, false, false, false, true, false, true, "Małopolskie"}
        };
    }


    //endregion DATAPROVIDER - search trailers for passenger cars

    //region DATAPROVIDER - search trippers trailers
    @DataProvider
    public Object[][] searchTrippersTrailers() {
        return new Object[][]{
                {"Renault", "", "2000", "20000", "Firma", "", 2, 3, "2010", "2017", "Polska", 4, 5, "", "",
                        8, 9, "", "", true, 10, 11, "1000", "3000", true, false, true, false, false, false, false, true, false, true, "Małopolskie"}
        };
    }
    //endregion DATAPROVIDER - search trippers trailers

    //region DATAPROVIDER - search flatbed semi-trailers and taurpaulins
    @DataProvider
    public Object[][] searchFlatbedSemiTrailersAndTarpaulins() {
        return new Object[][]{
                {"Renault", "", "2000", "20000", "Firma", "", 2, 3, "2010", "2017", "Polska", 4, 5, "", "",
                        8, 9, "", "", true, 10, 11, "1000", "3000", true, false, true, false, false, false, false, true, false, true, "Małopolskie"}
        };
    }
    //endregion DATAPROVIDER - search flatbed semi-trailers and taurpaulins

    //region DATAPROVIDER - search refrigerated semi-trailers and isotherms
    @DataProvider
    public Object[][] searchRefrigeratedSemiTrailersAndIsotherms() {
        return new Object[][]{
                {"Renault", "", "2000", "20000", "Firma", "", 2, 3, "2010", "2017", "Polska", 4, 5, "", "",
                        8, 9, "", "", true, 10, 11, "1000", "3000", true, false, true, false, false, false, false, true, false, true, "Małopolskie"}
        };
    }
    //endregion DATAPROVIDER - search refrigerated semi-trailers and isotherms

    //region DATAPROVIDER - search tank semi-trailers
    @DataProvider
    public Object[][] searchTankSemiTrailers() {
        return new Object[][]{
                {"Renault", "", "2000", "20000", "Firma", "", 2, 3, "2010", "2017", "Polska", 4, 5, "", "",
                        8, 9, "", "", true, 10, 11, "1000", "3000", true, false, true, false, false, false, false, true, false, true, "Małopolskie"}
        };
    }
    //endregion DATAPROVIDER - search tank semi-trailers

    //region DATAPROVIDER - search vehicle transport semi-trailers
    @DataProvider
    public Object[][] searchVehicleTransportSemiTrailers() {
        return new Object[][]{
                {"Renault", "", "2000", "20000", "Firma", "", 2, 3, "2010", "2017", "Polska", 4, 5, "", "",
                        8, 9, "", "", true, 10, 11, "1000", "3000", true, false, true, false, false, false, false, true, false, true, "Małopolskie"}
        };
    }
    //endregion DATAPROVIDER - search vehicle transport semi-trailers

    //region DATAPROVIDER - search low floor semi-trailers
    @DataProvider
    public Object[][] searchLowFloorSemiTrailers() {
        return new Object[][]{
                {"Renault", "", "2000", "20000", "Firma", "", 2, 3, "2010", "2017", "Polska", 4, 5, "", "",
                        8, 9, "", "", true, 10, 11, "1000", "3000", true, false, true, false, false, false, false, true, false, true, "Małopolskie"}
        };
    }
    //endregion DATAPROVIDER - search low floor semi-trailers

    //region DATAPROVIDER - search container semi-trailers
    @DataProvider
    public Object[][] searchContainerSemiTrailers() {
        return new Object[][]{
                {"Renault", "", "2000", "20000", "Firma", "", 2, 3, "2010", "2017", "Polska", 4, 5, "", "",
                        8, 9, "", "", true, 10, 11, "1000", "3000", true, false, true, false, false, false, false, true, false, true, "Małopolskie"}
        };
    }
    //endregion DATAPROVIDER - search container semi-trailers

    //region DATAPROVIDER - search trippers semi-trailers
    @DataProvider
    public Object[][] searchTrippersSemiTrailers() {
        return new Object[][]{
                {"Renault", "", "2000", "20000", "Firma", "", 2, 3, "2010", "2017", "Polska", 4, 5, "", "",
                        8, 9, "", "", true, 10, 11, "1000", "3000", true, false, true, false, false, false, false, true, false, true, "Małopolskie"}
        };
    }
    //endregion DATAPROVIDER - search trippers semi-trailers

    //region DATAPROVIDER - search flatbed trailers and taurpaulins
    @DataProvider
    public Object[][] searchFlatbedTrailersAndTarpaulins() {
        return new Object[][]{
                {"Renault", "", "2000", "20000", "Firma", "", 2, 3, "2010", "2017", "Polska", 4, 5, "", "",
                        8, 9, "", "", true, 10, 11, "1000", "3000", true, false, true, false, false, false, false, true, false, true, "Małopolskie"}
        };
    }
    //endregion DATAPROVIDER - search flatbed trailers and taurpaulins

    //region DATAPROVIDER - search refrigerated trailers and isotherms
    @DataProvider
    public Object[][] searchRefrigeratedTrailersAndIsotherms() {
        return new Object[][]{
                {"Renault", "", "2000", "20000", "Firma", "", 2, 3, "2010", "2017", "Polska", 4, 5, "", "",
                        8, 9, "", "", true, 10, 11, "1000", "3000", true, false, true, false, false, false, false, true, false, true, "Małopolskie"}
        };
    }
    //endregion DATAPROVIDER - search refrigerated trailers and isotherms

    //region DATAPROVIDER - search vehicle transport trailers
    @DataProvider
    public Object[][] searchVehicleTransportTrailers() {
        return new Object[][]{
                {"Renault", "", "2000", "20000", "Firma", "", 2, 3, "2010", "2017", "Polska", 4, 5, "", "",
                        8, 9, "", "", true, 10, 11, "1000", "3000", true, false, true, false, false, false, false, true, false, true, "Małopolskie"}
        };
    }
    //endregion DATAPROVIDER - search vehicle transport trailers

    //region DATAPROVIDER - search tank trailers
    @DataProvider
    public Object[][] searchTankTrailers() {
        return new Object[][]{
                {"Renault", "", "2000", "20000", "Firma", "", 2, 3, "2010", "2017", "Polska", 4, 5, "", "",
                        8, 9, "", "", true, 10, 11, "1000", "3000", true, false, true, false, false, false, false, true, false, true, "Małopolskie"}
        };
    }
    //endregion DATAPROVIDER - search tank trailers

    //region DATAPROVIDER - search container trailers
    @DataProvider
    public Object[][] searchContainerTrailers() {
        return new Object[][]{
                {"Renault", "", "2000", "20000", "Firma", "", 2, 3, "2010", "2017", "Polska", 4, 5, "", "",
                        8, 9, "", "", true, 10, 11, "1000", "3000", true, false, true, false, false, false, false, true, false, true, "Małopolskie"}
        };
    }
    //endregion DATAPROVIDER - search container trailers

    //region DATAPROVIDER - search catering trailers
    @DataProvider
    public Object[][] searchCateringTrailers() {
        return new Object[][]{
                {"Renault", "", "2000", "20000", "Firma", "", 2, 3, "2010", "2017", "Polska", 4, 5, "", "",
                        8, 9, "", "", true, 10, 11, "1000", "3000", true, false, true, false, false, false, false, true, false, true, "Małopolskie"}
        };
    }
    //endregion DATAPROVIDER - search catering trailers

    //region DATAPROVIDER - search trailers accessories
    @DataProvider
    public Object[][] searchTrailersAccessories() {
        return new Object[][]{
                {"Renault", "", "2000", "20000", "Firma", "", 2, 3, "2010", "2017", "Polska", 8, 9, "", "",
                        true, 10, 11, "1000", "3000", true, false, true, false, false, false, false, true, false, true, "Małopolskie"}
        };
    }
    //endregion DATAPROVIDER - search trailers accessories

    //region DATAPROVIDER - search all in trailers
    @DataProvider
    public Object[][] searchAllTrailers() {
        return new Object[][]{
                {"Renault", "", "2000", "20000", "Firma", "", 2, 3, "2010", "2017", "Polska", 8, 9, "", "",
                        true, 10, 11, "1000", "3000", true, false, true, false, false, false, false, true, false, true, "Małopolskie"}
        };
    }
    //endregion DATAPROVIDER - search all in trailers


    //endregion DATAPROVIDERS

    //region TESTS
    MethodHelper helper = new MethodHelper();
    //region TEST - other trailers searching
    @Test(dataProvider = "searchOtherTrailers", description = "Searching item in Other Trailers category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search other trailers")
    public void testOtherTrailersSearching(String vehicleBrand, String vehicleModel,
                                           String priceMin, String priceMax, String privacy, String currency, int third, int fourth,
                                           String yearOfProdFrom, String yearOfProdTo, String country, int fifth, int sixth, String packageMin,
                                           String packageMax, int seventh, int eighth, String capacityMin, String capacityMax,
                                           int ninth, int tenth, String axlesMin, String axlesMax, boolean doubleWheels, int eleventh, int twelfth,
                                           String grossWeightMin, String grossWeightMax, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat, boolean financingOption, boolean leasing, boolean rent,
                                           boolean damaged, boolean registerInPoland, boolean firstOwner, boolean noAccidents, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Przyczepy\"");
        WebDriver driver = getDriver();
        SearchTrailersScreen trailersScreen = PageFactory.initElements(driver, SearchTrailersScreen.class);

        trailersScreen.searchSetTrailers();
        trailersScreen.searchApplication("Przyczepy pozostałe");
        trailersScreen.searchVehicleBrand(vehicleBrand);
        trailersScreen.searchVehicleModel(vehicleModel);
        trailersScreen.searchSetPrice(priceMin, priceMax);
        trailersScreen.searchSetPrivacy(privacy);
        trailersScreen.searchSetCurrency(currency);
        trailersScreen.searchMoreParameters();
        trailersScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        trailersScreen.searchSetCountryOfOrigin(country);
        trailersScreen.searchSetAllowedPackage(4, 5, packageMin, packageMax);
        trailersScreen.searchSetCapacity(6, 7, capacityMin, capacityMax);
        trailersScreen.searchSetNumberOfAxles(8, 9, axlesMin, axlesMax);
        trailersScreen.searchSetDoubleRearWheels(doubleWheels);
        trailersScreen.searchSetPermissibleGrossWeight(10, 11, grossWeightMin, grossWeightMax);
        trailersScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        trailersScreen.searchCarStatus(damaged, registerInPoland, firstOwner, noAccidents);
        trailersScreen.searchLocation(voivodeship);
        trailersScreen.searchShowResults();

        helper.testScreenShoot("SearchTrailersAndroidTestOtherTrailers");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");

    }
    //endregion other trailers searching

    //region TEST - caravanas searching
    @Test(dataProvider = "searchCaravanas", description = "Searching item in caravanas category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search caravanas")
    public void testCaravanasSearching(String vehicleBrand, String vehicleModel,
                                       String priceMin, String priceMax, String privacy, String currency, int third, int fourth,
                                       String yearOfProdFrom, String yearOfProdTo, String country, int fifth, int sixth, String packageMin,
                                       String packageMax, int ninth, int tenth, String axlesMin, String axlesMax, boolean doubleWheels, int eleventh, int twelfth,
                                       String grossWeightMin, String grossWeightMax, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat, boolean financingOption, boolean leasing, boolean rent,
                                       boolean damaged, boolean registerInPoland, boolean firstOwner, boolean noAccidents, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Przyczepy\"");
        WebDriver driver = getDriver();
        SearchTrailersScreen trailersScreen = PageFactory.initElements(driver, SearchTrailersScreen.class);

        trailersScreen.searchSetTrailers();
        trailersScreen.searchApplication("Przyczepy kempingowe");
        trailersScreen.searchVehicleBrand(vehicleBrand);
        trailersScreen.searchVehicleModel(vehicleModel);
        trailersScreen.searchSetPrice(priceMin, priceMax);
        trailersScreen.searchSetPrivacy(privacy);
        trailersScreen.searchSetCurrency(currency);
        trailersScreen.searchMoreParameters();
        trailersScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        trailersScreen.searchSetCountryOfOrigin(country);
        trailersScreen.searchSetAllowedPackage(4, 5, packageMin, packageMax);
        trailersScreen.searchSetNumberOfAxles(8, 9, axlesMin, axlesMax);
        trailersScreen.searchSetDoubleRearWheels(doubleWheels);
        trailersScreen.searchSetPermissibleGrossWeight(10, 11, grossWeightMin, grossWeightMax);
        trailersScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        trailersScreen.searchCarStatus(damaged, registerInPoland, firstOwner, noAccidents);
        trailersScreen.searchLocation(voivodeship);
        trailersScreen.searchShowResults();

        helper.testScreenShoot("SearchTrailersAndroidTestCaravanas");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");

    }
    //endregion caravanas searching

    //region TEST - other semi-trailers searching
    @Test(dataProvider = "searchOtherSemiTrailers", description = "Searching item in other semi-trailers category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search other semi-trailers")
    public void testOtherSemiTrailersSearching(String vehicleBrand, String vehicleModel,
                                               String priceMin, String priceMax, String privacy, String currency, int third, int fourth,
                                               String yearOfProdFrom, String yearOfProdTo, String vehicleType, String country, int fifth, int sixth, String packageMin,
                                               String packageMax, int ninth, int tenth, String axlesMin, String axlesMax, boolean doubleWheels, int eleventh, int twelfth,
                                               String grossWeightMin, String grossWeightMax, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat, boolean financingOption, boolean leasing, boolean rent,
                                               boolean damaged, boolean registerInPoland, boolean firstOwner, boolean noAccidents, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Przyczepy\"");
        WebDriver driver = getDriver();
        SearchTrailersScreen trailersScreen = PageFactory.initElements(driver, SearchTrailersScreen.class);

        trailersScreen.searchSetTrailers();
        trailersScreen.searchApplication("Naczepy pozostałe");
        trailersScreen.searchVehicleBrand(vehicleBrand);
        trailersScreen.searchVehicleModel(vehicleModel);
        trailersScreen.searchSetPrice(priceMin, priceMax);
        trailersScreen.searchSetPrivacy(privacy);
        trailersScreen.searchSetCurrency(currency);
        trailersScreen.searchMoreParameters();
        trailersScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        trailersScreen.searchVehicleType(vehicleType);
        trailersScreen.searchSetCountryOfOrigin(country);
        trailersScreen.searchSetAllowedPackage(4, 5, packageMin, packageMax);
        trailersScreen.searchSetNumberOfAxles(8, 9, axlesMin, axlesMax);
        trailersScreen.searchSetDoubleRearWheels(doubleWheels);
        trailersScreen.searchSetPermissibleGrossWeight(10, 11, grossWeightMin, grossWeightMax);
        trailersScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        trailersScreen.searchCarStatus(damaged, registerInPoland, firstOwner, noAccidents);
        trailersScreen.searchLocation(voivodeship);
        trailersScreen.searchShowResults();

        helper.testScreenShoot("SearchTrailersAndroidTestOtherSemiTrailers");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");

    }
    //endregion other semi-trailers searching

    //region TEST - trailers for passenger cars searching
    @Test(dataProvider = "searchTrailersForCars", description = "Searching item in trailers for passenger cars category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search trailers for passenger cars")
    public void testTrailersForCarsSearching(String vehicleBrand, String vehicleModel,
                                             String priceMin, String priceMax, String privacy, String currency, int third, int fourth,
                                             String yearOfProdFrom, String yearOfProdTo, String country, int fifth, int sixth, String packageMin,
                                             String packageMax, int ninth, int tenth, String axlesMin, String axlesMax, boolean doubleWheels, int eleventh, int twelfth,
                                             String grossWeightMin, String grossWeightMax, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat, boolean financingOption, boolean leasing, boolean rent,
                                             boolean damaged, boolean registerInPoland, boolean firstOwner, boolean noAccidents, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Przyczepy\"");
        WebDriver driver = getDriver();
        SearchTrailersScreen trailersScreen = PageFactory.initElements(driver, SearchTrailersScreen.class);

        trailersScreen.searchSetTrailers();
        trailersScreen.searchApplication("Przyczepy do samochodów osobowych");
        trailersScreen.searchVehicleBrand(vehicleBrand);
        trailersScreen.searchVehicleModel(vehicleModel);
        trailersScreen.searchSetPrice(priceMin, priceMax);
        trailersScreen.searchSetPrivacy(privacy);
        trailersScreen.searchSetCurrency(currency);
        trailersScreen.searchMoreParameters();
        trailersScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        trailersScreen.searchSetCountryOfOrigin(country);
        trailersScreen.searchSetAllowedPackage(4, 5, packageMin, packageMax);
        trailersScreen.searchSetNumberOfAxles(8, 9, axlesMin, axlesMax);
        trailersScreen.searchSetDoubleRearWheels(doubleWheels);
        trailersScreen.searchSetPermissibleGrossWeight(10, 11, grossWeightMin, grossWeightMax);
        trailersScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        trailersScreen.searchCarStatus(damaged, registerInPoland, firstOwner, noAccidents);
        trailersScreen.searchLocation(voivodeship);
        trailersScreen.searchShowResults();

        helper.testScreenShoot("SearchTrailersAndroidTestTrailersForCars");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");

    }
    //endregion trailers for passenger cars searching

    //region TEST - trippers trailers searching
    @Test(dataProvider = "searchTrippersTrailers", description = "Searching item in trippers trailers category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search trippers trailers ")
    public void testTrippersTrailersSearching(String vehicleBrand, String vehicleModel,
                                              String priceMin, String priceMax, String privacy, String currency, int third, int fourth,
                                              String yearOfProdFrom, String yearOfProdTo, String country, int fifth, int sixth, String packageMin,
                                              String packageMax, int ninth, int tenth, String axlesMin, String axlesMax, boolean doubleWheels, int eleventh, int twelfth,
                                              String grossWeightMin, String grossWeightMax, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat, boolean financingOption, boolean leasing, boolean rent,
                                              boolean damaged, boolean registerInPoland, boolean firstOwner, boolean noAccidents, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Przyczepy\"");
        WebDriver driver = getDriver();
        SearchTrailersScreen trailersScreen = PageFactory.initElements(driver, SearchTrailersScreen.class);

        trailersScreen.searchSetTrailers();
        trailersScreen.searchApplication("Przyczepy wywrotki");
        trailersScreen.searchVehicleBrand(vehicleBrand);
        trailersScreen.searchVehicleModel(vehicleModel);
        trailersScreen.searchSetPrice(priceMin, priceMax);
        trailersScreen.searchSetPrivacy(privacy);
        trailersScreen.searchSetCurrency(currency);
        trailersScreen.searchMoreParameters();
        trailersScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        trailersScreen.searchSetCountryOfOrigin(country);
        trailersScreen.searchSetAllowedPackage(4, 5, packageMin, packageMax);
        trailersScreen.searchSetNumberOfAxles(8, 9, axlesMin, axlesMax);
        trailersScreen.searchSetDoubleRearWheels(doubleWheels);
        trailersScreen.searchSetPermissibleGrossWeight(10, 11, grossWeightMin, grossWeightMax);
        trailersScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        trailersScreen.searchCarStatus(damaged, registerInPoland, firstOwner, noAccidents);
        trailersScreen.searchLocation(voivodeship);
        trailersScreen.searchShowResults();

        helper.testScreenShoot("SearchTrailersAndroidTestTrippersTrailers");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");

    }
    //endregion trippers trailers searching

    //region TEST - flatbed semitrailers and tarpaulins searching
    @Test(dataProvider = "searchFlatbedSemiTrailersAndTarpaulins", description = "Searching item in flatbed semitrailers and tarpaulins category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search flatbed semitrailers and tarpaulins ")
    public void testFlatbedSemiTrailersAndTarpaulinsSearching(String vehicleBrand, String vehicleModel,
                                                              String priceMin, String priceMax, String privacy, String currency, int third, int fourth,
                                                              String yearOfProdFrom, String yearOfProdTo, String country, int fifth, int sixth, String packageMin,
                                                              String packageMax, int ninth, int tenth, String axlesMin, String axlesMax, boolean doubleWheels, int eleventh, int twelfth,
                                                              String grossWeightMin, String grossWeightMax, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat, boolean financingOption, boolean leasing, boolean rent,
                                                              boolean damaged, boolean registerInPoland, boolean firstOwner, boolean noAccidents, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Przyczepy\"");
        WebDriver driver = getDriver();
        SearchTrailersScreen trailersScreen = PageFactory.initElements(driver, SearchTrailersScreen.class);

        trailersScreen.searchSetTrailers();
        trailersScreen.searchApplication("Naczepy burtowe i plandeki");
        trailersScreen.searchVehicleBrand(vehicleBrand);
        trailersScreen.searchVehicleModel(vehicleModel);
        trailersScreen.searchSetPrice(priceMin, priceMax);
        trailersScreen.searchSetPrivacy(privacy);
        trailersScreen.searchSetCurrency(currency);
        trailersScreen.searchMoreParameters();
        trailersScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        trailersScreen.searchSetCountryOfOrigin(country);
        trailersScreen.searchSetAllowedPackage(4, 5, packageMin, packageMax);
        trailersScreen.searchSetNumberOfAxles(8, 9, axlesMin, axlesMax);
        trailersScreen.searchSetDoubleRearWheels(doubleWheels);
        trailersScreen.searchSetPermissibleGrossWeight(10, 11, grossWeightMin, grossWeightMax);
        trailersScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        trailersScreen.searchCarStatus(damaged, registerInPoland, firstOwner, noAccidents);
        trailersScreen.searchLocation(voivodeship);
        trailersScreen.searchShowResults();

        helper.testScreenShoot("SearchTrailersAndroidTestFlatbedSemiTrailers");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");

    }
    //endregion flatbed semitrailers and tarpaulins searching

    //region TEST - refrigerated semi-trailers and isotherms searching
    @Test(dataProvider = "searchRefrigeratedSemiTrailersAndIsotherms", description = "Searching item in refrigerated semi-trailers and isotherms category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search refrigerated semi-trailers and isotherms")
    public void testRefrigeratedSemiTrailersAndIsothermsSearching(String vehicleBrand, String vehicleModel,
                                                                  String priceMin, String priceMax, String privacy, String currency, int third, int fourth,
                                                                  String yearOfProdFrom, String yearOfProdTo, String country, int fifth, int sixth, String packageMin,
                                                                  String packageMax, int ninth, int tenth, String axlesMin, String axlesMax, boolean doubleWheels, int eleventh, int twelfth,
                                                                  String grossWeightMin, String grossWeightMax, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat, boolean financingOption, boolean leasing, boolean rent,
                                                                  boolean damaged, boolean registerInPoland, boolean firstOwner, boolean noAccidents, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Przyczepy\"");
        WebDriver driver = getDriver();
        SearchTrailersScreen trailersScreen = PageFactory.initElements(driver, SearchTrailersScreen.class);

        trailersScreen.searchSetTrailers();
        trailersScreen.searchApplication("Naczepy chłodnie i izotermy");
        trailersScreen.searchVehicleBrand(vehicleBrand);
        trailersScreen.searchVehicleModel(vehicleModel);
        trailersScreen.searchSetPrice(priceMin, priceMax);
        trailersScreen.searchSetPrivacy(privacy);
        trailersScreen.searchSetCurrency(currency);
        trailersScreen.searchMoreParameters();
        trailersScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        trailersScreen.searchSetCountryOfOrigin(country);
        trailersScreen.searchSetAllowedPackage(4, 5, packageMin, packageMax);
        trailersScreen.searchSetNumberOfAxles(8, 9, axlesMin, axlesMax);
        trailersScreen.searchSetDoubleRearWheels(doubleWheels);
        trailersScreen.searchSetPermissibleGrossWeight(10, 11, grossWeightMin, grossWeightMax);
        trailersScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        trailersScreen.searchCarStatus(damaged, registerInPoland, firstOwner, noAccidents);
        trailersScreen.searchLocation(voivodeship);
        trailersScreen.searchShowResults();

        helper.testScreenShoot("SearchTrailersAndroidTestRefrigeratedSemiTrailers");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");

    }
    //endregion refrigerated semi-trailers and isotherms searching

    //region TEST - tank semi-trailers searching
    @Test(dataProvider = "searchTankSemiTrailers", description = "Searching item in tank semi-trailers category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search tank semi-trailers")
    public void testTankSemiTrailersSearching(String vehicleBrand, String vehicleModel,
                                              String priceMin, String priceMax, String privacy, String currency, int third, int fourth,
                                              String yearOfProdFrom, String yearOfProdTo, String country, int fifth, int sixth, String packageMin,
                                              String packageMax, int ninth, int tenth, String axlesMin, String axlesMax, boolean doubleWheels, int eleventh, int twelfth,
                                              String grossWeightMin, String grossWeightMax, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat, boolean financingOption, boolean leasing, boolean rent,
                                              boolean damaged, boolean registerInPoland, boolean firstOwner, boolean noAccidents, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Przyczepy\"");
        WebDriver driver = getDriver();
        SearchTrailersScreen trailersScreen = PageFactory.initElements(driver, SearchTrailersScreen.class);

        trailersScreen.searchSetTrailers();
        trailersScreen.searchApplication("Naczepy cysterny");
        trailersScreen.searchVehicleBrand(vehicleBrand);
        trailersScreen.searchVehicleModel(vehicleModel);
        trailersScreen.searchSetPrice(priceMin, priceMax);
        trailersScreen.searchSetPrivacy(privacy);
        trailersScreen.searchSetCurrency(currency);
        trailersScreen.searchMoreParameters();
        trailersScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        trailersScreen.searchSetCountryOfOrigin(country);
        trailersScreen.searchSetAllowedPackage(4, 5, packageMin, packageMax);
        trailersScreen.searchSetNumberOfAxles(8, 9, axlesMin, axlesMax);
        trailersScreen.searchSetDoubleRearWheels(doubleWheels);
        trailersScreen.searchSetPermissibleGrossWeight(10, 11, grossWeightMin, grossWeightMax);
        trailersScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        trailersScreen.searchCarStatus(damaged, registerInPoland, firstOwner, noAccidents);
        trailersScreen.searchLocation(voivodeship);
        trailersScreen.searchShowResults();

        helper.testScreenShoot("SearchTrailersAndroidTestTankSemiTrailers");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");

    }
    //endregion tank semi-trailers searching

    //region TEST - vehicle transport semi-trailers searching
    @Test(dataProvider = "searchVehicleTransportSemiTrailers", description = "Searching item in vehicle transport semi-trailers category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search vehicle transport semi-trailers")
    public void testVehicleTransportSemiTrailersSearching(String vehicleBrand, String vehicleModel,
                                                          String priceMin, String priceMax, String privacy, String currency, int third, int fourth,
                                                          String yearOfProdFrom, String yearOfProdTo, String country, int fifth, int sixth, String packageMin,
                                                          String packageMax, int ninth, int tenth, String axlesMin, String axlesMax, boolean doubleWheels, int eleventh, int twelfth,
                                                          String grossWeightMin, String grossWeightMax, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat, boolean financingOption, boolean leasing, boolean rent,
                                                          boolean damaged, boolean registerInPoland, boolean firstOwner, boolean noAccidents, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Przyczepy\"");
        WebDriver driver = getDriver();
        SearchTrailersScreen trailersScreen = PageFactory.initElements(driver, SearchTrailersScreen.class);

        trailersScreen.searchSetTrailers();
        trailersScreen.searchApplication("Naczepy do przewozu pojazdów");
        trailersScreen.searchVehicleBrand(vehicleBrand);
        trailersScreen.searchVehicleModel(vehicleModel);
        trailersScreen.searchSetPrice(priceMin, priceMax);
        trailersScreen.searchSetPrivacy(privacy);
        trailersScreen.searchSetCurrency(currency);
        trailersScreen.searchMoreParameters();
        trailersScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        trailersScreen.searchSetCountryOfOrigin(country);
        trailersScreen.searchSetAllowedPackage(4, 5, packageMin, packageMax);
        trailersScreen.searchSetNumberOfAxles(8, 9, axlesMin, axlesMax);
        trailersScreen.searchSetDoubleRearWheels(doubleWheels);
        trailersScreen.searchSetPermissibleGrossWeight(10, 11, grossWeightMin, grossWeightMax);
        trailersScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        trailersScreen.searchCarStatus(damaged, registerInPoland, firstOwner, noAccidents);
        trailersScreen.searchLocation(voivodeship);
        trailersScreen.searchShowResults();

        helper.testScreenShoot("SearchTrailersAndroidTestVehicleTransportSemiTrailers");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");

    }
    //endregion vehicle transport semi-trailers searching

    //region TEST - low floor semi-trailers searching
    @Test(dataProvider = "searchLowFloorSemiTrailers", description = "Searching item in low floor semi-trailers category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search low floor semi-trailers")
    public void testLowFloorSemiTrailersSearching(String vehicleBrand, String vehicleModel,
                                                  String priceMin, String priceMax, String privacy, String currency, int third, int fourth,
                                                  String yearOfProdFrom, String yearOfProdTo, String country, int fifth, int sixth, String packageMin,
                                                  String packageMax, int ninth, int tenth, String axlesMin, String axlesMax, boolean doubleWheels, int eleventh, int twelfth,
                                                  String grossWeightMin, String grossWeightMax, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat, boolean financingOption, boolean leasing, boolean rent,
                                                  boolean damaged, boolean registerInPoland, boolean firstOwner, boolean noAccidents, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Przyczepy\"");
        WebDriver driver = getDriver();
        SearchTrailersScreen trailersScreen = PageFactory.initElements(driver, SearchTrailersScreen.class);

        trailersScreen.searchSetTrailers();
        trailersScreen.searchApplication("Naczepy niskopodłogowe");
        trailersScreen.searchVehicleBrand(vehicleBrand);
        trailersScreen.searchVehicleModel(vehicleModel);
        trailersScreen.searchSetPrice(priceMin, priceMax);
        trailersScreen.searchSetPrivacy(privacy);
        trailersScreen.searchSetCurrency(currency);
        trailersScreen.searchMoreParameters();
        trailersScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        trailersScreen.searchSetCountryOfOrigin(country);
        trailersScreen.searchSetAllowedPackage(4, 5, packageMin, packageMax);
        trailersScreen.searchSetNumberOfAxles(8, 9, axlesMin, axlesMax);
        trailersScreen.searchSetDoubleRearWheels(doubleWheels);
        trailersScreen.searchSetPermissibleGrossWeight(10, 11, grossWeightMin, grossWeightMax);
        trailersScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        trailersScreen.searchCarStatus(damaged, registerInPoland, firstOwner, noAccidents);
        trailersScreen.searchLocation(voivodeship);
        trailersScreen.searchShowResults();

        helper.testScreenShoot("SearchTrailersAndroidTestLowFloorSemiTrailers");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");

    }
    //endregion low floor semi-trailers searching

    //region TEST - container semi-trailers searching
    @Test(dataProvider = "searchContainerSemiTrailers", description = "Searching item in container semi-trailers category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search container semi-trailers")
    public void testContainerSemiTrailersSearching(String vehicleBrand, String vehicleModel,
                                                   String priceMin, String priceMax, String privacy, String currency, int third, int fourth,
                                                   String yearOfProdFrom, String yearOfProdTo, String country, int fifth, int sixth, String packageMin,
                                                   String packageMax, int ninth, int tenth, String axlesMin, String axlesMax, boolean doubleWheels, int eleventh, int twelfth,
                                                   String grossWeightMin, String grossWeightMax, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat,
                                                   boolean financingOption, boolean leasing, boolean rent,
                                                   boolean damaged, boolean registerInPoland, boolean firstOwner, boolean noAccidents, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Przyczepy\"");
        WebDriver driver = getDriver();
        SearchTrailersScreen trailersScreen = PageFactory.initElements(driver, SearchTrailersScreen.class);

        trailersScreen.searchSetTrailers();
        trailersScreen.searchApplication("Naczepy pod kontener");
        trailersScreen.searchVehicleBrand(vehicleBrand);
        trailersScreen.searchVehicleModel(vehicleModel);
        trailersScreen.searchSetPrice(priceMin, priceMax);
        trailersScreen.searchSetPrivacy(privacy);
        trailersScreen.searchSetCurrency(currency);
        trailersScreen.searchMoreParameters();
        trailersScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        trailersScreen.searchSetCountryOfOrigin(country);
        trailersScreen.searchSetAllowedPackage(4, 5, packageMin, packageMax);
        trailersScreen.searchSetNumberOfAxles(8, 9, axlesMin, axlesMax);
        trailersScreen.searchSetDoubleRearWheels(doubleWheels);
        trailersScreen.searchSetPermissibleGrossWeight(10, 11, grossWeightMin, grossWeightMax);
        trailersScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        trailersScreen.searchCarStatus(damaged, registerInPoland, firstOwner, noAccidents);
        trailersScreen.searchLocation(voivodeship);
        trailersScreen.searchShowResults();

        helper.testScreenShoot("SearchTrailersAndroidTestContainerSemiTrailers");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");

    }
    //endregion container semi-trailers searching

    //region TEST - trippers semi-trailers searching
    @Test(dataProvider = "searchTrippersSemiTrailers", description = "Searching item in trippers semi-trailers category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search trippers semi-trailers ")
    public void testTrippersSemiTrailersSearching(String vehicleBrand, String vehicleModel,
                                                  String priceMin, String priceMax, String privacy, String currency, int third, int fourth,
                                                  String yearOfProdFrom, String yearOfProdTo, String country, int fifth, int sixth, String packageMin,
                                                  String packageMax, int ninth, int tenth, String axlesMin, String axlesMax, boolean doubleWheels, int eleventh, int twelfth,
                                                  String grossWeightMin, String grossWeightMax, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat, boolean financingOption, boolean leasing, boolean rent,
                                                  boolean damaged, boolean registerInPoland, boolean firstOwner, boolean noAccidents, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Przyczepy\"");
        WebDriver driver = getDriver();
        SearchTrailersScreen trailersScreen = PageFactory.initElements(driver, SearchTrailersScreen.class);

        trailersScreen.searchSetTrailers();
        trailersScreen.searchApplication("Naczepy wywrotki");
        trailersScreen.searchVehicleBrand(vehicleBrand);
        trailersScreen.searchVehicleModel(vehicleModel);
        trailersScreen.searchSetPrice(priceMin, priceMax);
        trailersScreen.searchSetPrivacy(privacy);
        trailersScreen.searchSetCurrency(currency);
        trailersScreen.searchMoreParameters();
        trailersScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        trailersScreen.searchSetCountryOfOrigin(country);
        trailersScreen.searchSetAllowedPackage(4, 5, packageMin, packageMax);
        trailersScreen.searchSetNumberOfAxles(8, 9, axlesMin, axlesMax);
        trailersScreen.searchSetDoubleRearWheels(doubleWheels);
        trailersScreen.searchSetPermissibleGrossWeight(10, 11, grossWeightMin, grossWeightMax);
        trailersScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        trailersScreen.searchCarStatus(damaged, registerInPoland, firstOwner, noAccidents);
        trailersScreen.searchLocation(voivodeship);
        trailersScreen.searchShowResults();

        helper.testScreenShoot("SearchTrailersAndroidTestTrippersSemiTrailers");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");

    }
    //endregion trippers semi-trailers searching

    //region TEST - flatbed trailers and tarpaulins searching
    @Test(dataProvider = "searchFlatbedTrailersAndTarpaulins", description = "Searching item in flatbed trailers and tarpaulins category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search flatbed trailers and tarpaulins ")
    public void testFlatbedTrailersAndTarpaulinsSearching(String vehicleBrand, String vehicleModel,
                                                          String priceMin, String priceMax, String privacy, String currency, int third, int fourth,
                                                          String yearOfProdFrom, String yearOfProdTo, String country, int fifth, int sixth, String packageMin,
                                                          String packageMax, int ninth, int tenth, String axlesMin, String axlesMax, boolean doubleWheels, int eleventh, int twelfth,
                                                          String grossWeightMin, String grossWeightMax, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat, boolean financingOption, boolean leasing, boolean rent,
                                                          boolean damaged, boolean registerInPoland, boolean firstOwner, boolean noAccidents, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Przyczepy\"");
        WebDriver driver = getDriver();
        SearchTrailersScreen trailersScreen = PageFactory.initElements(driver, SearchTrailersScreen.class);

        trailersScreen.searchSetTrailers();
        trailersScreen.searchApplication("Przyczepy burtowe i plandeki");
        trailersScreen.searchVehicleBrand(vehicleBrand);
        trailersScreen.searchVehicleModel(vehicleModel);
        trailersScreen.searchSetPrice(priceMin, priceMax);
        trailersScreen.searchSetPrivacy(privacy);
        trailersScreen.searchSetCurrency(currency);
        trailersScreen.searchMoreParameters();
        trailersScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        trailersScreen.searchSetCountryOfOrigin(country);
        trailersScreen.searchSetAllowedPackage(4, 5, packageMin, packageMax);
        trailersScreen.searchSetNumberOfAxles(8, 9, axlesMin, axlesMax);
        trailersScreen.searchSetDoubleRearWheels(doubleWheels);
        trailersScreen.searchSetPermissibleGrossWeight(10, 11, grossWeightMin, grossWeightMax);
        trailersScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        trailersScreen.searchCarStatus(damaged, registerInPoland, firstOwner, noAccidents);
        trailersScreen.searchLocation(voivodeship);
        trailersScreen.searchShowResults();

        helper.testScreenShoot("SearchTrailersAndroidTestFlatbedTrailersSemiTrailers");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");

    }
    //endregion flatbed trailers and tarpaulins searching

    //region TEST - refrigerated trailers and isotherms searching
    @Test(dataProvider = "searchRefrigeratedTrailersAndIsotherms", description = "Searching item in refrigerated trailers and isotherms category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search refrigerated trailers and isotherms")
    public void testRefrigeratedTrailersAndIsothermsSearching(String vehicleBrand, String vehicleModel,
                                                              String priceMin, String priceMax, String privacy, String currency, int third, int fourth,
                                                              String yearOfProdFrom, String yearOfProdTo, String country, int fifth, int sixth, String packageMin,
                                                              String packageMax, int ninth, int tenth, String axlesMin, String axlesMax, boolean doubleWheels, int eleventh, int twelfth,
                                                              String grossWeightMin, String grossWeightMax, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat, boolean financingOption, boolean leasing, boolean rent,
                                                              boolean damaged, boolean registerInPoland, boolean firstOwner, boolean noAccidents, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Przyczepy\"");
        WebDriver driver = getDriver();
        SearchTrailersScreen trailersScreen = PageFactory.initElements(driver, SearchTrailersScreen.class);

        trailersScreen.searchSetTrailers();
        trailersScreen.searchApplication("Przyczepy chłodnie i izotermy");
        trailersScreen.searchVehicleBrand(vehicleBrand);
        trailersScreen.searchVehicleModel(vehicleModel);
        trailersScreen.searchSetPrice(priceMin, priceMax);
        trailersScreen.searchSetPrivacy(privacy);
        trailersScreen.searchSetCurrency(currency);
        trailersScreen.searchMoreParameters();
        trailersScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        trailersScreen.searchSetCountryOfOrigin(country);
        trailersScreen.searchSetAllowedPackage(4, 5, packageMin, packageMax);
        trailersScreen.searchSetNumberOfAxles(8, 9, axlesMin, axlesMax);
        trailersScreen.searchSetDoubleRearWheels(doubleWheels);
        trailersScreen.searchSetPermissibleGrossWeight(10, 11, grossWeightMin, grossWeightMax);
        trailersScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        trailersScreen.searchCarStatus(damaged, registerInPoland, firstOwner, noAccidents);
        trailersScreen.searchLocation(voivodeship);
        trailersScreen.searchShowResults();

        helper.testScreenShoot("SearchTrailersAndroidTestRefrigeratedTrailers");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");

    }
    //endregion refrigerated trailers and isotherms searching

    //region TEST - tank trailers searching
    @Test(dataProvider = "searchTankTrailers", description = "Searching item in tank trailers category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search tank semi-trailers")
    public void testTankTrailersSearching(String vehicleBrand, String vehicleModel,
                                          String priceMin, String priceMax, String privacy, String currency, int third, int fourth,
                                          String yearOfProdFrom, String yearOfProdTo, String country, int fifth, int sixth, String packageMin,
                                          String packageMax, int ninth, int tenth, String axlesMin, String axlesMax, boolean doubleWheels, int eleventh, int twelfth,
                                          String grossWeightMin, String grossWeightMax, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat, boolean financingOption, boolean leasing, boolean rent,
                                          boolean damaged, boolean registerInPoland, boolean firstOwner, boolean noAccidents, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Przyczepy\"");
        WebDriver driver = getDriver();
        SearchTrailersScreen trailersScreen = PageFactory.initElements(driver, SearchTrailersScreen.class);

        trailersScreen.searchSetTrailers();
        trailersScreen.searchApplication("Przyczepy cysterny");
        trailersScreen.searchVehicleBrand(vehicleBrand);
        trailersScreen.searchVehicleModel(vehicleModel);
        trailersScreen.searchSetPrice(priceMin, priceMax);
        trailersScreen.searchSetPrivacy(privacy);
        trailersScreen.searchSetCurrency(currency);
        trailersScreen.searchMoreParameters();
        trailersScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        trailersScreen.searchSetCountryOfOrigin(country);
        trailersScreen.searchSetAllowedPackage(4, 5, packageMin, packageMax);
        trailersScreen.searchSetNumberOfAxles(8, 9, axlesMin, axlesMax);
        trailersScreen.searchSetDoubleRearWheels(doubleWheels);
        trailersScreen.searchSetPermissibleGrossWeight(10, 11, grossWeightMin, grossWeightMax);
        trailersScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        trailersScreen.searchCarStatus(damaged, registerInPoland, firstOwner, noAccidents);
        trailersScreen.searchLocation(voivodeship);
        trailersScreen.searchShowResults();

        helper.testScreenShoot("SearchTrailersAndroidTestTankTrailers");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");

    }
    //endregion tank trailers searching

    //region TEST - vehicle transport trailers searching
    @Test(dataProvider = "searchVehicleTransportTrailers", description = "Searching item in vehicle transport trailers category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search vehicle transport trailers")
    public void testVehicleTransportTrailersSearching(String vehicleBrand, String vehicleModel,
                                                      String priceMin, String priceMax, String privacy, String currency, int third, int fourth,
                                                      String yearOfProdFrom, String yearOfProdTo, String country, int fifth, int sixth, String packageMin,
                                                      String packageMax, int ninth, int tenth, String axlesMin, String axlesMax, boolean doubleWheels, int eleventh, int twelfth,
                                                      String grossWeightMin, String grossWeightMax, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat, boolean financingOption, boolean leasing, boolean rent,
                                                      boolean damaged, boolean registerInPoland, boolean firstOwner, boolean noAccidents, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Przyczepy\"");
        WebDriver driver = getDriver();
        SearchTrailersScreen trailersScreen = PageFactory.initElements(driver, SearchTrailersScreen.class);

        trailersScreen.searchSetTrailers();
        trailersScreen.searchApplication("Przyczepy do przewozu pojazdów");
        trailersScreen.searchVehicleBrand(vehicleBrand);
        trailersScreen.searchVehicleModel(vehicleModel);
        trailersScreen.searchSetPrice(priceMin, priceMax);
        trailersScreen.searchSetPrivacy(privacy);
        trailersScreen.searchSetCurrency(currency);
        trailersScreen.searchMoreParameters();
        trailersScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        trailersScreen.searchSetCountryOfOrigin(country);
        trailersScreen.searchSetAllowedPackage(4, 5, packageMin, packageMax);
        trailersScreen.searchSetNumberOfAxles(8, 9, axlesMin, axlesMax);
        trailersScreen.searchSetDoubleRearWheels(doubleWheels);
        trailersScreen.searchSetPermissibleGrossWeight(10, 11, grossWeightMin, grossWeightMax);
        trailersScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        trailersScreen.searchCarStatus(damaged, registerInPoland, firstOwner, noAccidents);
        trailersScreen.searchLocation(voivodeship);
        trailersScreen.searchShowResults();

        helper.testScreenShoot("SearchTrailersAndroidTestVehicleTransportTrailers");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion vehicle transport trailers searching

    //region TEST - container trailers searching
    @Test(dataProvider = "searchContainerTrailers", description = "Searching item in container trailers category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search container trailers")
    public void testContainerTrailersSearching(String vehicleBrand, String vehicleModel,
                                               String priceMin, String priceMax, String privacy, String currency, int third, int fourth,
                                               String yearOfProdFrom, String yearOfProdTo, String country, int fifth, int sixth, String packageMin,
                                               String packageMax, int ninth, int tenth, String axlesMin, String axlesMax, boolean doubleWheels, int eleventh, int twelfth,
                                               String grossWeightMin, String grossWeightMax, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat, boolean financingOption, boolean leasing, boolean rent,
                                               boolean damaged, boolean registerInPoland, boolean firstOwner, boolean noAccidents, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Przyczepy\"");
        WebDriver driver = getDriver();
        SearchTrailersScreen trailersScreen = PageFactory.initElements(driver, SearchTrailersScreen.class);

        trailersScreen.searchSetTrailers();
        trailersScreen.searchApplication("Przyczepy kontenery");
        trailersScreen.searchVehicleBrand(vehicleBrand);
        trailersScreen.searchVehicleModel(vehicleModel);
        trailersScreen.searchSetPrice(priceMin, priceMax);
        trailersScreen.searchSetPrivacy(privacy);
        trailersScreen.searchSetCurrency(currency);
        trailersScreen.searchMoreParameters();
        trailersScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        trailersScreen.searchSetCountryOfOrigin(country);
        trailersScreen.searchSetAllowedPackage(4, 5, packageMin, packageMax);
        trailersScreen.searchSetNumberOfAxles(8, 9, axlesMin, axlesMax);
        trailersScreen.searchSetDoubleRearWheels(doubleWheels);
        trailersScreen.searchSetPermissibleGrossWeight(10, 11, grossWeightMin, grossWeightMax);
        trailersScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        trailersScreen.searchCarStatus(damaged, registerInPoland, firstOwner, noAccidents);
        trailersScreen.searchLocation(voivodeship);
        trailersScreen.searchShowResults();

        helper.testScreenShoot("SearchTrailersAndroidTestContainerTrailers");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");


    }
    //endregion container trailers searching

    //region TEST - catering trailers searching
    @Test(dataProvider = "searchCateringTrailers", description = "Searching item in catering trailers category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search catering trailers")
    public void testCateringTrailersSearching(String vehicleBrand, String vehicleModel,
                                              String priceMin, String priceMax, String privacy, String currency, int third, int fourth,
                                              String yearOfProdFrom, String yearOfProdTo, String country, int fifth, int sixth, String packageMin,
                                              String packageMax, int ninth, int tenth, String axlesMin, String axlesMax, boolean doubleWheels, int eleventh, int twelfth,
                                              String grossWeightMin, String grossWeightMax, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat, boolean financingOption, boolean leasing, boolean rent,
                                              boolean damaged, boolean registerInPoland, boolean firstOwner, boolean noAccidents, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Przyczepy\"");
        WebDriver driver = getDriver();
        SearchTrailersScreen trailersScreen = PageFactory.initElements(driver, SearchTrailersScreen.class);

        trailersScreen.searchSetTrailers();
        trailersScreen.searchApplication("Przyczepy gastronomiczne");
        trailersScreen.searchVehicleBrand(vehicleBrand);
        trailersScreen.searchVehicleModel(vehicleModel);
        trailersScreen.searchSetPrice(priceMin, priceMax);
        trailersScreen.searchSetPrivacy(privacy);
        trailersScreen.searchSetCurrency(currency);
        trailersScreen.searchMoreParameters();
        trailersScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        trailersScreen.searchSetCountryOfOrigin(country);
        trailersScreen.searchSetAllowedPackage(4, 5, packageMin, packageMax);
        trailersScreen.searchSetNumberOfAxles(8, 9, axlesMin, axlesMax);
        trailersScreen.searchSetDoubleRearWheels(doubleWheels);
        trailersScreen.searchSetPermissibleGrossWeight(10, 11, grossWeightMin, grossWeightMax);
        trailersScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        trailersScreen.searchCarStatus(damaged, registerInPoland, firstOwner, noAccidents);
        trailersScreen.searchLocation(voivodeship);
        trailersScreen.searchShowResults();

        helper.testScreenShoot("SearchTrailersAndroidTestCateringTrailers");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");

    }
    //endregion catering trailers searching

    //region TEST - trailers accessories searching
    @Test(dataProvider = "searchTrailersAccessories", description = "Searching item in trailers accessories category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search trailers accessories")
    public void testTrailersAccessoriesSearching(String vehicleBrand, String vehicleModel,
                                                 String priceMin, String priceMax, String privacy, String currency, int third, int fourth,
                                                 String yearOfProdFrom, String yearOfProdTo, String country,
                                                 int ninth, int tenth, String axlesMin, String axlesMax, boolean doubleWheels, int eleventh, int twelfth,
                                                 String grossWeightMin, String grossWeightMax, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat, boolean financingOption, boolean leasing, boolean rent,
                                                 boolean damaged, boolean registerInPoland, boolean firstOwner, boolean noAccidents, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Przyczepy\"");
        WebDriver driver = getDriver();
        SearchTrailersScreen trailersScreen = PageFactory.initElements(driver, SearchTrailersScreen.class);

        trailersScreen.searchSetTrailers();
        trailersScreen.searchApplication("Akcesoria do przyczep");
        trailersScreen.searchVehicleBrand(vehicleBrand);
        trailersScreen.searchVehicleModel(vehicleModel);
        trailersScreen.searchSetPrice(priceMin, priceMax);
        trailersScreen.searchSetPrivacy(privacy);
        trailersScreen.searchSetCurrency(currency);
        trailersScreen.searchMoreParameters();
        trailersScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        trailersScreen.searchSetCountryOfOrigin(country);
        trailersScreen.searchSetNumberOfAxles(8, 9, axlesMin, axlesMax);
        trailersScreen.searchSetDoubleRearWheels(doubleWheels);
        trailersScreen.searchSetPermissibleGrossWeight(10, 11, grossWeightMin, grossWeightMax);
        trailersScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        trailersScreen.searchCarStatus(damaged, registerInPoland, firstOwner, noAccidents);
        trailersScreen.searchLocation(voivodeship);
        trailersScreen.searchShowResults();

        helper.testScreenShoot("SearchTrailersAndroidTestTrailersAccessories");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");

    }
    //endregion trailers accessories searching

    //region TEST - all in trailers searching
    @Test(dataProvider = "searchAllTrailers", description = "Searching all items in trailers category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search all in trailers")
    public void testAllInTrailersSearching(String vehicleBrand, String vehicleModel,
                                           String priceMin, String priceMax, String privacy, String currency, int third, int fourth,
                                           String yearOfProdFrom, String yearOfProdTo, String country,
                                           int ninth, int tenth, String axlesMin, String axlesMax, boolean doubleWheels, int eleventh, int twelfth,
                                           String grossWeightMin, String grossWeightMax, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat, boolean financingOption, boolean leasing, boolean rent,
                                           boolean damaged, boolean registerInPoland, boolean firstOwner, boolean noAccidents, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Przyczepy\"");
        WebDriver driver = getDriver();
        SearchTrailersScreen trailersScreen = PageFactory.initElements(driver, SearchTrailersScreen.class);

        trailersScreen.searchSetTrailers();
        trailersScreen.searchApplication("Wszystkie w Przyczepy");
        trailersScreen.searchVehicleBrand(vehicleBrand);
        trailersScreen.searchVehicleModel(vehicleModel);
        trailersScreen.searchSetPrice(priceMin, priceMax);
        trailersScreen.searchSetPrivacy(privacy);
        trailersScreen.searchSetCurrency(currency);
        trailersScreen.searchMoreParameters();
        trailersScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        trailersScreen.searchSetCountryOfOrigin(country);
        trailersScreen.searchSetNumberOfAxles(8, 9, axlesMin, axlesMax);
        trailersScreen.searchSetDoubleRearWheels(doubleWheels);
        trailersScreen.searchSetPermissibleGrossWeight(10, 11, grossWeightMin, grossWeightMax);
        trailersScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        trailersScreen.searchCarStatus(damaged, registerInPoland, firstOwner, noAccidents);
        trailersScreen.searchLocation(voivodeship);
        trailersScreen.searchShowResults();

        helper.testScreenShoot("SearchTrailersAndroidTestAllTrailers");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");

    }
    //endregion all in trailers searching

    //endregion TESTS

}
