package com.selenium.java.tests.test.android;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.MethodHelper;
import com.selenium.java.pages.android.RegistrationScreen;
import io.qameta.allure.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.*;
import io.qameta.allure.Feature;

@Feature("REGISTRATION: Registration Tests")
public class RegistrationAndroidTest extends BaseTest {

    MethodHelper methodHelper = new MethodHelper();

    @BeforeClass
    public void startup() {

        startAppiumServer();
    }

    @BeforeMethod(description = "Starting appium server")
    @Parameters(value = {"Platform", "DeviceID", "DeviceName"})
    public void setUp(String platform, String deviceId, String deviceName, ITestContext context) {

        instalApp = false;

        launchAndroid(platform, deviceId, deviceName, context);
        System.out.println("Start of the tests");
    }

    @DataProvider
    public Object[][] getRegister() {
        return new Object[][]{

                {0, 1, 2, "marek.test144@spoko.pl", "123Test123", "123Test123"},
                {0, 1, 2, "marek.test145@spoko.pl", "123Test123", "123Test123"},
        };
    }

    @DataProvider
    public Object[][] getNoData() {
        return new Object[][]{

                {0, 1, 2," "," "," "}
        };
    }

    @DataProvider
    public Object[][] getNoEmail() {
        return new Object[][]{

                {0, 1, 2, " ", "123Test123", "123Test123"}
        };
    }

    @DataProvider
    public Object[][] getNoPassword() {
        return new Object[][]{

                {0, 1, 2, "marek.testowy11@spoko.pl", " ", "123Test123"},
                {0, 1, 2, "marek.testowy11@spoko.pl", " ", " "}
        };

    }

    @DataProvider
    public Object[][] getNoRePassword() {
        return new Object[][]{

                {0, 1, 2, "marek.testowy11@spoko.pl", "123Test123", " "}
        };

    }

    @DataProvider
    public Object[][] getWrongEmail() {
        return new Object[][]{

                {0, 1, 2, "@spoko.pl", "123Test123", "123Test123"},
                {0, 1, 2, "marek.testowyspoko.pl", "123Test123", "123Test123"},
                {0, 1, 2, "marek.testowy1@spoko.pl", "123Test123", "123Test123"}

        };

    }

    @DataProvider
    public Object[][] getWrongPassword() {
        return new Object[][]{

                {0, 1, 2, "marek.testowy1111@spoko.pl", "1", "1"},
                {0, 1, 2, "marek.testowy3333@spoko.pl", "!!!!!!!", "!!!!!!!"}
        };
    }

    @DataProvider
    public Object[][] getWrongRePassword() {
        return new Object[][]{

                {0, 1, 2, "marek.testowy1111@spoko.pl", "123Test123", "12345Test12345"},
                {0, 1, 2, "marek.testowy1111@spoko.pl", "123Test123", "!!!!!!!"}
        };
    }

    @Test(dataProvider = "getRegister", description = "Checking registration with true parameters")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - registration on")
    @Story("REGISTER: true registration")
    public void testRegToApp(int first, int second, int third, String email, String password, String
            repeatPassword) {
        System.out.println("Correct registration test");

        WebDriver driver = getDriver();
        RegistrationScreen registrationScreen = PageFactory.initElements(driver, RegistrationScreen.class);

        registrationScreen.regGoToRegister();
        registrationScreen.regInputData(first,second,third, email, password, repeatPassword);
        registrationScreen.regCheckbox();
        registrationScreen.regRegister();
        methodHelper.waitTime(2);

      //  Assert.assertTrue(driver.findElement(By.xpath("//*[contains(@text, 'Wróć na')]")).isDisplayed());
        methodHelper.testScreenshot("testTrueRegToApp");
        methodHelper.getScreenShot("boxViewIncorect.png");
        registrationScreen.regBackToLog();
    }

    @Test(dataProvider = "getNoData", description = "Checking registration with no data")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - registration on mobile")
    @Story("REGISTER: No data")
    public void testRegToAppNoData(int first, int second, int third, String email, String password, String
            repeatPassword) {
        System.out.println("Incorrect registration test");

        WebDriver driver = getDriver();
        RegistrationScreen registrationScreen = PageFactory.initElements(driver, RegistrationScreen.class);

        registrationScreen.regGoToRegister();
        registrationScreen.regInputData(first,second,third, email, password, repeatPassword);
        registrationScreen.regRegister();
      //  Assert.assertTrue(registrationScreen.btn_register.isDisplayed(), "Test failed, account registered");
        Assert.assertTrue(driver.findElement(By.xpath("//*[contains(@text, 'E-mail:')]")).isDisplayed());
        methodHelper.getScreenShot("IncorrectRegNoData.png");
        methodHelper.testScreenshot("IncorrectRegNoData");
    }

    @Test(dataProvider = "getNoEmail", description = "Checking registration with no email")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - registration on mobile")
    @Story("REGISTER: No email")
    public void testRegToAppNoEmail(int first, int second, int third, String email, String password, String
            repeatPassword) {
        System.out.println("Incorrect registration test");

        WebDriver driver = getDriver();
        RegistrationScreen registrationScreen = PageFactory.initElements(driver, RegistrationScreen.class);

        registrationScreen.regGoToRegister();
        registrationScreen.regInputData(first,second,third, email, password, repeatPassword);
        registrationScreen.regRegister();
        Assert.assertTrue(driver.findElement(By.xpath("//*[contains(@text, 'E-mail:')]")).isDisplayed());
        methodHelper.getScreenShot("IncorrectRegNoEmail.png");
        methodHelper.testScreenshot("IncorrectRegNoEmail");
    }

    @Test(dataProvider = "getNoPassword", description = "Checking registration with no password")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - registration on mobile")
    @Story("REGISTER: No password")
    public void testRegToAppNoPassword(int first, int second, int third, String email, String
            password, String repeatPassword) {
        System.out.println("Incorrect registration test");

        WebDriver driver = getDriver();
        RegistrationScreen registrationScreen = PageFactory.initElements(driver, RegistrationScreen.class);

        registrationScreen.regGoToRegister();
        registrationScreen.regInputData(first,second,third, email, password, repeatPassword);
        registrationScreen.regRegister();
        Assert.assertTrue(driver.findElement(By.xpath("//*[contains(@text, 'Hasło:')]")).isDisplayed());
        methodHelper.getScreenShot("IncorrectRegNoPassword.png");
        methodHelper.testScreenshot("IncorrectRegNoPass");
    }

    @Test(dataProvider = "getNoRePassword", description = "Checking registration with no re password")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - registration on mobile")
    @Story("REGISTER: No re-password")
    public void testRegToAppNoRePassword(int first, int second, int third, String email, String
            password, String repeatPassword) {
        System.out.println("Incorrect registration test");

        WebDriver driver = getDriver();
        RegistrationScreen registrationScreen = PageFactory.initElements(driver, RegistrationScreen.class);

        registrationScreen.regGoToRegister();
        registrationScreen.regInputData(first,second,third, email, password, repeatPassword);
        registrationScreen.regRegister();
        Assert.assertTrue(driver.findElement(By.xpath("//*[contains(@text, 'Powtórz hasło:')]")).isDisplayed());
        methodHelper.getScreenShot("IncorrectRegNoRePassword.png");
        methodHelper.testScreenshot("IncorrectRegNoRePass");
    }

    @Test(dataProvider = "getWrongEmail", description = "Checking registration with wrong email")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - registration on mobile")
    @Story("REGISTER: Wrong email")
    public void testRegToAppWrongEmail(int first, int second, int third, String email, String
            password, String repeatPassword) {
        System.out.println("Incorrect registration test");

        WebDriver driver = getDriver();
        RegistrationScreen registrationScreen = PageFactory.initElements(driver, RegistrationScreen.class);

        registrationScreen.regGoToRegister();
        registrationScreen.regInputData(first,second,third, email, password, repeatPassword);
        registrationScreen.regRegister();
        Assert.assertTrue(driver.findElement(By.xpath("//*[contains(@text, 'E-mail:')]")).isDisplayed());
        methodHelper.getScreenShot("IncorrectRegWrongEmail.png");
        methodHelper.testScreenshot("IncorrectRegWrongEmail");
    }

    @Test(dataProvider = "getWrongPassword", description = "Checking registration with wrong password")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - registration on mobile")
    @Story("REGISTER: Wrong password")
    public void testRegToAppWrongPassword(int first, int second, int third, String email, String
            password, String repeatPassword) {
        System.out.println("Incorrect registration test");

        WebDriver driver = getDriver();
        RegistrationScreen registrationScreen = PageFactory.initElements(driver, RegistrationScreen.class);

        registrationScreen.regGoToRegister();
        registrationScreen.regInputData(first,second,third, email, password, repeatPassword);
        registrationScreen.regRegister();
      //  Assert.assertTrue(driver.findElement(By.xpath("//*[contains(@text, 'Hasło:')]")).isDisplayed());
        methodHelper.getScreenShot("IncorrectRegWrongPassword.png");
        methodHelper.testScreenshot("IncorrectRegWrongPass");
    }

    @Test(dataProvider = "getWrongRePassword", description = "Checking registration with re password")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - registration on mobile")
    @Story("REGISTER: Wrong re-password")
    public void testRegToAppWrongRePassword(int first, int second, int third, String email, String
            password, String repeatPassword) {
        System.out.println("Incorrect registration test");

        WebDriver driver = getDriver();
        RegistrationScreen registrationScreen = PageFactory.initElements(driver, RegistrationScreen.class);

        registrationScreen.regGoToRegister();
        registrationScreen.regInputData(first,second,third, email, password, repeatPassword);
        registrationScreen.regRegister();
        Assert.assertTrue(driver.findElement(By.xpath("//*[contains(@text, 'Powtórz hasło:')]")).isDisplayed());
        methodHelper.getScreenShot("IncorrectRegWrongRePassword.png");
        methodHelper.testScreenshot("IncorrectRegWrongRePass");
    }

    @AfterMethod(description = "Finishing test and shutting down app")
    public void tearDown() {
        System.out.println("End of the tests");
    }
}



