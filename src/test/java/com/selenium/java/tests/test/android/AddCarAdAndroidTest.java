package com.selenium.java.tests.test.android;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.MethodHelper;
import com.selenium.java.listeners.TestListener;
import com.selenium.java.pages.android.AdCarsScreen;
import com.selenium.java.pages.android.LoginScreen;
import io.appium.java_client.AppiumDriver;
import io.qameta.allure.*;
import org.openqa.selenium.ScreenOrientation;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.*;

@Listeners({TestListener.class})
@Feature("ADDING AD: Car adding ad Mobile Tests")

public class AddCarAdAndroidTest extends BaseTest {

    @BeforeClass
    public void startup() {

        startAppiumServer();
    }

    @BeforeMethod(description = "Starting appium server")
    @Parameters(value = {"Platform", "DeviceID", "DeviceName"})
    public void setUp(String platform, String deviceId, String deviceName, ITestContext context) {

        instalApp = false;
        launchAndroid(platform, deviceId, deviceName, context);
    }

    @DataProvider
    public Object[][] getAd() {
        return new Object[][]{
                {2, 9, 10,"Aston Martin", "xxxxxxxxxxxxxxxxx", "Bulldog", "2010", "150000", "Diesel", "10 000", true, "Kombi",
                        "Złoty", "5", "5", "Opis auta dodanego do ogłoszenia w ramach testu automatycznego",
                        "Kujawsko-pomorskie", "Bydgoszcz", "Adam Adamiak", "712123123", true, false, true, false,
                        true, false, true, false, false},
                { 1, 6, 7,"BMW", "xxxxxxxxxxxxxxxxx", "i8", "2015", "100000", "Benzyna+LPG", "190 000", true, "Kompakt", "Biały",
                        "5", "5", "Opis auta dodanego do ogłoszenia w ramach testu automatycznego", "Mazowieckie",
                        "Ciechanów", "Adam Adamiak", "712123123", true, false, true, false, true, false, true,
                        false, false},
                {3, 4, 5, "Alfa Romeo", "xxxxxxxxxxxxxxxxx", "147", "2012", "170000", "Hybryda", "20 000", true, "Sedan",
                        "Czarny","5", "5", "Opis auta dodanego do ogłoszenia w ramach testu automatycznego",
                        "Mazowieckie", "Ciechanów", "Adam Adamiak", "712123123", true, false, true, false, true,
                        false, true, false, false}
        };
    }

    @Test(dataProvider = "getAd", description = "Adding car ad with basic parameters")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - adding car ad")
    @Story("ADDING AD: Add car ad with basic parameters")
    public void testAddCarAd(int first_photo, int second_photo, int third_photo, String brand, String vin, String model,
                             String year, String mileage, String fuel, String price, boolean negotiation, String type,
                             String colour, String seats, String doors, String description, String voivodeship,
                             String city, String person, String number, boolean vat, boolean funding, boolean invoice,
                             boolean leasing, boolean metalic, boolean pearl,  boolean mat, boolean acrylic, boolean wheel) throws InterruptedException {

        WebDriver driver = getDriver();
        AppiumDriver driver2 = (AppiumDriver) getDriver();
        MethodHelper methodHelper = new MethodHelper();
        LoginScreen loginScreen = PageFactory.initElements(driver, LoginScreen.class);
        AdCarsScreen adCarsScreen = PageFactory.initElements(driver, AdCarsScreen.class);

        loginScreen.loginToApp("damian.testowy12@vp.pl", "Haslo123");
        methodHelper.waitTime(2);
        adCarsScreen.adGoToMenu();
        System.out.println("Going to menu");
        adCarsScreen.adGoToAdd();
        System.out.println("Going to ad screen");
        methodHelper.waitTime(3);

        adCarsScreen.adBrandOfCar(brand);
        adCarsScreen.adAssertionBrand(brand);
        System.out.println("Brand set. Setting VIN..");

        adCarsScreen.adVin(vin);
        adCarsScreen.adAssertionVin(vin);
        System.out.println("VIN set. Setting model..");

        adCarsScreen.adModelOfCar(model);
        adCarsScreen.adAssertionModel(model);
        System.out.println("Model set. Setting year of production..");

        adCarsScreen.adYearOfProduction(year);
        adCarsScreen.adAssertionYear(year);
        System.out.println("Year of production set. Setting mileage..");

        adCarsScreen.adMileage(mileage);
        adCarsScreen.adAssertionMileage(mileage);
        System.out.println("Mileage set. Setting type of fuel..");

        adCarsScreen.adTypeOfFuel(fuel);
        adCarsScreen.adAssertionFuel(fuel);
        System.out.println("Fuel set. Setting price..");
        methodHelper.scrollText("Lokalizacja", direction.UP, "up", 0.6, 1);

        adCarsScreen.adPrice(price, negotiation);
        adCarsScreen.adAssertionPrice(price, negotiation);
        System.out.println("Price set. Setting additional price parameters..");

        adCarsScreen.adPriceAdditional(vat, funding, invoice, leasing);
        adCarsScreen.adAssertionAdditional(vat, funding, invoice, leasing);
        System.out.println("Additional price parameters set. Setting type..");

        adCarsScreen.adTypeOfCar(type);
        adCarsScreen.adAssertionTypeOfCar(type);
        System.out.println("Type set. Setting colour..");

        adCarsScreen.adColour(colour);
        adCarsScreen.adAssertionColour(colour);
        System.out.println("Colour set. Setting number of seats..");

        methodHelper.scrollText("Opis", direction.UP, "up", 0.5, 1);

        adCarsScreen.adNumberOfSeats(seats);
        adCarsScreen.adAssertionNumberOfSeats(seats);
        System.out.println("Number of seats set. Setting number of doors..");

        adCarsScreen.adNumberOfDoors(doors);
        adCarsScreen.adAssertionNumberOfDoors(doors);
        System.out.println("Number of doors set. Setting body parameters..");

        adCarsScreen.adBodyParameters(metalic, pearl, mat, acrylic);
        adCarsScreen.adAssertionBodyParameters(metalic, pearl, mat, acrylic);
        System.out.println("Body parameters set. Setting wheel side..");

        adCarsScreen.carsEnglishWheel(wheel);
        adCarsScreen.adAssertionWheel(wheel);
        System.out.println("English wheel set. Setting description..");

        adCarsScreen.adDescription(description);
        adCarsScreen.adAssertionDescription(description);
        System.out.println("Description set. Setting contact person..");
        driver2.rotate(ScreenOrientation.LANDSCAPE);
        methodHelper.waitTime(3);
        driver2.rotate(ScreenOrientation.PORTRAIT);
        methodHelper.waitTime(3);
        methodHelper.scrollText("Oświadczam", direction.UP, "up", 0.6, 1);
        methodHelper.waitTime(2);



        adCarsScreen.adContactPerson(person);
        adCarsScreen.adAssertionContactPerson(person);
        System.out.println("Person set. Setting phone number..");

        adCarsScreen.adPhoneNumber(number);
        adCarsScreen.adAssertionPhoneNumber(number);
        System.out.println("Number set. Accepting state..");

        adCarsScreen.adAcceptState();
        adCarsScreen.adAssertionState();
        System.out.println("State accept. Setting location..");

        adCarsScreen.adLocation(voivodeship, city);
        adCarsScreen.adAssertionLocation(voivodeship, city);
        System.out.println("City: " + city + " set. Adding ad..");
        methodHelper.waitTime(2);

        adCarsScreen.adPreview();
        methodHelper.waitTime(2);

        adCarsScreen.adAssertionCorrect();
        System.out.println("Ad preview");




    }

    @Test(dataProvider = "getAd", description = "Adding car ad without required param - brand")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - adding car ad without required param - brand")
    @Story("ADDING AD: Add car ad without required param - brand")
    public void testAddNoBrandCarAd(int first_photo, int second_photo, int third_photo, String brand, String vin, String model,
                                    String year, String mileage, String fuel, String price, boolean negotiation, String type,
                                    String colour, String seats, String doors, String description, String voivodeship,
                                    String city, String person, String number, boolean vat, boolean funding, boolean invoice,
                                    boolean leasing, boolean metalic, boolean pearl,  boolean mat, boolean acrylic, boolean wheel) throws InterruptedException {

        WebDriver driver = getDriver();
        AppiumDriver driver2 = (AppiumDriver) getDriver();
        MethodHelper methodHelper = new MethodHelper();
        LoginScreen loginScreen = PageFactory.initElements(driver, LoginScreen.class);
        AdCarsScreen adCarsScreen = PageFactory.initElements(driver, AdCarsScreen.class);

        loginScreen.loginToApp("damian.testowy12@vp.pl", "Haslo123");
        methodHelper.waitTime(2);
        adCarsScreen.adGoToMenu();
        System.out.println("Going to menu");
        adCarsScreen.adGoToAdd();
        System.out.println("Going to ad screen");
        methodHelper.waitTime(3);

        System.out.println("Setting VIN..");

        adCarsScreen.adVin(vin);
        adCarsScreen.adAssertionVin(vin);
        System.out.println("VIN set. Setting year..");


        adCarsScreen.adYearOfProduction(year);
        adCarsScreen.adAssertionYear(year);
        System.out.println("Year of production set. Setting mileage..");

        adCarsScreen.adMileage(mileage);
        adCarsScreen.adAssertionMileage(mileage);
        System.out.println("Mileage set. Setting type of fuel..");

        adCarsScreen.adTypeOfFuel(fuel);
        adCarsScreen.adAssertionFuel(fuel);
        System.out.println("Fuel set. Setting price..");
        methodHelper.scrollText("Lokalizacja", direction.UP, "up", 0.6, 1);

        adCarsScreen.adPrice(price, negotiation);
        adCarsScreen.adAssertionPrice(price, negotiation);
        System.out.println("Price set. Setting additional price parameters..");

        adCarsScreen.adPriceAdditional(vat, funding, invoice, leasing);
        adCarsScreen.adAssertionAdditional(vat, funding, invoice, leasing);
        System.out.println("Additional price parameters set. Setting type..");

        adCarsScreen.adTypeOfCar(type);
        adCarsScreen.adAssertionTypeOfCar(type);
        System.out.println("Type set. Setting colour..");

        adCarsScreen.adColour(colour);
        adCarsScreen.adAssertionColour(colour);
        System.out.println("Colour set. Setting number of seats..");

        methodHelper.scrollText("Opis", direction.UP, "up", 0.5, 1);

        adCarsScreen.adNumberOfSeats(seats);
        adCarsScreen.adAssertionNumberOfSeats(seats);
        System.out.println("Number of seats set. Setting number of doors..");

        adCarsScreen.adNumberOfDoors(doors);
        adCarsScreen.adAssertionNumberOfDoors(doors);
        System.out.println("Number of doors set. Setting body parameters..");

        adCarsScreen.adBodyParameters(metalic, pearl, mat, acrylic);
        adCarsScreen.adAssertionBodyParameters(metalic, pearl, mat, acrylic);
        System.out.println("Body parameters set. Setting wheel side..");

        adCarsScreen.carsEnglishWheel(wheel);
        adCarsScreen.adAssertionWheel(wheel);
        System.out.println("English wheel set. Setting description..");

        adCarsScreen.adDescription(description);
        adCarsScreen.adAssertionDescription(description);
        System.out.println("Description set. Setting contact person..");
        driver2.rotate(ScreenOrientation.LANDSCAPE);
        methodHelper.waitTime(3);
        driver2.rotate(ScreenOrientation.PORTRAIT);
        methodHelper.waitTime(3);
        methodHelper.scrollText("Oświadczam", direction.UP, "up", 0.6, 1);
        methodHelper.waitTime(2);



        adCarsScreen.adContactPerson(person);
        adCarsScreen.adAssertionContactPerson(person);
        System.out.println("Person set. Setting phone number..");

        adCarsScreen.adPhoneNumber(number);
        adCarsScreen.adAssertionPhoneNumber(number);
        System.out.println("Number set. Accepting state..");

        adCarsScreen.adAcceptState();
        adCarsScreen.adAssertionState();
        System.out.println("State accept. Setting location..");

        adCarsScreen.adLocation(voivodeship, city);
        adCarsScreen.adAssertionLocation(voivodeship, city);
        System.out.println("City: " + city + " set. Adding ad..");
        methodHelper.waitTime(2);


        adCarsScreen.adAssertionIncorrect();
        System.out.println("Ad can't be add");




    }


    @Test(dataProvider = "getAd", description = "Adding car ad without required param - year of production")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - adding car ad without required param - year of production")
    @Story("ADDING AD: Add car ad without required param - year of production")
    public void testAddNoYearCarAd(int first_photo, int second_photo, int third_photo, String brand, String vin, String model,
                                   String year, String mileage, String fuel, String price, boolean negotiation, String type,
                                   String colour, String seats, String doors, String description, String voivodeship,
                                   String city, String person, String number, boolean vat, boolean funding, boolean invoice,
                                   boolean leasing, boolean metalic, boolean pearl,  boolean mat, boolean acrylic, boolean wheel) throws InterruptedException {

        WebDriver driver = getDriver();
        AppiumDriver driver2 = (AppiumDriver) getDriver();
        MethodHelper methodHelper = new MethodHelper();
        LoginScreen loginScreen = PageFactory.initElements(driver, LoginScreen.class);
        AdCarsScreen adCarsScreen = PageFactory.initElements(driver, AdCarsScreen.class);

        loginScreen.loginToApp("damian.testowy12@vp.pl", "Haslo123");
        methodHelper.waitTime(2);
        adCarsScreen.adGoToMenu();
        System.out.println("Going to menu");
        adCarsScreen.adGoToAdd();
        System.out.println("Going to ad screen");
        methodHelper.waitTime(3);
        System.out.println("Setting brand..");

        adCarsScreen.adBrandOfCar(brand);
        adCarsScreen.adAssertionBrand(brand);
        System.out.println("Brand set. Setting VIN..");

        adCarsScreen.adVin(vin);
        adCarsScreen.adAssertionVin(vin);
        System.out.println("VIN set. Setting model..");

        adCarsScreen.adModelOfCar(model);
        adCarsScreen.adAssertionModel(model);
        System.out.println("Model set. Setting mileage.");

        adCarsScreen.adMileage(mileage);
        adCarsScreen.adAssertionMileage(mileage);
        System.out.println("Mileage set. Setting type of fuel..");

        adCarsScreen.adTypeOfFuel(fuel);
        adCarsScreen.adAssertionFuel(fuel);
        System.out.println("Fuel set. Setting price..");
        methodHelper.scrollText("Lokalizacja", direction.UP, "up", 0.6, 1);

        adCarsScreen.adPrice(price, negotiation);
        adCarsScreen.adAssertionPrice(price, negotiation);
        System.out.println("Price set. Setting additional price parameters..");

        adCarsScreen.adPriceAdditional(vat, funding, invoice, leasing);
        adCarsScreen.adAssertionAdditional(vat, funding, invoice, leasing);
        System.out.println("Additional price parameters set. Setting type..");

        adCarsScreen.adTypeOfCar(type);
        adCarsScreen.adAssertionTypeOfCar(type);
        System.out.println("Type set. Setting colour..");

        adCarsScreen.adColour(colour);
        adCarsScreen.adAssertionColour(colour);
        System.out.println("Colour set. Setting number of seats..");

        methodHelper.scrollText("Opis", direction.UP, "up", 0.5, 1);

        adCarsScreen.adNumberOfSeats(seats);
        adCarsScreen.adAssertionNumberOfSeats(seats);
        System.out.println("Number of seats set. Setting number of doors..");

        adCarsScreen.adNumberOfDoors(doors);
        adCarsScreen.adAssertionNumberOfDoors(doors);
        System.out.println("Number of doors set. Setting body parameters..");

        adCarsScreen.adBodyParameters(metalic, pearl, mat, acrylic);
        adCarsScreen.adAssertionBodyParameters(metalic, pearl, mat, acrylic);
        System.out.println("Body parameters set. Setting wheel side..");

        adCarsScreen.carsEnglishWheel(wheel);
        adCarsScreen.adAssertionWheel(wheel);
        System.out.println("English wheel set. Setting description..");

        adCarsScreen.adDescription(description);
        adCarsScreen.adAssertionDescription(description);
        System.out.println("Description set. Setting contact person..");
        driver2.rotate(ScreenOrientation.LANDSCAPE);
        methodHelper.waitTime(3);
        driver2.rotate(ScreenOrientation.PORTRAIT);
        methodHelper.waitTime(3);
        methodHelper.scrollText("Oświadczam", direction.UP, "up", 0.6, 1);
        methodHelper.waitTime(2);



        adCarsScreen.adContactPerson(person);
        adCarsScreen.adAssertionContactPerson(person);
        System.out.println("Person set. Setting phone number..");

        adCarsScreen.adPhoneNumber(number);
        adCarsScreen.adAssertionPhoneNumber(number);
        System.out.println("Number set. Accepting state..");

        adCarsScreen.adAcceptState();
        adCarsScreen.adAssertionState();
        System.out.println("State accept. Setting location..");

        adCarsScreen.adLocation(voivodeship, city);
        adCarsScreen.adAssertionLocation(voivodeship, city);
        System.out.println("City: " + city + " set. Adding ad..");
        methodHelper.waitTime(2);

        adCarsScreen.adAssertionIncorrect();
        System.out.println("Ad can't be add");

    }

    @Test(dataProvider = "getAd", description = "Adding car ad without required param - mileage")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - adding car ad without required param - mileage")
    @Story("ADDING AD: Add car ad without required param - mileage")
    public void testAddNoMileageCarAd(int first_photo, int second_photo, int third_photo, String brand, String vin, String model,
                                      String year, String mileage, String fuel, String price, boolean negotiation, String type,
                                      String colour, String seats, String doors, String description, String voivodeship,
                                      String city, String person, String number, boolean vat, boolean funding, boolean invoice,
                                      boolean leasing, boolean metalic, boolean pearl,  boolean mat, boolean acrylic, boolean wheel) throws InterruptedException {

        WebDriver driver = getDriver();
        AppiumDriver driver2 = (AppiumDriver) getDriver();
        MethodHelper methodHelper = new MethodHelper();
        LoginScreen loginScreen = PageFactory.initElements(driver, LoginScreen.class);
        AdCarsScreen adCarsScreen = PageFactory.initElements(driver, AdCarsScreen.class);

        loginScreen.loginToApp("damian.testowy12@vp.pl", "Haslo123");
        methodHelper.waitTime(2);
        adCarsScreen.adGoToMenu();
        System.out.println("Going to menu");
        adCarsScreen.adGoToAdd();
        System.out.println("Going to ad screen");
        methodHelper.waitTime(3);
        System.out.println("Setting brand..");

        adCarsScreen.adBrandOfCar(brand);
        adCarsScreen.adAssertionBrand(brand);
        System.out.println("Brand set. Setting VIN..");

        adCarsScreen.adVin(vin);
        adCarsScreen.adAssertionVin(vin);
        System.out.println("VIN set. Setting model..");

        adCarsScreen.adModelOfCar(model);
        adCarsScreen.adAssertionModel(model);
        System.out.println("Model set. Setting year of production..");

        adCarsScreen.adYearOfProduction(year);
        adCarsScreen.adAssertionYear(year);
        System.out.println("Year of production set. Setting fuel..");

        adCarsScreen.adTypeOfFuel(fuel);
        adCarsScreen.adAssertionFuel(fuel);
        System.out.println("Fuel set. Setting price..");
        methodHelper.scrollText("Lokalizacja", direction.UP, "up", 0.6, 1);

        adCarsScreen.adPrice(price, negotiation);
        adCarsScreen.adAssertionPrice(price, negotiation);
        System.out.println("Price set. Setting additional price parameters..");

        adCarsScreen.adPriceAdditional(vat, funding, invoice, leasing);
        adCarsScreen.adAssertionAdditional(vat, funding, invoice, leasing);
        System.out.println("Additional price parameters set. Setting type..");

        adCarsScreen.adTypeOfCar(type);
        adCarsScreen.adAssertionTypeOfCar(type);
        System.out.println("Type set. Setting colour..");

        adCarsScreen.adColour(colour);
        adCarsScreen.adAssertionColour(colour);
        System.out.println("Colour set. Setting number of seats..");

        methodHelper.scrollText("Opis", direction.UP, "up", 0.5, 1);

        adCarsScreen.adNumberOfSeats(seats);
        adCarsScreen.adAssertionNumberOfSeats(seats);
        System.out.println("Number of seats set. Setting number of doors..");

        adCarsScreen.adNumberOfDoors(doors);
        adCarsScreen.adAssertionNumberOfDoors(doors);
        System.out.println("Number of doors set. Setting body parameters..");

        adCarsScreen.adBodyParameters(metalic, pearl, mat, acrylic);
        adCarsScreen.adAssertionBodyParameters(metalic, pearl, mat, acrylic);
        System.out.println("Body parameters set. Setting wheel side..");

        adCarsScreen.carsEnglishWheel(wheel);
        adCarsScreen.adAssertionWheel(wheel);
        System.out.println("English wheel set. Setting description..");

        adCarsScreen.adDescription(description);
        adCarsScreen.adAssertionDescription(description);
        System.out.println("Description set. Setting contact person..");
        driver2.rotate(ScreenOrientation.LANDSCAPE);
        methodHelper.waitTime(3);
        driver2.rotate(ScreenOrientation.PORTRAIT);
        methodHelper.waitTime(3);
        methodHelper.scrollText("Oświadczam", direction.UP, "up", 0.6, 1);
        methodHelper.waitTime(2);



        adCarsScreen.adContactPerson(person);
        adCarsScreen.adAssertionContactPerson(person);
        System.out.println("Person set. Setting phone number..");

        adCarsScreen.adPhoneNumber(number);
        adCarsScreen.adAssertionPhoneNumber(number);
        System.out.println("Number set. Accepting state..");

        adCarsScreen.adAcceptState();
        adCarsScreen.adAssertionState();
        System.out.println("State accept. Setting location..");

        adCarsScreen.adLocation(voivodeship, city);
        adCarsScreen.adAssertionLocation(voivodeship, city);
        System.out.println("City: " + city + " set. Adding ad..");
        methodHelper.waitTime(2);

        adCarsScreen.adAssertionIncorrect();
        System.out.println("Ad can't be add");

    }


    @Test(dataProvider = "getAd", description = "Adding car ad without required param - type of fuel")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - adding car ad without required param - type of fuel")
    @Story("ADDING AD: Add car ad without required param - type of fuel")
    public void testAddNoFuelTypeCarAd(int first_photo, int second_photo, int third_photo, String brand, String vin, String model,
                                       String year, String mileage, String fuel, String price, boolean negotiation, String type,
                                       String colour, String seats, String doors, String description, String voivodeship,
                                       String city, String person, String number, boolean vat, boolean funding, boolean invoice,
                                       boolean leasing, boolean metalic, boolean pearl,  boolean mat, boolean acrylic, boolean wheel) throws InterruptedException {

        WebDriver driver = getDriver();
        AppiumDriver driver2 = (AppiumDriver) getDriver();
        MethodHelper methodHelper = new MethodHelper();
        LoginScreen loginScreen = PageFactory.initElements(driver, LoginScreen.class);
        AdCarsScreen adCarsScreen = PageFactory.initElements(driver, AdCarsScreen.class);

        loginScreen.loginToApp("damian.testowy12@vp.pl", "Haslo123");
        methodHelper.waitTime(2);
        adCarsScreen.adGoToMenu();
        System.out.println("Going to menu");
        adCarsScreen.adGoToAdd();
        System.out.println("Going to ad screen");
        methodHelper.waitTime(3);
        System.out.println("Setting brand..");

        adCarsScreen.adBrandOfCar(brand);
        adCarsScreen.adAssertionBrand(brand);
        System.out.println("Brand set. Setting VIN..");

        adCarsScreen.adVin(vin);
        adCarsScreen.adAssertionVin(vin);
        System.out.println("VIN set. Setting model..");

        adCarsScreen.adModelOfCar(model);
        adCarsScreen.adAssertionModel(model);
        System.out.println("Model set. Setting year of production..");

        adCarsScreen.adYearOfProduction(year);
        adCarsScreen.adAssertionYear(year);
        System.out.println("Year of production set. Setting mileage..");

        adCarsScreen.adMileage(mileage);
        adCarsScreen.adAssertionMileage(mileage);
        System.out.println("Mileage set. Setting price..");
        methodHelper.scrollText("Lokalizacja", direction.UP, "up", 0.6, 1);

        adCarsScreen.adPrice(price, negotiation);
        adCarsScreen.adAssertionPrice(price, negotiation);
        System.out.println("Price set. Setting additional price parameters..");

        adCarsScreen.adPriceAdditional(vat, funding, invoice, leasing);
        adCarsScreen.adAssertionAdditional(vat, funding, invoice, leasing);
        System.out.println("Additional price parameters set. Setting type..");

        adCarsScreen.adTypeOfCar(type);
        adCarsScreen.adAssertionTypeOfCar(type);
        System.out.println("Type set. Setting colour..");

        adCarsScreen.adColour(colour);
        adCarsScreen.adAssertionColour(colour);
        System.out.println("Colour set. Setting number of seats..");

        methodHelper.scrollText("Opis", direction.UP, "up", 0.5, 1);

        adCarsScreen.adNumberOfSeats(seats);
        adCarsScreen.adAssertionNumberOfSeats(seats);
        System.out.println("Number of seats set. Setting number of doors..");

        adCarsScreen.adNumberOfDoors(doors);
        adCarsScreen.adAssertionNumberOfDoors(doors);
        System.out.println("Number of doors set. Setting body parameters..");

        adCarsScreen.adBodyParameters(metalic, pearl, mat, acrylic);
        adCarsScreen.adAssertionBodyParameters(metalic, pearl, mat, acrylic);
        System.out.println("Body parameters set. Setting wheel side..");

        adCarsScreen.carsEnglishWheel(wheel);
        adCarsScreen.adAssertionWheel(wheel);
        System.out.println("English wheel set. Setting description..");

        adCarsScreen.adDescription(description);
        adCarsScreen.adAssertionDescription(description);
        System.out.println("Description set. Setting contact person..");
        driver2.rotate(ScreenOrientation.LANDSCAPE);
        methodHelper.waitTime(3);
        driver2.rotate(ScreenOrientation.PORTRAIT);
        methodHelper.waitTime(3);
        methodHelper.scrollText("Oświadczam", direction.UP, "up", 0.6, 1);
        methodHelper.waitTime(2);



        adCarsScreen.adContactPerson(person);
        adCarsScreen.adAssertionContactPerson(person);
        System.out.println("Person set. Setting phone number..");

        adCarsScreen.adPhoneNumber(number);
        adCarsScreen.adAssertionPhoneNumber(number);
        System.out.println("Number set. Accepting state..");

        adCarsScreen.adAcceptState();
        adCarsScreen.adAssertionState();
        System.out.println("State accept. Setting location..");

        adCarsScreen.adLocation(voivodeship, city);
        adCarsScreen.adAssertionLocation(voivodeship, city);
        System.out.println("City: " + city + " set. Adding ad..");
        methodHelper.waitTime(2);

        adCarsScreen.adAssertionIncorrect();
        System.out.println("Ad can't be add");

    }

    @Test(dataProvider = "getAd", description = "Adding car ad without required param - price")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - adding car ad without required param - price")
    @Story("ADDING AD: Add car ad without required param - price")
    public void testAddNoPriceCarAd(int first_photo, int second_photo, int third_photo, String brand, String vin, String model,
                                    String year, String mileage, String fuel, String price, boolean negotiation, String type,
                                    String colour, String seats, String doors, String description, String voivodeship,
                                    String city, String person, String number, boolean vat, boolean funding, boolean invoice,
                                    boolean leasing, boolean metalic, boolean pearl,  boolean mat, boolean acrylic, boolean wheel) throws InterruptedException {

        WebDriver driver = getDriver();
        AppiumDriver driver2 = (AppiumDriver) getDriver();
        MethodHelper methodHelper = new MethodHelper();
        LoginScreen loginScreen = PageFactory.initElements(driver, LoginScreen.class);
        AdCarsScreen adCarsScreen = PageFactory.initElements(driver, AdCarsScreen.class);



        loginScreen.loginToApp("damian.testowy12@vp.pl", "Haslo123");
        methodHelper.waitTime(2);
        adCarsScreen.adGoToMenu();
        System.out.println("Going to menu");
        adCarsScreen.adGoToAdd();
        System.out.println("Going to ad screen");
        methodHelper.waitTime(3);
        System.out.println("Setting brand..");

        adCarsScreen.adBrandOfCar(brand);
        adCarsScreen.adAssertionBrand(brand);
        System.out.println("Brand set. Setting VIN..");

        adCarsScreen.adVin(vin);
        adCarsScreen.adAssertionVin(vin);
        System.out.println("VIN set. Setting model..");

        adCarsScreen.adModelOfCar(model);
        adCarsScreen.adAssertionModel(model);
        System.out.println("Model set. Setting year of production..");

        adCarsScreen.adYearOfProduction(year);
        adCarsScreen.adAssertionYear(year);
        System.out.println("Year of production set. Setting mileage..");

        adCarsScreen.adMileage(mileage);
        adCarsScreen.adAssertionMileage(mileage);
        System.out.println("Mileage set. Setting type of fuel..");

        adCarsScreen.adTypeOfFuel(fuel);
        adCarsScreen.adAssertionFuel(fuel);
        System.out.println("Fuel set. Setting price..");
        methodHelper.scrollText("Lokalizacja", direction.UP, "up", 0.6, 1);

//        adCarsScreen.adPrice(price, negotiation);
//        adCarsScreen.adAssertionPrice(price, negotiation);
//        System.out.println("Price set. Setting additional price parameters..");
//
//        adCarsScreen.adPriceAdditional(vat, funding, invoice, leasing);
//        adCarsScreen.adAssertionAdditional(vat, funding, invoice, leasing);
//        System.out.println("Additional price parameters set. Setting type..");

        adCarsScreen.adTypeOfCar(type);
        adCarsScreen.adAssertionTypeOfCar(type);
        System.out.println("Type set. Setting colour..");

        adCarsScreen.adColour(colour);
        adCarsScreen.adAssertionColour(colour);

        methodHelper.scrollText("Opis", direction.UP, "up", 0.4, 1);

        adCarsScreen.adNumberOfSeats(seats);
        adCarsScreen.adAssertionNumberOfSeats(seats);
        System.out.println("Number of seats set. Setting number of doors..");

        adCarsScreen.adNumberOfDoors(doors);
        adCarsScreen.adAssertionNumberOfDoors(doors);
        System.out.println("Number of doors set. Setting body parameters..");

        adCarsScreen.adBodyParameters(metalic, pearl, mat, acrylic);
        adCarsScreen.adAssertionBodyParameters(metalic, pearl, mat, acrylic);
        System.out.println("Body parameters set. Setting wheel side..");

        adCarsScreen.carsEnglishWheel(wheel);
        adCarsScreen.adAssertionWheel(wheel);
        System.out.println("English wheel set. Setting contact person..");

        adCarsScreen.adDescription(description);
        adCarsScreen.adAssertionDescription(description);

        driver2.rotate(ScreenOrientation.LANDSCAPE);
        methodHelper.waitTime(3);
        driver2.rotate(ScreenOrientation.PORTRAIT);
        methodHelper.waitTime(3);
        methodHelper.scrollText("Oświadczam", direction.UP, "up", 0.6, 1);
        methodHelper.waitTime(2);

        adCarsScreen.adContactPerson(person);
        adCarsScreen.adAssertionContactPerson(person);
        System.out.println("Person set. Setting phone number..");

        adCarsScreen.adPhoneNumber(number);
        adCarsScreen.adAssertionPhoneNumber(number);
        System.out.println("Number set.");

        adCarsScreen.adAcceptState();
        adCarsScreen.adAssertionState();
        System.out.println("State accept. Setting location..");

        adCarsScreen.adLocation(voivodeship, city);
        adCarsScreen.adAssertionLocation(voivodeship, city);
        System.out.println("City: " + city + " set. Adding ad..");
        methodHelper.waitTime(2);

        adCarsScreen.adAssertionIncorrect();

        System.out.println("Can't add ad");


    }

    @Test(dataProvider = "getAd", description = "Adding car ad without required param - type of car")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - adding car ad without required param - type of car")
    @Story("ADDING AD: Add car ad without required param - type of car")
    public void testAddNoTypeCarAd(int first_photo, int second_photo, int third_photo, String brand, String vin, String model,
                                   String year, String mileage, String fuel, String price, boolean negotiation, String type,
                                   String colour, String seats, String doors, String description, String voivodeship,
                                   String city, String person, String number, boolean vat, boolean funding, boolean invoice,
                                   boolean leasing, boolean metalic, boolean pearl,  boolean mat, boolean acrylic, boolean wheel) throws InterruptedException {

        WebDriver driver = getDriver();
        AppiumDriver driver2 = (AppiumDriver) getDriver();
        MethodHelper methodHelper = new MethodHelper();
        LoginScreen loginScreen = PageFactory.initElements(driver, LoginScreen.class);
        AdCarsScreen adCarsScreen = PageFactory.initElements(driver, AdCarsScreen.class);




        loginScreen.loginToApp("damian.testowy12@vp.pl", "Haslo123");
        methodHelper.waitTime(2);
        adCarsScreen.adGoToMenu();
        System.out.println("Going to menu");
        adCarsScreen.adGoToAdd();
        System.out.println("Going to ad screen");
        methodHelper.waitTime(3);
        System.out.println("Setting brand..");

        adCarsScreen.adBrandOfCar(brand);
        adCarsScreen.adAssertionBrand(brand);
        System.out.println("Brand set. Setting VIN..");

        adCarsScreen.adVin(vin);
        adCarsScreen.adAssertionVin(vin);
        System.out.println("VIN set. Setting model..");

        adCarsScreen.adModelOfCar(model);
        adCarsScreen.adAssertionModel(model);
        System.out.println("Model set. Setting year of production..");

        adCarsScreen.adYearOfProduction(year);
        adCarsScreen.adAssertionYear(year);
        System.out.println("Year of production set. Setting mileage..");

        adCarsScreen.adMileage(mileage);
        adCarsScreen.adAssertionMileage(mileage);
        System.out.println("Mileage set. Setting type of fuel..");

        adCarsScreen.adTypeOfFuel(fuel);
        adCarsScreen.adAssertionFuel(fuel);
        System.out.println("Fuel set. Setting price..");
        methodHelper.scrollText("Lokalizacja", direction.UP, "up", 0.6, 1);

        adCarsScreen.adPrice(price, negotiation);
        adCarsScreen.adAssertionPrice(price, negotiation);
        System.out.println("Price set. Setting additional price parameters..");

        adCarsScreen.adPriceAdditional(vat, funding, invoice, leasing);
        adCarsScreen.adAssertionAdditional(vat, funding, invoice, leasing);
        System.out.println("Additional price parameters set. Setting type..");

//        adCarsScreen.adTypeOfCar(type);
//        System.out.println("Type set. Setting colour..");
//
        adCarsScreen.adColour(colour);
        adCarsScreen.adAssertionColour(colour);

        methodHelper.scrollText("Opis", direction.UP, "up", 0.4, 1);

//        adCarsScreen.adNumberOfSeats(seats);
//        adCarsScreen.adAssertionNumberOfSeats(seats);
//        System.out.println("Number of seats set. Setting number of doors..");
//
//        adCarsScreen.adNumberOfDoors(doors);
//        adCarsScreen.adAssertionNumberOfDoors(doors);
//        System.out.println("Number of doors set. Setting body parameters..");
//
//        adCarsScreen.adBodyParameters(metalic, pearl, mat, acrylic);
//        adCarsScreen.adAssertionBodyParameters(metalic, pearl, mat, acrylic);
//        System.out.println("Body parameters set. Setting wheel side..");
//
//        adCarsScreen.carsEnglishWheel(wheel);
//        adCarsScreen.adAssertionWheel(wheel);
//        System.out.println("English wheel set. Setting contact person..");
        methodHelper.waitTime(2);
        adCarsScreen.adDescription(description);
        adCarsScreen.adAssertionDescriptionFuel(description);

        driver2.rotate(ScreenOrientation.LANDSCAPE);
        methodHelper.waitTime(3);
        driver2.rotate(ScreenOrientation.PORTRAIT);
        methodHelper.waitTime(3);
        methodHelper.scrollText("Oświadczam", direction.UP, "up", 0.6, 1);
        methodHelper.waitTime(2);

        adCarsScreen.adContactPerson(person);
        adCarsScreen.adAssertionContactPerson(person);
        System.out.println("Person set. Setting phone number..");

        adCarsScreen.adPhoneNumber(number);
        adCarsScreen.adAssertionPhoneNumber(number);
        System.out.println("Number set.");

        adCarsScreen.adAcceptState();
        adCarsScreen.adAssertionState();
        System.out.println("State accept. Setting location..");

        adCarsScreen.adLocation(voivodeship, city);
        adCarsScreen.adAssertionLocation(voivodeship, city);
        System.out.println("City: " + city + " set. Adding ad..");
        methodHelper.waitTime(2);

        adCarsScreen.adAssertionIncorrect();

        System.out.println("Can't add ad");
    }


    @Test(dataProvider = "getAd", description = "Adding car ad without required param - colour of car")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - adding car ad without required param - colour of car")
    @Story("ADDING AD: Add car ad without required param - colour of car")
    public void testAddNoColourCarAd(int first_photo, int second_photo, int third_photo, String brand, String vin, String model,
                                     String year, String mileage, String fuel, String price, boolean negotiation, String type,
                                     String colour, String seats, String doors, String description, String voivodeship,
                                     String city, String person, String number, boolean vat, boolean funding, boolean invoice,
                                     boolean leasing, boolean metalic, boolean pearl,  boolean mat, boolean acrylic, boolean wheel) throws InterruptedException {

        WebDriver driver = getDriver();
        AppiumDriver driver2 = (AppiumDriver) getDriver();
        MethodHelper methodHelper = new MethodHelper();
        LoginScreen loginScreen = PageFactory.initElements(driver, LoginScreen.class);
        AdCarsScreen adCarsScreen = PageFactory.initElements(driver, AdCarsScreen.class);


        loginScreen.loginToApp("damian.testowy12@vp.pl", "Haslo123");
        methodHelper.waitTime(2);
        adCarsScreen.adGoToMenu();
        System.out.println("Going to menu");
        adCarsScreen.adGoToAdd();
        System.out.println("Going to ad screen");
        methodHelper.waitTime(3);
        System.out.println("Setting brand..");

        adCarsScreen.adBrandOfCar(brand);
        adCarsScreen.adAssertionBrand(brand);
        System.out.println("Brand set. Setting VIN..");

        adCarsScreen.adVin(vin);
        adCarsScreen.adAssertionVin(vin);
        System.out.println("VIN set. Setting model..");

        adCarsScreen.adModelOfCar(model);
        adCarsScreen.adAssertionModel(model);
        System.out.println("Model set. Setting year of production..");

        adCarsScreen.adYearOfProduction(year);
        adCarsScreen.adAssertionYear(year);
        System.out.println("Year of production set. Setting mileage..");

        adCarsScreen.adMileage(mileage);
        adCarsScreen.adAssertionMileage(mileage);
        System.out.println("Mileage set. Setting type of fuel..");

        adCarsScreen.adTypeOfFuel(fuel);
        adCarsScreen.adAssertionFuel(fuel);
        System.out.println("Fuel set. Setting price..");
        methodHelper.scrollText("Lokalizacja", direction.UP, "up", 0.6, 1);

        adCarsScreen.adPrice(price, negotiation);
        adCarsScreen.adAssertionPrice(price, negotiation);
        System.out.println("Price set. Setting additional price parameters..");

        adCarsScreen.adPriceAdditional(vat, funding, invoice, leasing);
        adCarsScreen.adAssertionAdditional(vat, funding, invoice, leasing);
        System.out.println("Additional price parameters set. Setting type..");

        adCarsScreen.adTypeOfCar(type);
        adCarsScreen.adAssertionTypeOfCar(type);
        System.out.println("Type set. Setting colour..");

        methodHelper.scrollText("Opis", direction.UP, "up", 0.5, 1);

        adCarsScreen.adNumberOfSeats(seats);
        adCarsScreen.adAssertionNumberOfSeats(seats);
        System.out.println("Number of seats set. Setting number of doors..");

        adCarsScreen.adNumberOfDoors(doors);
        adCarsScreen.adAssertionNumberOfDoors(doors);
        System.out.println("Number of doors set. Setting body parameters..");

        adCarsScreen.adBodyParameters(metalic, pearl, mat, acrylic);
        adCarsScreen.adAssertionBodyParameters(metalic, pearl, mat, acrylic);
        System.out.println("Body parameters set. Setting wheel side..");

        adCarsScreen.carsEnglishWheel(wheel);
        adCarsScreen.adAssertionWheel(wheel);
        System.out.println("English wheel set. Setting contact person..");

        adCarsScreen.adDescription(description);
        adCarsScreen.adAssertionDescription(description);

        driver2.rotate(ScreenOrientation.LANDSCAPE);
        methodHelper.waitTime(3);
        driver2.rotate(ScreenOrientation.PORTRAIT);
        methodHelper.waitTime(3);
        methodHelper.scrollText("Oświadczam", direction.UP, "up", 0.6, 1);
        methodHelper.waitTime(2);

        adCarsScreen.adContactPerson(person);
        adCarsScreen.adAssertionContactPerson(person);
        System.out.println("Person set. Setting phone number..");

        adCarsScreen.adPhoneNumber(number);
        adCarsScreen.adAssertionPhoneNumber(number);
        System.out.println("Number set.");

        adCarsScreen.adAcceptState();
        adCarsScreen.adAssertionState();
        System.out.println("State accept. Setting location..");

        adCarsScreen.adLocation(voivodeship, city);
        adCarsScreen.adAssertionLocation(voivodeship, city);
        System.out.println("City: " + city + " set. Adding ad..");
        methodHelper.waitTime(2);

        methodHelper.waitTime(2);
        adCarsScreen.adAssertionIncorrect();

        System.out.println("Can't add ad");


    }


    @Test(dataProvider = "getAd", description = "Adding car ad without required param - accept state")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - adding car ad without required param - accept state")
    @Story("ADDING AD: Add car ad without required param - accept state")
    public void testAddNoPrivacy(int first_photo, int second_photo, int third_photo, String brand, String vin, String model,
                                   String year, String mileage, String fuel, String price, boolean negotiation, String type,
                                   String colour, String seats, String doors, String description, String voivodeship,
                                   String city, String person, String number, boolean vat, boolean funding, boolean invoice,
                                   boolean leasing, boolean metalic, boolean pearl,  boolean mat, boolean acrylic, boolean wheel) throws InterruptedException {

        WebDriver driver = getDriver();
        AppiumDriver driver2 = (AppiumDriver) getDriver();
        MethodHelper methodHelper = new MethodHelper();
        LoginScreen loginScreen = PageFactory.initElements(driver, LoginScreen.class);
        AdCarsScreen adCarsScreen = PageFactory.initElements(driver, AdCarsScreen.class);

        loginScreen.loginToApp("damian.testowy12@vp.pl", "Haslo123");
        methodHelper.waitTime(2);
        adCarsScreen.adGoToMenu();
        System.out.println("Going to menu");
        adCarsScreen.adGoToAdd();
        System.out.println("Going to ad screen");
        methodHelper.waitTime(3);
        System.out.println("Setting brand..");

        adCarsScreen.adBrandOfCar(brand);
        adCarsScreen.adAssertionBrand(brand);
        System.out.println("Brand set. Setting VIN..");

        adCarsScreen.adVin(vin);
        adCarsScreen.adAssertionVin(vin);
        System.out.println("VIN set. Setting model..");

        adCarsScreen.adModelOfCar(model);
        adCarsScreen.adAssertionModel(model);
        System.out.println("Model set. Setting year of production..");

        adCarsScreen.adYearOfProduction(year);
        adCarsScreen.adAssertionYear(year);
        System.out.println("Year of production set. Setting mileage..");

        adCarsScreen.adMileage(mileage);
        adCarsScreen.adAssertionMileage(mileage);
        System.out.println("Mileage set. Setting type of fuel..");

        adCarsScreen.adTypeOfFuel(fuel);
        adCarsScreen.adAssertionFuel(fuel);
        System.out.println("Fuel set. Setting price..");
        methodHelper.scrollText("Lokalizacja", direction.UP, "up", 0.6, 1);

        adCarsScreen.adPrice(price, negotiation);
        adCarsScreen.adAssertionPrice(price, negotiation);
        System.out.println("Price set. Setting additional price parameters..");

        adCarsScreen.adPriceAdditional(vat, funding, invoice, leasing);
        adCarsScreen.adAssertionAdditional(vat, funding, invoice, leasing);
        System.out.println("Additional price parameters set. Setting type..");

        adCarsScreen.adTypeOfCar(type);
        adCarsScreen.adAssertionTypeOfCar(type);
        System.out.println("Type set. Setting colour..");

        adCarsScreen.adColour(colour);
        adCarsScreen.adAssertionColour(colour);
        System.out.println("Colour set. Setting number of seats..");

        methodHelper.scrollText("Opis", direction.UP, "up", 0.5, 1);

        adCarsScreen.adNumberOfSeats(seats);
        adCarsScreen.adAssertionNumberOfSeats(seats);
        System.out.println("Number of seats set. Setting number of doors..");

        adCarsScreen.adNumberOfDoors(doors);
        adCarsScreen.adAssertionNumberOfDoors(doors);
        System.out.println("Number of doors set. Setting body parameters..");

        adCarsScreen.adBodyParameters(metalic, pearl, mat, acrylic);
        adCarsScreen.adAssertionBodyParameters(metalic, pearl, mat, acrylic);
        System.out.println("Body parameters set. Setting wheel side..");

        adCarsScreen.carsEnglishWheel(wheel);
        adCarsScreen.adAssertionWheel(wheel);
        System.out.println("English wheel set. Setting contact person..");

        adCarsScreen.adDescription(description);
        adCarsScreen.adAssertionDescription(description);

        driver2.rotate(ScreenOrientation.LANDSCAPE);
        methodHelper.waitTime(3);
        driver2.rotate(ScreenOrientation.PORTRAIT);
        methodHelper.waitTime(3);
        methodHelper.scrollText("Oświadczam", direction.UP, "up", 0.6, 1);
        methodHelper.waitTime(2);



        adCarsScreen.adContactPerson(person);
        adCarsScreen.adAssertionContactPerson(person);
        System.out.println("Person set. Setting phone number..");

        adCarsScreen.adPhoneNumber(number);
        adCarsScreen.adAssertionPhoneNumber(number);
        System.out.println("Number set. Setting location..");


        adCarsScreen.adLocation(voivodeship, city);
        adCarsScreen.adAssertionLocation(voivodeship, city);
        System.out.println("City: " + city + " set. Adding ad..");
        methodHelper.waitTime(2);

        adCarsScreen.adPreview();
        methodHelper.waitTime(2);
        adCarsScreen.adAssertionIncorrectState();

        System.out.println("Can't add ad");

    }


    @Test(dataProvider = "getAd", description = "Adding car ad without required param - location")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - adding car ad without required param - location")
    @Story("ADDING AD: Add car ad without required param - location")
    public void testAddNoLocationCarAd(int first_photo, int second_photo, int third_photo, String brand, String vin, String model,
                                       String year, String mileage, String fuel, String price, boolean negotiation, String type,
                                       String colour, String seats, String doors, String description, String voivodeship,
                                       String city, String person, String number, boolean vat, boolean funding, boolean invoice,
                                       boolean leasing, boolean metalic, boolean pearl,  boolean mat, boolean acrylic, boolean wheel) throws InterruptedException {

        WebDriver driver = getDriver();
        AppiumDriver driver2 = (AppiumDriver) getDriver();
        MethodHelper methodHelper = new MethodHelper();
        LoginScreen loginScreen = PageFactory.initElements(driver, LoginScreen.class);
        AdCarsScreen adCarsScreen = PageFactory.initElements(driver, AdCarsScreen.class);
        loginScreen.loginToApp("damian.testowy12@vp.pl", "Haslo123");
        methodHelper.waitTime(2);
        adCarsScreen.adGoToMenu();
        System.out.println("Going to menu");
        adCarsScreen.adGoToAdd();
        System.out.println("Going to ad screen");
        methodHelper.waitTime(3);
        System.out.println("Setting brand..");

        adCarsScreen.adBrandOfCar(brand);
        adCarsScreen.adAssertionBrand(brand);
        System.out.println("Brand set. Setting VIN..");

        adCarsScreen.adVin(vin);
        adCarsScreen.adAssertionVin(vin);
        System.out.println("VIN set. Setting model..");

        adCarsScreen.adModelOfCar(model);
        adCarsScreen.adAssertionModel(model);
        System.out.println("Model set. Setting year of production..");

        adCarsScreen.adYearOfProduction(year);
        adCarsScreen.adAssertionYear(year);
        System.out.println("Year of production set. Setting mileage..");

        adCarsScreen.adMileage(mileage);
        adCarsScreen.adAssertionMileage(mileage);
        System.out.println("Mileage set. Setting type of fuel..");

        adCarsScreen.adTypeOfFuel(fuel);
        adCarsScreen.adAssertionFuel(fuel);
        System.out.println("Fuel set. Setting price..");
        methodHelper.scrollText("Lokalizacja", direction.UP, "up", 0.6, 1);

        adCarsScreen.adPrice(price, negotiation);
        adCarsScreen.adAssertionPrice(price, negotiation);
        System.out.println("Price set. Setting additional price parameters..");

        adCarsScreen.adPriceAdditional(vat, funding, invoice, leasing);
        adCarsScreen.adAssertionAdditional(vat, funding, invoice, leasing);
        System.out.println("Additional price parameters set. Setting type..");

        adCarsScreen.adTypeOfCar(type);
        adCarsScreen.adAssertionTypeOfCar(type);
        System.out.println("Type set. Setting colour..");

        adCarsScreen.adColour(colour);
        adCarsScreen.adAssertionColour(colour);
        System.out.println("Colour set. Setting number of seats..");

        methodHelper.scrollText("Opis", direction.UP, "up", 0.5, 1);

        adCarsScreen.adNumberOfSeats(seats);
        adCarsScreen.adAssertionNumberOfSeats(seats);
        System.out.println("Number of seats set. Setting number of doors..");

        adCarsScreen.adNumberOfDoors(doors);
        adCarsScreen.adAssertionNumberOfDoors(doors);
        System.out.println("Number of doors set. Setting body parameters..");

        adCarsScreen.adBodyParameters(metalic, pearl, mat, acrylic);
        adCarsScreen.adAssertionBodyParameters(metalic, pearl, mat, acrylic);
        System.out.println("Body parameters set. Setting wheel side..");

        adCarsScreen.carsEnglishWheel(wheel);
        adCarsScreen.adAssertionWheel(wheel);
        System.out.println("English wheel set. Setting contact person..");

        adCarsScreen.adDescription(description);
        adCarsScreen.adAssertionDescription(description);

        driver2.rotate(ScreenOrientation.LANDSCAPE);
        methodHelper.waitTime(3);
        driver2.rotate(ScreenOrientation.PORTRAIT);
        methodHelper.waitTime(3);
        methodHelper.scrollText("Oświadczam", direction.UP, "up", 0.6, 1);
        methodHelper.waitTime(2);



        adCarsScreen.adContactPerson(person);
        adCarsScreen.adAssertionContactPerson(person);
        System.out.println("Person set. Setting phone number..");

        adCarsScreen.adPhoneNumber(number);
        adCarsScreen.adAssertionPhoneNumber(number);
        System.out.println("Number set.");


        methodHelper.waitTime(2);
        adCarsScreen.adAssertionIncorrect();

        System.out.println("Can't add ad");


    }

    @AfterMethod(description = "Finishing test and shutting down app")
    public void tearDown() {
        System.out.println("Test end...");
    }
}
