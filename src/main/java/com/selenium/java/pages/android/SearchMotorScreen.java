package com.selenium.java.pages.android;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.MethodHelper;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.List;

public class SearchMotorScreen extends BaseTest {

    //region BUTTONS

    @FindBy(how = How.ID, using = "pl.otomoto:id/show_more_filters")
    public WebElement btn_moreParameters;

    @FindBy(how = How.ID, using = "pl.otomoto:id/filter")
    public WebElement input_vehicleBrand;

    @FindBy(how = How.ID, using = "android:id/text1")
    public WebElement btn_chooseBrand;

    @FindBy(how = How.ID, using = "pl.otomoto:id/dialog_onboarding_filterable_single_choice_filter")
    public WebElement input_countryOfOrigin;

    @FindBy(how = How.ID, using = "android:id/text1")
    public WebElement btn_chooseCountry;

    @FindBy(how = How.ID, using = "pl.otomoto:id/chooserBtn")
    public WebElement btn_chooseLocation;

    @FindBy(how = How.ID, using = "pl.otomoto:id/dialog_onboarding_filterable_single_choice_filter")
    public WebElement input_addEquip;

    @FindBy(how = How.ID, using = "android:id/text1")
    public WebElement btn_chooseAddEquip;

    @FindBy(how = How.ID, using = "pl.otomoto:id/dialog_onboarding_filterable_single_choice_filter")
    public WebElement input_fuel;

    @FindBy(how = How.ID, using ="android:id/text1")
    public WebElement btn_chooseFuel;

    @FindBy(how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/" +
            "android.widget.FrameLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.view.ViewGroup/" +
            "android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ScrollView/android.widget." +
            "LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/" +
            "android.widget.LinearLayout[2]/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.TextView")
    public WebElement btn_setVehicleModel;

    @FindBy(how = How.ID, using = "pl.otomoto:id/et_range_min")
    public List<WebElement> input_min;

    @FindBy(how = How.ID, using = "pl.otomoto:id/et_range_max")
    public List<WebElement> input_max;

    //endregion BUTTONS


    //region METHODS
    MethodHelper helper = new MethodHelper();
    WebDriver driver = getDriver();

    @Step("Setting motors category")
    public void searchSetMotors() {

        helper.waitTime(3);

        while (helper.swipeToElementByText("MOTOCYKLE")) {
            new TouchAction((PerformsTouchActions) driver).longPress(PointOption.point(984, 312))
                    .moveTo(PointOption.point(108, 316)).release().perform();  }

        helper.waitTime(5);
        WebElement trucks_cat = getDriver().findElement(By.xpath("//*[contains(@text, '" + "MOTOCYKLE" + "')]"));
        trucks_cat.click();
        System.out.println("\"Motorcycles\" tab selected");
    }

    @Step("Setting brand of vehicle")
    public void searchVehicleBrand(String vehicleBrand) {
        WebElement chooseVehicleBrand = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Marka pojazdu" + "')]"));
        chooseVehicleBrand.click();
        System.out.println("Choosing \"Vehicle brand\" ");
        input_vehicleBrand.sendKeys(vehicleBrand);
        btn_chooseBrand.click();
        System.out.println("Selected: " + vehicleBrand);
        helper.waitTime(3);
    }

    @Step("Setting type")
    public void searchSetType(String type) {
        WebElement types = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Typ" + "')]"));
        types.click();
        System.out.println("Choosing\"Type\"");
        WebElement setCurrency = getDriver().findElement(By.xpath("//*[contains(@text, '" + type + "')]"));
        setCurrency.click();
        System.out.println("Selected: " + type);
        WebElement ready = getDriver().findElement(By.xpath("//*[contains(@text, '" + "GOTOWE" + "')]"));
        ready.click();
        helper.waitTime(3);
    }

    @Step("Setting min and max price")
    public void searchSetPrice(String priceMin, String priceMax) {
        while (helper.swipeToElementByText("Cena")) {
            helper.swipeInDirection(direction.UP, "up", 0.4);
        }

        for (int d = 0; d < input_max.size(); d++) {
            if (d == 1) {
                input_min.get(0).sendKeys(priceMin);
                input_max.get(0).sendKeys(priceMax);
            }
        }

        System.out.println("Entered minimal price: " + priceMin);
        System.out.println("Entered maximum price: " + priceMax);

    }

    @Step("Setting min and max cubic capacity")
    public void searchSetCubicCapacity(String cubicCapacityMin, String cubicCapacityMax) {
        while (helper.swipeToElementByText("Pojemność skokowa")) {
            helper.swipeInDirection(direction.UP, "up", 0.4);
        }
        helper.waitTime(3);
        for (int d = 0; d < input_max.size(); d++) {
            if (d == 1) {
                input_min.get(1).sendKeys(cubicCapacityMin);
                input_max.get(1).sendKeys(cubicCapacityMax);
            }
        }

        System.out.println("Entered minimal cubic capacity: " + cubicCapacityMin);
        System.out.println("Entered minimal cubic capacity: " + cubicCapacityMax);
        helper.waitTime(3);
    }

    @Step("Setting privacy")
    public void searchSetPrivacy(String privacy) {
        while (helper.swipeToElementByText("Prywatnie lub firma")) {
            helper.swipeInDirection(direction.UP, "up", 0.4);
        }
        WebElement privateOrCorpo = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Prywatnie lub firma" + "')]"));
        privateOrCorpo.click();
        System.out.println("Choosing \"Private or company\"");
        WebElement setPrivacy = getDriver().findElement(By.xpath("//*[contains(@text, '" + privacy + "')]"));
        setPrivacy.click();
        System.out.println("Selected: " + privacy);
        WebElement ready = getDriver().findElement(By.xpath("//*[contains(@text, '" + "GOTOWE" + "')]"));
        ready.click();
    }

    @Step("Setting currency")
    public void searchSetCurrency(String currency) {
        while (helper.swipeToElementByText("Waluta")) {
            helper.swipeInDirection(direction.UP, "up", 0.4);
        }
        WebElement currencies = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Waluta" + "')]"));
        currencies.click();
        System.out.println("Choosing \"Currency\"");
        WebElement setCurrency = getDriver().findElement(By.xpath("//*[contains(@text, '" + currency + "')]"));
        setCurrency.click();
        System.out.println("Selected: " + currency);
        WebElement ready = getDriver().findElement(By.xpath("//*[contains(@text, '" + "GOTOWE" + "')]"));
        ready.click();
        helper.waitTime(3);
    }

    @Step("Setting more parameters")
    public void searchMoreParameters() {
        while (helper.swipeToElementByText("WIĘCEJ PARAMETRÓW")) {
            helper.swipeInDirection(direction.UP, "up", 0.4);
        }
        btn_moreParameters.click();
        System.out.println("Choosing\"More parameters\" ");
    }

    @Step("Setting min and max year of production")
    public void searchSetYearOfProduction(String yearOfProdFrom, String yearOfProdTo) {
        while (helper.swipeToElementByText("Rodzaj paliwa")) {
            helper.swipeInDirection(direction.UP, "up", 0.4);
        }

        helper.waitTime(3);
            for (int d = 0; d < input_max.size(); d++) {
                if (d == 1) {
                    input_min.get(0).sendKeys(yearOfProdFrom);
                    input_max.get(0).sendKeys(yearOfProdTo);
                }
            }
        System.out.println("Production year selected from: " + yearOfProdFrom);
        System.out.println("Production year selected to: " + yearOfProdTo);
        helper.waitTime(3);
    }

    @Step("Setting mileage")
    public void searchSetMileage(String mileageMin, String mileageMax) {
            for (int d = 0; d < input_max.size(); d++) {
                if (d == 1) {
                    input_min.get(1).sendKeys(mileageMin);
                    input_max.get(1).sendKeys(mileageMax);
                }
            }
        System.out.println("Minimum mileage entered: " + mileageMin);
        System.out.println("Maximum mileage entered: " + mileageMax);
        helper.waitTime(3);
    }

    @Step("Setting country of origin")
    public void searchSetCountryOfOrigin(String country) {
        while (helper.swipeToElementByText("Kraj pochodzenia")) {
            helper.swipeInDirection(direction.UP, "up", 0.4);
        }
        WebElement chooseCountry = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Kraj pochodzenia" + "')]"));
        chooseCountry.click();
        System.out.println("Choosing \"Country\"");
        input_countryOfOrigin.sendKeys(country);
        btn_chooseCountry.click();
        System.out.println("Country selected: " + country);
        WebElement ready = getDriver().findElement(By.xpath("//*[contains(@text, '" + "GOTOWE" + "')]"));
        ready.click();
        helper.waitTime(3);
    }

    @Step("Setting min and max power")
    public void searchSetPower(String powerMin, String powerMax) {
        while (helper.swipeToElementByText("Moc")) {
            helper.swipeInDirection(direction.UP, "up", 0.4);
        }
        for (int d = 0; d < input_max.size(); d++) {
            if (d == 1) {
                input_min.get(0).sendKeys(powerMin);
                input_max.get(0).sendKeys(powerMax);
            }
        }
        System.out.println("Minimum power entered " + powerMin);
        System.out.println("Maximum power entered " + powerMax);
        helper.waitTime(3);
    }


    @Step("Setting type of drive")
    public void searchSetTypeOfDrive(String typeOfDrive) {

        WebElement typesOfDrive = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Rodzaj napędu" + "')]"));
        typesOfDrive.click();
        System.out.println("Choosing \"Type of drive\"");
        WebElement setGearbox = getDriver().findElement(By.xpath("//*[contains(@text, '" + typeOfDrive + "')]"));
        setGearbox.click();
        System.out.println("Selected: " + typeOfDrive);
        WebElement ready = getDriver().findElement(By.xpath("//*[contains(@text, '" + "GOTOWE" + "')]"));
        ready.click();
        helper.waitTime(3);
    }

    @Step("Setting gearbox")
    public void searchSetGearbox(String gearbox) {
        while (helper.swipeToElementByText("Skrzynia biegów")) {
            helper.swipeInDirection(direction.UP, "up", 0.4);
        }
        WebElement gearboxes = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Skrzynia biegów" + "')]"));
        gearboxes.click();
        System.out.println("Choosing \"Gearbox\"");
        WebElement setGearbox = getDriver().findElement(By.xpath("//*[contains(@text, '" + gearbox + "')]"));
        setGearbox.click();
        System.out.println("Selected: " + gearbox);
        WebElement ready = getDriver().findElement(By.xpath("//*[contains(@text, '" + "GOTOWE" + "')]"));
        ready.click();
        helper.waitTime(3);
    }

    @Step("Setting additional equipment")
    public void searchSetAdditionalEquipment(String addEquip) {
        while (helper.swipeToElementByText("Dodatkowe wyposażenie")) {
            helper.swipeInDirection(direction.UP, "up", 0.4);
        }
        WebElement addEquips = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Dodatkowe wyposażenie" + "')]"));
        addEquips.click();
        System.out.println("Choosing \"Additional equipment\"");
        input_addEquip.sendKeys(addEquip);
        btn_chooseAddEquip.click();
        System.out.println("Selected: " + addEquip);
        WebElement ready = getDriver().findElement(By.xpath("//*[contains(@text, '" + "GOTOWE" + "')]"));
        ready.click();
        helper.waitTime(3);
    }

    @Step("Setting model of vehicle")
    public void searchVehicleModel( String vehicleModel) {
        btn_setVehicleModel.click();
        helper.waitTime(3);
        System.out.println("Choosing \"Model\"");
        while (helper.swipeToElementByText(vehicleModel)) {
            new TouchAction((PerformsTouchActions) driver).longPress(PointOption.point(286, 1888))
                    .moveTo(PointOption.point(389, 599)).release().perform();  }
        WebElement model = getDriver().findElement(By.xpath("//*[contains(@text, '" + vehicleModel + "')]"));
         model.click();
        WebElement ready = getDriver().findElement(By.xpath("//*[contains(@text, '" + "GOTOWE" + "')]"));
        ready.click();
        System.out.println("Selected: " + vehicleModel);
        helper.waitTime(3);
    }

    @Step("Setting type of engine")
    public void searchSetEngineType(String engineType) {
        while (helper.swipeToElementByText("Typ Silnika")) {
            helper.swipeInDirection(direction.UP, "up", 0.4);
        }
        WebElement engineTypes = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Typ Silnika" + "')]"));
        engineTypes.click();
        System.out.println("Choosing \"Type of engine\"");
        WebElement setGearbox = getDriver().findElement(By.xpath("//*[contains(@text, '" + engineType + "')]"));
        setGearbox.click();
        System.out.println("Selected: " + engineType);
        WebElement ready = getDriver().findElement(By.xpath("//*[contains(@text, '" + "GOTOWE" + "')]"));
        ready.click();
        helper.waitTime(3);
    }

    @Step("Setting metalic")
    public void searchSetMetalic(boolean metalic) {
        while (helper.swipeToElementByText("Metalik")) {
            helper.swipeInDirection(direction.UP, "up", 0.4);
        }
        if (metalic) {
            WebElement metalics = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Metalik" + "')]"));
            metalics.click();
        }else{}
        System.out.println("Selected: " + metalic);
    }

    @Step("Setting pearly")
    public void searchSetPearly(boolean pearly) {
        if (pearly) {
            WebElement pearlies = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Perłowy" + "')]"));
            pearlies.click();
        }else{}

        System.out.println("Selected: " + pearly);

    }

    @Step("Setting mat")
    public void searchSetMat(boolean mat) {
        while (helper.swipeToElementByText("Matowy")) {
            if (mat) {
                WebElement mats = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Matowy" + "')]"));
                mats.click();
            } else {
            }
            System.out.println("Selected: " + mat);
        }
    }

    @Step("Setting colour")
    public void searchSetColour(String colour) {
        while (helper.swipeToElementByText("Kolor")) {
            helper.swipeInDirection(direction.UP, "up", 0.4);
        }
        WebElement colours = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Kolor" + "')]"));
        colours.click();
        System.out.println("Choosing \"Colour\"");
        WebElement setGearbox = getDriver().findElement(By.xpath("//*[contains(@text, '" + colour + "')]"));
        setGearbox.click();
        System.out.println("Selected: " + colour);
        WebElement ready = getDriver().findElement(By.xpath("//*[contains(@text, '" + "GOTOWE" + "')]"));
        ready.click();
        helper.waitTime(3);
    }

    @Step("Setting results")
    public void searchShowResults() {
        WebElement showResults = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Pokaż wyniki" + "')]"));
        showResults.click();
    }

    @Step("Setting financial information")
    public void searchFinancialInformation(boolean authorizedDealer, boolean vatMargin, boolean invoiceVat,
                                           boolean financingOption, boolean leasing, boolean rent) {

        System.out.println("Choosing \"Financial information\"\"");
        while (helper.swipeToElementByText("Możliwość finansowania")) {
            helper.swipeInDirection(direction.UP, "up", 0.4);
        }

        if (authorizedDealer) {
            WebElement setAuthorizedDealer = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Autoryzowanego Dealera" + "')]"));
            setAuthorizedDealer.click();
            System.out.println("Selected: Leasing");
        } else {
        }
        if (vatMargin) {
            WebElement setVatMargin = getDriver().findElement(By.xpath("//*[contains(@text, '" + "VAT marża" + "')]"));
            setVatMargin.click();
            System.out.println("Selected:  VAT margin");
        } else {
        }
        if (invoiceVat) {
            WebElement setInvoiceVat = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Faktura VAT" + "')]"));
            setInvoiceVat.click();
            System.out.println("Selected: VAT invoice");
        } else {
        }
        if (financingOption) {
            WebElement setFinancingOption = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Możliwość finansowania" + "')]"));
            setFinancingOption.click();
            System.out.println("Selected financing option");
        } else {
        }

        if (leasing) {
            WebElement setLeasing = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Leasing" + "')]"));
            setLeasing.click();
            System.out.println("Wybrano leasing");
        } else {
        }
        if (rent) {
            WebElement setRent = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Opcja wynajem" + "')]"));
            setRent.click();
            System.out.println("Selected: rent");
        } else {
        }
    }

    @Step("Setting car status")
    public void searchCarStatus(boolean registrationNumber, boolean offersWithVin, boolean damaged, boolean registerInPoland,
                                boolean noAccidents, boolean servisedASO, boolean tunning, boolean antique) {

        System.out.println("Choosing \" Vehicle status \"");
        while (helper.swipeToElementByText("Zarejestrowany w Polsce")) {
            helper.swipeInDirection(direction.UP, "up", 0.4);
        }

        if (registrationNumber) {
            WebElement setRegistrationNumber = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Ma numer rejestracyjny" + "')]"));
            setRegistrationNumber.click();
            System.out.println("Selected status: Ma numer rejestracyjny");
        } else {
        }
        if (offersWithVin) {
            WebElement setOffersWithWin = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Pokaż oferty z numerem VIN" + "')]"));
            setOffersWithWin.click();
            System.out.println("Wybrano status: Pokaż oferty z numerem VIN");
        } else {
        }
        if (damaged) {
            WebElement setDamaged = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Uszkodzony" + "')]"));
            setDamaged.click();
            System.out.println("Wybrano status: Uszkodzony");
        } else {
        }
        if (registerInPoland) {
            WebElement setRegisterInPoland = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Zarejestrowany w Polsce" + "')]"));
            setRegisterInPoland.click();
            System.out.println("Wybrano status: Zarejestrowany w Polsce");
        } else {
        }
        if (noAccidents) {
            WebElement setNoAccidents = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Bezwypadkowy" + "')]"));
            setNoAccidents.click();
            System.out.println("Wybrano status: Bezwypadkowy");
        } else {
        }
        while (helper.swipeToElementByText("Zarejestrowany jako zabytek")) {
            helper.swipeInDirection(direction.UP, "up", 0.4);
        }
        if (servisedASO) {
            WebElement setServisedASO = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Serwisowany w ASO" + "')]"));
            setServisedASO.click();
            System.out.println("Selected status: servised in ASO");
        } else {}
        if (tunning) {
            WebElement setTunning = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Tuning" + "')]"));
            setTunning.click();
            System.out.println("Wybrano status: Tunning");
        } else {
        }
        if (antique) {
            WebElement setAntique = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Zarejestrowany jako zabytek" + "')]"));
            setAntique.click();
            System.out.println("Wybrano status: Zarejestrowany jako zabytek");
        } else {}
    }

    @Step("Setting location")
    public void searchLocation(String voivodeship) {
        while (helper.swipeToElementByText("Wybierz lokalizację")) {
            helper.swipeInDirection(direction.UP, "up", 0.4);
        }
        btn_chooseLocation.click();
        System.out.println("Choosing \"Select location\" ");
        while (helper.swipeToElementByText(voivodeship)) {
            helper.swipeInDirection(direction.UP, "up", 0.4);
        }
        WebElement chooseVoivodeship = getDriver().findElement(By.xpath("//*[contains(@text, '" + voivodeship + "')]"));
        chooseVoivodeship.click();
        System.out.println("Selected voivodeship: " + voivodeship);
        helper.waitTime(2);

        WebElement chooseAllVoivodeship = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Całe " + voivodeship + "')]"));
        chooseAllVoivodeship.click();
    }

    @Step("Setting type of fuel")
    public void searchSetTypeOfFuel(String fuel) {
        while (helper.swipeToElementByText("Rodzaj paliwa")) {
            helper.swipeInDirection(direction.UP, "up", 0.4);
        }
        System.out.println("Choosing \"Type of fuel\"");
        WebElement chooseSystem = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Rodzaj paliwa" + "')]"));
        chooseSystem.click();
        input_fuel.sendKeys(fuel);
        btn_chooseFuel.click();
        System.out.println("Selected: " + fuel);
        WebElement ready = getDriver().findElement(By.xpath("//*[contains(@text, '" + "GOTOWE" + "')]"));
        ready.click();
        helper.waitTime(3);
    }


    //endregion METHODS
}
