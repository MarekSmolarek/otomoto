package com.selenium.java.pages.android;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.MethodHelper;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import java.util.List;


public class AdCarsScreen extends BaseTest {

    MethodHelper methodHelper = new MethodHelper();
    //region BUTTONS

    @FindBy(how = How.XPATH, using = "//android.widget.ImageButton[@content-desc=\"Otwórz menu\"]")
    public WebElement btn_menu;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Dodaj ogłoszenie')]")
    public WebElement btn_move_to_add;

    @FindBy(how = How.ID, using = "pl.otomoto:id/imageView")
    public WebElement btn_add_photo;

    @FindBy(how = How.ID, using = "pl.otomoto:id/img")
    public List<WebElement> list_photo;

    @FindBy(how = How.ID, using = "pl.otomoto:id/action_done")
    public WebElement btn_done;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Marka pojazdu')]")
    public WebElement btn_brand_of_car;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Model pojazdu')]")
    public WebElement btn_model_of_car;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'VIN')]")
    public WebElement input_vin;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Rok produkcji')]")
    public WebElement input_year;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Przebieg')]")
    public WebElement input_milleage;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Cena')]")
    public WebElement input_price;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Kolor')]")
    public WebElement btn_colour;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Lokalizacja')]")
    public WebElement btn_location;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Opis')]")
    public WebElement input_description;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Osoba kontaktowa')]")
    public WebElement input_person;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Numer telefonu')]")
    public WebElement input_phone;

    @FindBy(how = How.ID, using = "pl.otomoto:id/checkbox")
    public WebElement checkbox_ad;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Oświadczam, że zapoznałem się i akceptuję Regulamin')]")
    public WebElement checkbox_state;


    @FindBy(how = How.ID, using = "pl.otomoto:id/checktextview")
    public WebElement checkbox_neg;

    @FindBy(how = How.ID, using = "pl.otomoto:id/previewBtn")
    // @FindBy(how = How.XPATH, using = "//*[contains(@text, 'ZOBACZ PRZED DODANIEM')]")
    public WebElement btn_preview;

    @FindBy(how = How.ID, using = "pl.otomoto:id/addMoreBtn")
    public WebElement btn_add_more;

    @FindBy(how = How.ID, using = "pl.otomoto:id/widget_form_accordion_block_more")
    //  public WebElement btn_more;
    public List<WebElement> btn_more;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'VAT marża')]")
    public WebElement btn_vat;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Możliwość finansowania')]")
    public WebElement btn_funding;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Faktura VAT')]")
    public WebElement btn_invoice;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Leasing')]")
    public WebElement btn_leasing;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Liczba miejsc')]")
    public WebElement input_number_of_seats;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Liczba drzwi')]")
    public WebElement input_number_of_doors;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Metalik')]")
    public WebElement btn_metalic;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Perłowy')]")
    public WebElement btn_pearl;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Matowy')]")
    public WebElement btn_mat;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Akryl')]")
    public WebElement btn_acrylic;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Kierownica po prawej')]")
    public WebElement btn_wheel;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Tak')]")
    public WebElement btn_wheel_yes;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Nie')]")
    public WebElement btn_wheel_no;

    @FindBy(how = How.ID, using = "android:id/text1")
    public List<WebElement> input_choose;

    @FindBy(how = How.ID, using = "pl.otomoto:id/value")
    public List<WebElement> input_value;

    @FindBy(how = How.ID, using = "pl.otomoto:id/chooserBtn")
    public WebElement input_assert_local;
    //endregion BUTTONS

    //region METHODS

    @Step("Going to menu")
    public void adGoToMenu() {
        btn_menu.click();
    }

    @Step("Going to Add ad")
    public void adGoToAdd() {
        btn_move_to_add.click();
    }

    @Step("Adding photo")
    public void adAddPhoto(int first, int second, int third) {

        btn_add_photo.click();
        for (int d = 0; d < list_photo.size(); d++) {
            if (d == 0) {
                list_photo.get(first).click();
                list_photo.get(second).click();
                list_photo.get(third).click();
                btn_done.click();
            }
        }
    }

    @Step("Setting brand of car")
    public void adBrandOfCar(String brand) {

        btn_brand_of_car.click();

        WebElement element = getDriver().findElement(By.xpath("//*[contains(@text, 'Acura')]"));
        int leftX = element.getLocation().getX();
        int rightX = leftX + element.getSize().getWidth();
        int middleX = (rightX + leftX) / 2;
        int upperY = element.getLocation().getY();
        int lowerY = upperY + element.getSize().getHeight();
        int middleY = (upperY + lowerY) / 2;

        WebElement element2 = getDriver().findElement(By.xpath("//*[contains(@text, 'Audi')]"));
        int leftX2 = element2.getLocation().getX();
        int rightX2 = leftX2 + element2.getSize().getWidth();
        int middleX2 = (rightX2 + leftX2) / 2;
        int upperY2 = element2.getLocation().getY();
        int lowerY2 = upperY2 + element2.getSize().getHeight();
        int middleY2 = (upperY2 + lowerY2) / 2;

        methodHelper.swipeToElementByTextAndCordinates(brand, middleX2, middleY2, middleX, middleY);
        // methodHelper.scrollText(brand, BaseTest.direction.UP, "up", 0.5, 3);
        WebElement chooseType = getDriver().findElement(By.xpath("//*[contains(@text, '" + brand + "')]"));
        chooseType.click();

        methodHelper.waitTime(2);
    }

    @Step("Assertion for brand")
    public void adAssertionBrand(String brand) {
        methodHelper.waitTime(2);
        String text = input_choose.get(0).getText();
        methodHelper.testScreenshot("testAddCarAd");
        methodHelper.waitTime(2);
        Assert.assertEquals(text, brand);

    }

    @Step("Setting VIN of car")
    public void adVin(String vin) {

        input_vin.sendKeys(vin);

    }

    @Step("Assertion for VIN")
    public void adAssertionVin(String vin) {
        methodHelper.waitTime(2);
        String text = input_value.get(0).getText();
        methodHelper.testScreenshot("testAddCarAd");
        methodHelper.waitTime(2);
        Assert.assertEquals(text, vin);

    }

    @Step("Setting model of car")
    public void adModelOfCar(String model) {

        btn_model_of_car.click();
        WebElement element = getDriver().findElement(By.xpath("//*[contains(@text, 'Inny')]"));
        int leftX = element.getLocation().getX();
        int rightX = leftX + element.getSize().getWidth();
        int middleX = (rightX + leftX) / 2;
        int upperY = element.getLocation().getY();
        int lowerY = upperY + element.getSize().getHeight();
        int middleY = (upperY + lowerY) / 2;
        methodHelper.swipeToElementByTextAndCordinates(model, middleX, 2040, middleX, 1100);
        WebElement chooseModel = getDriver().findElement(By.xpath("//*[contains(@text, '" + model + "')]"));
        chooseModel.click();
    }

    @Step("Assertion for model")
    public void adAssertionModel(String model) {
        methodHelper.waitTime(2);
        String text = input_choose.get(1).getText();
        methodHelper.testScreenshot("testAddCarAd");
        methodHelper.waitTime(2);
        Assert.assertEquals(text, model);

    }

    @Step("Setting year of production")
    public void adYearOfProduction(String year) {

        input_year.sendKeys(year);

    }


    @Step("Assertion for year of production")
    public void adAssertionYear(String year) {
        methodHelper.waitTime(2);
        String text = input_value.get(1).getText();
        methodHelper.testScreenshot("testAddCarAd");
        methodHelper.waitTime(2);
        Assert.assertEquals(text, year);

    }

    @Step("Setting mileage")
    public void adMileage(String mileage) {

        input_milleage.sendKeys(mileage);
        methodHelper.waitTime(2);

    }

    @Step("Assertion for mileage")
    public void adAssertionMileage(String mileage) {
        methodHelper.waitTime(2);
        String text = input_value.get(2).getText();
        methodHelper.testScreenshot("testAddCarAd");
        methodHelper.waitTime(2);
        Assert.assertEquals(text, mileage);
    }

    @Step("Setting type of fuel")
    public void adTypeOfFuel(String type) {


        WebElement element = getDriver().findElement(By.xpath("//*[contains(@text, 'Benzyna')]"));
        int leftX = element.getLocation().getX();
        int rightX = leftX + element.getSize().getWidth();
        int middleX = (rightX + leftX) / 2;
        int upperY = element.getLocation().getY();
        int lowerY = upperY + element.getSize().getHeight();
        int middleY = (upperY + lowerY) / 2;

        methodHelper.swipeToElementByTextAndCordinates(type, 913, middleY, 146, middleY);
        WebElement chooseFuel = getDriver().findElement(By.xpath("//*[contains(@text, '" + type + "')]"));

        chooseFuel.click();


    }

    @Step("Assertion for fuel")
    public void adAssertionFuel(String type) {
        methodHelper.waitTime(2);
        WebElement chooseFuel = getDriver().findElement(By.xpath("//*[contains(@text, '" + type + "')]"));
        String text = chooseFuel.getText();

        methodHelper.testScreenshot("testAddCarAd");
        methodHelper.waitTime(2);
        Assert.assertEquals(text, type);
    }



    @Step("Setting price")
    public void adPrice(String price, boolean negotiation) {
        methodHelper.waitTime(2);
        input_price.sendKeys(price);
        if (negotiation) {
            checkbox_neg.click();
        }
        methodHelper.waitTime(2);
        btn_more.get(0).click();
    }

    @Step("Assertion for price")
    public void adAssertionPrice(String price, boolean negotiation) {
        methodHelper.waitTime(2);
        String text = checkbox_neg.getAttribute("checked");
        String text_price = input_value.get(0).getText();
        methodHelper.testScreenshot("testAddCarAd");
        Assert.assertEquals(price, text_price);

        if (negotiation) {
            Assert.assertEquals(text, "true");
        } else {
            Assert.assertEquals(text, "false");
        }
    }

    @Step("Setting additional price parameters")
    public void adPriceAdditional(boolean vat, boolean funding, boolean invoice, boolean leasing) {

        if (vat) {
            btn_vat.click();
        }

        if (funding) {
            btn_funding.click();
        }

        if (invoice) {
            btn_invoice.click();
        }

        if (leasing) {
            btn_leasing.click();
        }

    }

    @Step("Assertion for additional price parameters")
    public void adAssertionAdditional(boolean vat, boolean funding, boolean invoice, boolean leasing) {
        methodHelper.waitTime(2);
        String check_vat = btn_vat.getAttribute("checked");
        String check_fun = btn_funding.getAttribute("checked");
        String check_inv = btn_invoice.getAttribute("checked");
        String check_lea = btn_leasing.getAttribute("checked");

        methodHelper.testScreenshot("testAddCarAd");

        if (vat) {
            Assert.assertEquals(check_vat, "true");
        } else {
            Assert.assertEquals(check_vat, "false");
        }

        if (funding) {

            Assert.assertEquals(check_fun, "true");
        } else {
            Assert.assertEquals(check_fun, "false");
        }

        if (invoice) {

            Assert.assertEquals(check_inv, "true");
        } else {
            Assert.assertEquals(check_inv, "false");
        }

        if (leasing) {

            Assert.assertEquals(check_lea, "true");
        } else {
            Assert.assertEquals(check_lea, "false");
        }

    }

    @Step("Setting type of car")
    public void adTypeOfCar(String type) {

methodHelper.waitTime(2);
        //  btn_more.click();
        btn_more.get(1).click();

        WebElement element = getDriver().findElement(By.xpath("//*[contains(@text, 'Auta małe')]"));
        int leftX = element.getLocation().getX();
        int rightX = leftX + element.getSize().getWidth();
        int middleX = (rightX + leftX) / 2;
        int upperY = element.getLocation().getY();
        int lowerY = upperY + element.getSize().getHeight();
        int middleY = (upperY + lowerY) / 2;

        methodHelper.swipeToElementByTextAndCordinates(type, 913, middleY, 146, middleY);
        WebElement chooseType = getDriver().findElement(By.xpath("//*[contains(@text, '" + type + "')]"));
        chooseType.click();
        methodHelper.waitTime(2);

    }


    @Step("Assertion for type of car")
    public void adAssertionTypeOfCar(String type) {
        methodHelper.waitTime(2);
        WebElement chooseType = getDriver().findElement(By.xpath("//*[contains(@text, '" + type + "')]"));
        String text = chooseType.getText();

        methodHelper.testScreenshot("testAddCarAd");
        methodHelper.waitTime(2);

        Assert.assertEquals(text, type);
    }

    @Step("Setting colour")
    public void adColour(String colour) {

        btn_colour.click();
        //   methodHelper.scrollText(colour, BaseTest.direction.UP, "up", 0.5, 3);
        WebElement element = getDriver().findElement(By.xpath("//*[contains(@text, 'Beżowy')]"));
        int leftX = element.getLocation().getX();
        int rightX = leftX + element.getSize().getWidth();
        int middleX = (rightX + leftX) / 2;
        int upperY = element.getLocation().getY();
        int lowerY = upperY + element.getSize().getHeight();
        int middleY = (upperY + lowerY) / 2;

        methodHelper.swipeToElementByTextAndCordinates(colour, middleX, 2040, middleX, middleY);
        WebElement chooseColour = getDriver().findElement(By.xpath("//*[contains(@text, '" + colour + "')]"));
        chooseColour.click();

    }

    @Step("Assertion for colour")
    public void adAssertionColour(String colour) {
        methodHelper.waitTime(2);
        String text = input_choose.get(1).getText();
        methodHelper.testScreenshot("testAddCarAd");
        methodHelper.waitTime(2);
        Assert.assertEquals(text, colour);

    }


    @Step("Setting number of seats")
    public void adNumberOfSeats(String seats) {

        input_number_of_seats.sendKeys(seats);

    }


    @Step("Assertion for number of seats")
    public void adAssertionNumberOfSeats(String seats) {
        methodHelper.waitTime(2);
        String text = input_value.get(0).getText();
        methodHelper.testScreenshot("testAddCarAd");
        methodHelper.waitTime(2);
        Assert.assertEquals(text, seats);
    }

    @Step("Setting number of doors")
    public void adNumberOfDoors(String doors) {
        input_number_of_doors.sendKeys(doors);

    }

    @Step("Assertion for number of doors")
    public void adAssertionNumberOfDoors(String doors) {
        methodHelper.waitTime(2);
        String text = input_value.get(1).getText();
        methodHelper.testScreenshot("testAddCarAd");
        methodHelper.waitTime(2);
        Assert.assertEquals(text, doors);
    }

    @Step("Setting body parameters")
    public void adBodyParameters(boolean metalic, boolean pearl, boolean mat, boolean acrylic) {

        if (metalic) {

            btn_metalic.click();
        }

        if (pearl) {

            btn_pearl.click();
        }

        if (mat) {
            btn_mat.click();

        }

        if (acrylic) {

            btn_acrylic.click();

        }

    }

    @Step("Assertion for body parameters")
    public void adAssertionBodyParameters(boolean metalic, boolean pearl, boolean mat, boolean acrylic) {
        methodHelper.waitTime(2);
        String check_met = btn_metalic.getAttribute("checked");
        String check_pea = btn_pearl.getAttribute("checked");
        String check_mat = btn_mat.getAttribute("checked");
        String check_acr = btn_acrylic.getAttribute("checked");
        methodHelper.testScreenshot("testAddCarAd");
        methodHelper.waitTime(2);

        if (metalic) {

            Assert.assertEquals(check_met, "true");
        } else {
            Assert.assertEquals(check_met, "false");
        }

        if (pearl) {

            Assert.assertEquals(check_pea, "true");
        } else {
            Assert.assertEquals(check_pea, "false");
        }

        if (mat) {

            Assert.assertEquals(check_mat, "true");
        } else {
            Assert.assertEquals(check_mat, "false");
        }

        if (acrylic) {

            Assert.assertEquals(check_acr, "true");
        } else {
            Assert.assertEquals(check_acr, "false");
        }

    }


    @Step("Setting left or right side wheel")
    public void carsEnglishWheel(boolean wheel) {
        btn_wheel.click();

        if (wheel) {
            btn_wheel_yes.click();
        } else {
            btn_wheel_no.click();
        }

    }

    @Step("Assertion for wheel side")
    public void adAssertionWheel(boolean wheel) {
        methodHelper.waitTime(2);
        String text = input_choose.get(1).getText();

        methodHelper.testScreenshot("testAddCarAd");
        methodHelper.waitTime(2);
        if (wheel) {

            Assert.assertEquals(text, "Tak");
        } else {
            Assert.assertEquals(text, "Nie");
        }

    }


    @Step("Setting description")
    public void adDescription(String description) {
        input_description.sendKeys(description);
    }

    @Step("Assertion for description")
    public void adAssertionDescription(String description) {
        methodHelper.waitTime(2);
        String text = input_value.get(2).getText();
        methodHelper.testScreenshot("testAddCarAd");
        methodHelper.waitTime(2);
        Assert.assertEquals(text, description);
    }

    @Step("Assertion for description")
    public void adAssertionDescriptionFuel(String description) {
        methodHelper.waitTime(2);
        String text = input_value.get(1).getText();
        methodHelper.testScreenshot("testAddCarAd");
        methodHelper.waitTime(2);
        Assert.assertEquals(text, description);
    }

    @Step("Setting location")
    public void adLocation(String voivodeship, String city) {

        btn_location.click();


        methodHelper.scrollText(voivodeship, direction.UP, "up", 0.3, 1);

        WebElement chooseVoivodeship = getDriver().findElement(By.xpath("//*[contains(@text, '" +
                voivodeship + "')]"));
        chooseVoivodeship.click();

        methodHelper.waitTime(2);

        methodHelper.scrollText(city, direction.UP, "up", 0.3, 1);
        WebElement chooseCity = getDriver().findElement(By.xpath("//*[contains(@text, '" + city + "')]"));
        chooseCity.click();
    }

    @Step("Assertion for location")
    public void adAssertionLocation(String voivodeship, String city) {
        methodHelper.waitTime(2);
        String text = input_assert_local.getText();
        methodHelper.testScreenshot("testAddCarAd");
        methodHelper.waitTime(2);
        Assert.assertEquals(text, voivodeship + ", " + city);
    }

    @Step("Setting contact person")
    public void adContactPerson(String person) {

        input_person.sendKeys(person);

    }

    @Step("Assertion for contact person")
    public void adAssertionContactPerson(String person) {
        methodHelper.waitTime(2);
        String text = input_value.get(1).getText();
        methodHelper.testScreenshot("testAddCarAd");
        methodHelper.waitTime(2);
        Assert.assertEquals(text, person);
    }

    @Step("Setting phone number")
    public void adPhoneNumber(String number) {

        input_phone.sendKeys(number);
    }

    @Step("Assertion for phone number")
    public void adAssertionPhoneNumber(String number) {
        methodHelper.waitTime(2);
        String text = input_value.get(3).getText();

        methodHelper.testScreenshot("testAddCarAd");
        methodHelper.waitTime(2);
        Assert.assertEquals(text, number);

    }

    @Step("Setting accept state")
    public void adAcceptState() {
        checkbox_state.click();
    }


    @Step("Assertion for state")
    public void adAssertionState() {
        methodHelper.waitTime(2);
        String text = checkbox_state.getAttribute("checked");

        methodHelper.testScreenshot("testAddCarAd");
        methodHelper.waitTime(2);
        Assert.assertEquals(text, "true");


    }


    @Step("Preview ad")
    public void adPreview() {
        btn_preview.click();
    }



    @Step("Checking assertion for correct adding")
    public void adAssertionCorrect() {

        methodHelper.testScreenshot("testAddCarAd");
        methodHelper.waitTime(2);
        try {
            getDriver().findElement(By.xpath("//*[contains(@text, 'Podgląd')]"));
        }catch (java.util.NoSuchElementException e) {
            Assert.fail("Error on Page: element not found");
        }
        Assert.assertTrue(getDriver().findElement
                (By.xpath("//*[contains(@text, 'Podgląd')]")).isDisplayed());
    }


    @Step("Checking assertion for incorrect adding")
    public void adAssertionIncorrect() {

        methodHelper.testScreenshot("testAddCarAd");
        methodHelper.waitTime(2);
        try {
            getDriver().findElement(By.xpath("//*[contains(@text, 'Oświadczam, że zapoznałem się i " +
                    "akceptuję Regulamin')]"));
        }catch (java.util.NoSuchElementException e) {
            Assert.fail("Error on Page: element not found");
        }
        Assert.assertTrue(getDriver().findElement
                (By.xpath("//*[contains(@text, 'Oświadczam, że zapoznałem się i akceptuję Regulamin'" +
                        ")]")).isDisplayed());
    }


    @Step("Checking assertion for incorrect adding")
    public void adAssertionIncorrectState() {

        methodHelper.testScreenshot("testAddCarAd");
        methodHelper.waitTime(2);
        try {
            getDriver().findElement(By.xpath("//*[contains(@text, 'Dodaj ogłoszenie')]"));
        }catch (java.util.NoSuchElementException e) {
            Assert.fail("Error on Page: element not found");
        }
        Assert.assertTrue(getDriver().findElement
                (By.xpath("//*[contains(@text, 'Dodaj ogłoszenie')]")).isDisplayed());
    }
}
