package com.selenium.java.pages.android;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.MethodHelper;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;


public class AccountScreen extends BaseTest {

    MethodHelper methodHelper = new MethodHelper();

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Obserwowane')]")
    public WebElement btn_observed;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Moje OTOMOTO')]")
    public WebElement btn_MyAcc;
    @FindBy(how = How.ID, using = "pl.otomoto:id/action_remove")
    public WebElement btn_removeAllObserved;
    @FindBy(how = How.ID, using = "pl.otomoto:id/md_buttonDefaultNegative")
    public WebElement btn_doNotRemove;
    @FindBy(how = How.ID, using = "pl.otomoto:id/md_buttonDefaultPositive")
    public WebElement btn_remove;
    @FindBy(how = How.ID, using = "pl.otomoto:id/action_view_type")
    public WebElement btn_viewType;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Galeria')]")
    public WebElement btn_galleryVT;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Mosaika')]")
    public WebElement btn_mosaicVT;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'WYSZUKIWANIA')]")
    public WebElement btn_savedSearch;
    @FindBy(how = How.ID, using = "pl.otomoto:id/btnNotifyImg")
    public WebElement btn_notify;
    @FindBy(how = How.ID, using = "pl.otomoto:id/newAdsTitle")
    public WebElement btn_showSavedSearch;
    @FindBy(how = How.ID, using = "pl.otomoto:id/btnObs")
    public WebElement btn_deleteObserve;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Czat')]")
    public WebElement btn_chat;
    @FindBy(how = How.XPATH, using = "//android.widget.ImageView[@content-desc=\"Więcej opcji\"]")
    public WebElement btn_moreOpt;
    @FindBy(how = How.ID, using = "pl.otomoto:id/listitem_message_title")
    public WebElement btn_allMess;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'znacz')]")
    public WebElement btn_star;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Blokuj użytkownika')]")
    public WebElement btn_block;
    @FindBy(how = How.ID, using = "pl.otomoto:id/md_buttonDefaultNegative")
    public WebElement btn_cancel;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Archiwizuj')]")
    public WebElement btn_archive;
    @FindBy(how = How.ID, using = "pl.otomoto:id/fragment_conversation_input_message")
    public WebElement input_mess;
    @FindBy(how = How.ID, using = "pl.otomoto:id/fragment_conversation_attach_button")
    public WebElement btn_attach;
    @FindBy(how = How.ID, using = "pl.otomoto:id/quick_responses_button")
    public WebElement btn_qMess;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'SPRZEDAŻ')]")
    public WebElement btn_sell;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'KUPNO')]")
    public WebElement btn_buy;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'ARCHIWUM')]")
    public WebElement btn_archives;

    @FindBy(how = How.ID, using = "pl.otomoto:id/generalStats")
    public WebElement btn_stats;
    @FindBy(how = How.ID, using = "pl.otomoto:id/last30days")
    public WebElement btn_last30days;
    @FindBy(how = How.ID, using = "pl.otomoto:id/ads_postad_btn")
    public WebElement btn_addAd;
    @FindBy(how = How.ID, using = "pl.otomoto:id/ads_waiting_btn")
    public WebElement btn_waitingAds;
    @FindBy(how = How.ID, using = "pl.otomoto:id/ads_inactive_btn")
    public WebElement btn_endedAds;

    @Step("Actions in observed ads screen")
    public void accObserved(boolean remove,boolean gallery) {
        System.out.println("Wchodzę w obserwowane");
        methodHelper.waitTime(2);
        methodHelper.tapCoordinates(100, 200);
        methodHelper.waitTime(1);
        btn_observed.click();
        methodHelper.waitTime(2);
        btn_removeAllObserved.click();
        methodHelper.waitTime(2);
        if (remove) {
            btn_remove.click();
        } else {
            btn_doNotRemove.click();
        }
        btn_viewType.click();
        if (gallery) {
            btn_galleryVT.click();
        } else {
            btn_mosaicVT.click();
        }
        methodHelper.waitTime(1);
        btn_savedSearch.click();
        methodHelper.waitTime(1);
        btn_notify.click();
        methodHelper.waitTime(1);
        btn_showSavedSearch.click();
        methodHelper.tapCoordinates(300, 300);
        methodHelper.tapCoordinates(100, 200);
        methodHelper.waitTime(1);
//        btn_deleteObserve.click();
        methodHelper.pressAndroidBackBtn();
    }
    @Step("Actions in chat screen")
    public void accChat(String nick,String chat) {

        methodHelper.tapCoordinates(100, 200);
        methodHelper.waitTime(1);
        btn_chat.click();
        methodHelper.waitTime(1);
        btn_sell.click();
        methodHelper.waitTime(1);
        btn_buy.click();
        methodHelper.waitTime(1);
        btn_archives.click();
        methodHelper.waitTime(1);
        btn_sell.click();
        methodHelper.scrollText(nick,direction.UP,"up",0.8,1);
        WebElement chooseNick = getDriver().findElement(By.xpath("//*[contains(@text, '" + nick + "')]"));
        chooseNick.click();
        methodHelper.waitTime(2);
        methodHelper.pressAndroidBackBtn();
        methodHelper.waitTime(2);
        methodHelper.tapCoordinates(237, 879);
        System.out.println("Klikam 3 kropki");
        methodHelper.waitTime(2);
        btn_moreOpt.click();
        methodHelper.waitTime(2);
        System.out.println("Oznaczam wiad gwiazdka");
        btn_star.click();
        methodHelper.waitTime(2);
        btn_moreOpt.click();
        System.out.println("Klikam blokowanie uzytkownika");
        btn_block.click();
        methodHelper.waitTime(2);
        System.out.println("Anuluje ");
        btn_cancel.click();
        methodHelper.waitTime(1);
        btn_moreOpt.click();
        methodHelper.waitTime(1);
        System.out.println("Klikam archiwizowanie wiadomości");
        btn_archive.click();
        methodHelper.waitTime(1);
        btn_cancel.click();
        methodHelper.waitTime(1);
        System.out.println("Klikam załączniki");
        btn_attach.click();
        methodHelper.getScreenShot("attach buttnons text no visible.png");
        methodHelper.waitTime(1);
        methodHelper.pressAndroidBackBtn();
        System.out.println("Klikam szybką odpowiedź");
        btn_qMess.click();
        methodHelper.waitTime(2);
        methodHelper.pressAndroidBackBtn();
        System.out.println("Wpisuję swoją wiadomość");
        input_mess.sendKeys(chat);
        methodHelper.pressAndroidBackBtn();
    }

    @Step("Actions in my account screen")
    public void accMyAccountStats() {
        methodHelper.tapCoordinates(100, 200);
        System.out.println("Wchodzę w moje otomoto");
        btn_MyAcc.click();
        System.out.println("Wchodzę w statystyki");
        btn_stats.click();
        methodHelper.waitTime(3);
        System.out.println("Klikam statystyki z ostatnich 30 dni");
        btn_last30days.click();
        methodHelper.waitTime(3);
        System.out.println("Scrolluję na dół statystyk");
        methodHelper.scrollText("Wiadomości", direction.UP, "up", 0.8, 1);
        methodHelper.waitTime(5);
        methodHelper.pressAndroidBackBtn();
        System.out.println("Wchodzę w dodawanie ogłoszenia");
        btn_addAd.click();
        methodHelper.waitTime(3);
        methodHelper.pressAndroidBackBtn();
        try {
            btn_waitingAds.click();
            methodHelper.waitTime(3);
            methodHelper.pressAndroidBackBtn();
            btn_endedAds.click();
        } catch (Exception e) {
            methodHelper.pressAndroidBackBtn();
        }
    }
}