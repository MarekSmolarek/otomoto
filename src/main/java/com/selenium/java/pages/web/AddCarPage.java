package com.selenium.java.pages.web;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.MethodHelper;
import io.qameta.allure.Step;
import io.qameta.allure.Story;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.awt.*;

public class AddCarPage extends BaseTest {

    MethodHelper methodHelper = new MethodHelper();

    @FindBy(how = How.XPATH, using = "//*[@id=\"siteWrap\"]/header/div[1]/div/a")
    public WebElement btn_add_ad;

    @FindBy(how = How.XPATH, using = "//span[contains(text(), 'Importowany')]")
    public WebElement checkbox_import;

    @FindBy(how = How.XPATH, using = "//span[contains(text(), 'Uszkodzony')]")
    public WebElement checkbox_damaged;

    @FindBy(how = How.XPATH, using = "//span[contains(text(), 'Kierownica po prawej (Anglik)')]")
    public WebElement checkbox_wheel;

    @FindBy(how = How.ID, id = "vin")
    public WebElement input_vin;

    @FindBy(how = How.XPATH, using = "//*[@id=\"year\"]")
    public WebElement input_year;

    @FindBy(how = How.XPATH, using = "//*[@id=\"downshift-0-item-0\"]/div[1]/span[1]")
    public WebElement btn_year;

    @FindBy(how = How.XPATH, using = "//*[@id=\"make\"]")
    public WebElement input_brand;

    @FindBy(how = How.XPATH, using = "//*[@id=\"downshift-1-item-0\"]/div[1]/span[1]")
    public WebElement btn_brand;

    @FindBy(how = How.XPATH, using = "//*[@id=\"model\"]")
    public WebElement input_model;

    @FindBy(how = How.XPATH, using = "//*[@id=\"downshift-2-item-0\"]/div[1]/span[1]")
    public WebElement btn_model;

    @FindBy(how = How.XPATH, using = "//*[@id=\"fuel_type\"]")
    public WebElement input_fuel;

    @FindBy(how = How.XPATH, using = "//*[@id=\"downshift-3-item-0\"]/div[1]/span[1]")
    public WebElement btn_fuel;

    @FindBy(how = How.XPATH, using = "//*[@id=\"engine_power\"]")
    public WebElement input_engine_power;

    @FindBy(how = How.XPATH, using = "//*[@id=\"engine_capacity\"]")
    public WebElement input_engine_capacity;

    @FindBy(how = How.XPATH, using = "//*[@id=\"door_count\"]")
    public WebElement input_door_count;

    @FindBy(how = How.XPATH, using = "//*[@id=\"downshift-4-item-0\"]/div[1]/span[1]")
    public WebElement btn_door_count;

    @FindBy(how = How.XPATH, using = "//*[@id=\"gearbox\"]")
    public WebElement input_transmission;

    @FindBy(how = How.XPATH, using = "//*[@id=\"downshift-5-item-0\"]/div[1]/span[1]")
    public WebElement btn_transmission;

    @FindBy(how = How.XPATH, using = "//*[@id=\"version\"]")
    public WebElement input_version;

    @FindBy(how = How.XPATH, using = "//*[@id=\"downshift-6-item-0\"]/div[1]/span[1]")
    public WebElement btn_version;

    @FindBy(how = How.XPATH, using = "//*[@id=\"generation\"]")
    public WebElement input_generation;

    @FindBy(how = How.XPATH, using = "//*[@id=\"downshift-7-item-0\"]/div[1]/span[1]")
    public WebElement btn_generation;

    @FindBy(how = How.XPATH, using = "//*[@id=\"mileage-field\"]")
    public WebElement input_mileage;

    @FindBy(how = How.XPATH, using = "//*[@id=\"downshift-8-item-0\"]/div[1]/span[1]")
    public WebElement btn_mileage;

    @FindBy(how = How.XPATH, using = "//*[@id=\"add-title\"]")
    public WebElement input_title;

    @FindBy(how = How.XPATH, using = "//*[@id=\"select2-param579-container\"]")
    public WebElement btn_type;

    @FindBy(how = How.XPATH, using = "/html/body/span/span/span[1]/input")
    public WebElement input_type;

    @FindBy(how = How.XPATH, using = "/html/body/span/span/span[2]")
    public WebElement btn_choose_item;

    @FindBy(how = How.XPATH, using = "//*[@id=\"cookiesBar\"]/div/div/a")
    public WebElement btn_cookies;

    @FindBy(how = How.XPATH, using = "//*[@id=\"select2-param599-container\"]/span")
    public WebElement btn_colour;

    @FindBy(how = How.XPATH, using = "/html/body/span/span/span[1]/input")
    public WebElement input_colour;

    @FindBy(how = How.XPATH, using = "//*[@id=\"gallery\"]/div[1]/div/div[2]/a")
    public WebElement btn_photo;

    @FindBy(how = How.XPATH, using = "//*[@id=\"newOffer\"]/div[2]/main/fieldset[11]/div/div/div[1]/div[1]/input[2]")
    public WebElement input_price;

    @FindBy(how = How.XPATH, using = "//*[@id=\"select2-currency-container\"]")
    public WebElement btn_currency;

    @FindBy(how = How.XPATH, using = "/html/body/span/span/span[1]/input")
    public WebElement input_currency;

    @FindBy(how = How.XPATH, using = "//*[@id=\"newOffer\"]/div[2]/main/fieldset[11]/div/div/div[1]/div[3]/label[1]")
    public WebElement checkbox_netto;

    @FindBy(how = How.XPATH, using = "//*[@id=\"newOffer\"]/div[2]/main/fieldset[11]/div/div/div[1]/div[3]/label[2]")
    public WebElement checkbox_negotiation;

    @FindBy(how = How.XPATH, using = "/html/body/span/span/span[1]/input")
    public WebElement input_location;

    @FindBy(how = How.XPATH, using = "//*[@id=\"map_address\"]/span/span[1]/span")
    public WebElement btn_location;

    @FindBy(how = How.XPATH, using = "//*[@id=\"save\"]")
    public WebElement btn_summary;

    @FindBy(how = How.XPATH, using = "//*[@id=\"newOffer\"]/div[2]/main/div[6]/div/div/div[2]/div[2]/div/text()")
    public WebElement text_assert;

    @Story("Moving to add screen")
    public void addMoveToAddScreen() {
        btn_add_ad.click();
    }

    @Story("Setting basic information")
    public void addSetBasicInfo(boolean imported, boolean damaged, boolean wheel) {
        if (imported) {
            checkbox_import.click();
        }

        if (damaged) {
            checkbox_damaged.click();
        }

        if (wheel) {
            checkbox_wheel.click();
        }
    }

    @Story("Setting VIN")
    public void addSetVin(String vin) {
        input_vin.sendKeys(vin);
    }

    public void addSetYear(String year) {
        input_year.sendKeys(year);
        btn_year.click();

    }

    @Step("Setting brand")
    public void addSetBrand(String brand) {
        input_brand.sendKeys(brand);
        btn_brand.click();
    }

    @Step("Setting model")
    public void addSetModel(String model) {
        input_model.sendKeys(model);
        btn_model.click();
    }

    @Step("Setting type of fuel")
    public void addSetFuel(String fuel) {
        input_fuel.sendKeys(fuel);
        btn_fuel.click();
    }

    @Step("Setting engine power")
    public void addSetEnginePower(String power) {
        input_engine_power.sendKeys(power);

    }

    @Step("Setting engine capacity")
    public void addSetEngineCapacity(String capacity) {
        input_engine_capacity.sendKeys(capacity);

    }

    @Step("Setting number of doors")
    public void addSetDoors(String number) {
        input_door_count.sendKeys(number);
       btn_door_count.click();
    }

    @Step("Setting transmission")
    public void addSetTransmission(String type) {
        input_transmission.sendKeys(type);
        btn_transmission.click();
    }

    @Step("Setting version")
    public void addSetVersion(String version) {
        input_version.sendKeys(version);
        btn_version.click();
    }

    @Step("Setting generation")
    public void addSetGeneration(String generation) {
        input_generation.sendKeys(generation);
        btn_generation.click();
    }

    @Step("Setting mileage")
    public void addSetMileage(String mileage) {
        input_mileage.sendKeys(mileage);

    }

    @Step("Setting title")
    public void addSetTitle(String title) {
        input_title.sendKeys(title);
    }

    @Step("Setting type of car")
    public void addSetTypeOfCar(String type) {
        btn_type.click();
       input_type.sendKeys(type);
       methodHelper.waitTime(2);
        btn_choose_item.click();

    }

    @Step("Setting colour")
    public void addSetColour(String colour){
        btn_colour.click();
        input_colour.sendKeys(colour);
        methodHelper.waitTime(2);
        btn_choose_item.click();

    }

    @Step("Adding photo")
    public void addPhoto () throws AWTException {
        btn_photo.click();
            methodHelper.uploadImage("C:\\Users\\48501\\Desktop\\opel.jpg");
    }

    @Step("Setting price")
    public void addSetPrice(String price, String currency, boolean netto, boolean neg){
        input_price.sendKeys(price);
        btn_currency.click();
        input_currency.sendKeys(currency);
        btn_choose_item.click();
        if(netto){
        checkbox_netto.click();}
        if(neg){
        checkbox_negotiation.click();}
    }

    @Step("Setting localization")
    public void addSetLocation(String city){
        btn_location.click();
        input_location.sendKeys(city);

        btn_choose_item.click();
    }

    @Step("Move to summary")
    public void addSummary(){
        btn_summary.click();
    }

    @Step("Accepting cookies")
    public void addCookies() {
        btn_cookies.click();
    }


}

