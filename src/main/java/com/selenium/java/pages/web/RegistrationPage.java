package com.selenium.java.pages.web;

import com.selenium.java.base.BaseTest;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

public class RegistrationPage extends BaseTest {

    @FindBy(how = How.XPATH, using = "//*[@id=\"page-header\"]/div/nav/ul/li[3]/a/span")
    public WebElement btn_my_account;

    @FindBy(how = How.XPATH, using = "//*[@id=\"loginForm\"]/fieldset/div[6]/p[2]/a")
    public WebElement btn_move_to_reg;

    @FindBy(how = How.ID, using = "userEmail")
    public WebElement input_email;

    @FindBy(how = How.ID, using = "userPass")
    public WebElement input_password;

    @FindBy(how = How.ID, using = "userPass-repeat")
    public WebElement input_repassword;

    @FindBy(how = How.XPATH, using = "//*[@id=\"mainForm\"]/div/fieldset/div[4]/p/label")
    public WebElement checkbox_policy;

    @FindBy(how = How.ID, using = "se_userSignIn")
    public WebElement btn_sing_in;

    @FindBy(how = How.XPATH, using = "//*[@id=\"cookiesBar\"]/div/div/a")
    public WebElement btn_cookies;


    @Step("Moving to my account")
    public void regMyAccount(){
        btn_my_account.click();
    }

    @Step("Moving to registration")
    public void regMoveToReg(){
        btn_move_to_reg.click();
    }

    @Step("Setting e-mail")
    public void regSetEmail(String email){
        input_email.sendKeys(email);
    }

    @Step("Setting password")
    public void regSetPassword(String pass){
        input_password.sendKeys(pass);
    }

    @Step("Repeat password")
    public void regSetRepassword(String pass){
        input_repassword.sendKeys(pass);
    }

    @Step("Checking the policy")
    public void regCheckPolicy(boolean policy){
        if(policy){
            checkbox_policy.click();
        }
    }

    @Step("Sing In")
    public void regSingIn(){
        btn_sing_in.click();
    }

    @Step("Checking assertion for correct reg")
    public void regAssert(){

}
    @Step("Checking assertion for fail reg")
    public void regFailAssert(){

    }
    @Step("Accepting cookies")
    public void regCookies(){
            btn_cookies.click();
    }

}

